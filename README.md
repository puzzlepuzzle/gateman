
git clone git@bitbucket.org:puzzlepuzzle/gateman.git

cd gateman

npm install

node server

------
sudo apt-get install mysql-server
mysql -u root -p
(db/init.sql)

curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs build-essential
sudo ln -s /usr/bin/nodejs /usr/bin/node

(git clone git://github.com/creationix/nvm.git ~/.nvm
. ~/.nvm/nvm.sh
nvm install v4.7.0
nvm use v4.7.0)

npm version 5.5.1
(update  npm
cd ~/.nvm/versions/node/v4.7.0/lib/
npm install npm)

wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
sudo make install
cd utils
sudo ./install_server.sh

------
Запуск тестов
node ./node_modules/mocha/bin/mocha --timeout 150000 <path to test file>

------
Флаги

debug
1) вкл/выкл сигнализации через "http://localhost:" + config.get('port') + '/api/fake'
2) фейковое значение с датчика

start_queue
запускает очередь на считывание с датчиков (интерфейс, регистрация типов задач, добавление задач на датчики)

start_read
добавляет в очередь задачи на считывание с датчиков

start_alarm
1) инициализация сигнализаций
2) вкл/выкл сигнализации для нового значения с датчика
3) вкл/выкл сигнализации для нового значения с двери
4) можно дергать апи /alarms/off

БД (вызвать в любой дириктории, ввести судо пароль)
force_refresh.sh - пересоздать базу с нуля до последней версии
refresh.sh - перенакатить справочники  и процедуры с функциями
test_refresh.sh - нагенерировать тестовые данные
migrate.sh - миграция до последней версии