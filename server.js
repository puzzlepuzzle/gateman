#!/usr/bin/env node

require("babel-core/register")({
    presets: ['es2015', 'react'],
});
require.extensions['.css'] = () => {
    return;
};
require.extensions['.jpg'] = () => {
    return;
};

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// var logger = require('morgan');
var passport = require('passport');
var session = require('express-session');
var SessionStore = require('express-mysql-session');
var kue = require('./kue/kue_init');

var log = require('./log')(module);
var Errors = require('./errors');
var config = require('./config');
var AlarmSingleton = require("./alarm/alarm_singleton.js");

var options =  config.get('mysql:options');

var app = express();

// view engine setup
app.set('views', __dirname + '/templates');
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.disable('etag');

// app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());

app.use(require('./middleware/sendHttpError'));

app.use(session({
    secret: config.get('passport_key'),
    store: new SessionStore(options),
    resave: false,
    saveUninitialized: false,
    cookie:  { path: '/', httpOnly: true, secure: false, maxAge: null }
}));

require('./auth/passport')(passport); // pass passport for configuration
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(express.static(path.join(__dirname, 'public')));
app.use('/lib', express.static(__dirname + '/node_modules/bootstrap/dist/'));

app.use('/door', require('./routes/door'));
app.use('/api', require('./routes/api'));
app.use('/print', require('./routes/print'));
app.use('/auth', require('./routes/auth')(passport));

app.use('/', require('./routes/index'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Errors.HttpError(404);
    next(err);
});

// error handlers
app.use(function(err, req, res, next) {
    if (err instanceof Errors.HttpError || err instanceof Errors.AuthError || err instanceof Errors.ApiError) {
        res.sendHttpError(err);
    } else {
        log.error(err);
        err = new Errors.HttpError(500);
        res.sendHttpError(err);
    }
});
/**
 * Create HTTP server.
 */
// process.on('uncaughtException', function(err) {
// })

// process.on('unhandledRejection', function(reason, p){
// });

app.listen(config.get('port'), function () {
    log.info('Express server listening on port ' + config.get('port'));
    log.info('config', config.get());

    if (+config.get("start_queue")) {
        kue.initTasks();
    }
    if (+config.get("start_alarm")){
        new AlarmSingleton().initAllAlarms()
            .then(function(){
                return new AlarmSingleton().offAllAlarms()
            })
    }
});

if (module.parent){
    module.exports = app;
}
