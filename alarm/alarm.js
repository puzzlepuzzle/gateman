var Config = require('../config/index');
var log = require('../log/index')(module);
var rp = require('request-promise');
var Device = require("./../db/models/device");
var Indication = require("./../db/models/indication");
var Promise = require('bluebird');
var DeviceTypeList = require("../db/device_type_list");
var config = require('../config');

var Alarm = function (deviceId) {
    if (!deviceId) {
        return Promise.reject(new Error("No deviceId for alarm"));
    }
    var _this = this;
    _this.deviceId = deviceId;
    _this.devices = {};
    return Device.getById(deviceId)
        .then(function (device) {
            if (!device) {
                throw new Error("No alarm with id " + deviceId)
            }
            if (device.DEVICETYPE_ID != DeviceTypeList.types.alarm) {
                throw new Error("Alarm with id " + deviceId + " has wrong type " + device.DEVICETYPE_ID)
            }
            if (!device.DEVICE_IP) {
                throw new Error("Alarm with id " + deviceId + " has no DEVICE_IP")
            }
            _this.device = device;
            return _this;
        })
        .catch(function (err) {
            log.error("alarm init with deviceId ", deviceId, " , error: ", err)
            throw err;
        })
}

Alarm.prototype.addReasonDevice = function (deviceId) {
    return this.reasonDeviceOperation(deviceId, true);
}

Alarm.prototype.removeReasonDevice = function (deviceId) {
    return this.reasonDeviceOperation(deviceId, false);
}

Alarm.prototype.reasonDeviceOperation = function (deviceId, wantOn) {
    this.devices[deviceId] = wantOn;
    var canOff = true;
    for (var key in this.devices) {
        if (this.devices[key]) {
            canOff = false;
            break;
        }
    }
    
    if (wantOn) {
        return this.on();
    } else if (canOff)
        return this.off();
    else
        return Promise.resolve(true);
}

Alarm.prototype.func = function (func, value) {
    var _this = this;
    var addr = "http://" + _this.device.DEVICE_IP + '/' + func;
    if (+config.get("debug")) {
        addr = "http://localhost:" + config.get('port') + '/api/fake';
    }
    var options = {
        uri: addr,
        timeout: config.get("alarm_max")
    }
    return rp(options)
        .then(function (htmlString) {
            log.debug("alarm ", _this.device.DEVICE_IP, " result of " + func + ": ", htmlString)
            return htmlString
        })
        .then(function (htmlString) {
            return Indication.newIndicator(value, _this.device.DEVICE_ID)
        })
        .then(function (newValue) {
            log.debug("alarm SAVED: ", JSON.stringify(_this.device), newValue)
            return newValue;
        })
        .catch(function (err) {
            log.error("alarm, " + func + ", error: ", err, _this.device);
            throw err;
        });
}
Alarm.prototype.on = function () {
    return this.func("on", 1)
}

Alarm.prototype.off = function () {
    return this.func("off", 0)
}

module.exports = Alarm;