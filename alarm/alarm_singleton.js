var Promise = require('bluebird');
var config = require('../config/index');
var DeviceTypeList = require("../db/device_type_list");
var Alarm = require("./alarm");
var AlarmModel = require("../db/models/alarm")

var Singleton = (function () {
    var instance;
    var alarms = {};

// Приватные методы и свойства
    function add(deviceId) {
        if (!alarms[deviceId]) {
            return new Alarm(deviceId)
                .then(function (alarm) {
                    alarms[deviceId] = alarm;
                })

        } else return Promise.resolve(alarms[deviceId])
    }

// Конструктор
    function Singleton() {
        if (!instance)
            instance = this;
        else return instance;
    }

// Публичные методы

    Singleton.prototype.getCurrentAlarms = function () {
        return alarms;
    };

    Singleton.prototype.initAllAlarms = function () {
        return AlarmModel.getAll()
            .then(function (alarms) {
                var promises = [];
                for (var key in alarms) {
                    if (alarms[key].ACTION_DEVICE_ID){
                        promises.push(add(alarms[key].ACTION_DEVICE_ID));
                    }                        
                }
                return Promise.all(promises);
            })
    };

    Singleton.prototype.offAllAlarms = function () {
        var promises = [];
        for (var key in alarms) {
            promises.push(alarms[key].off())
        }
        return Promise.all(promises);
    };

    Singleton.prototype.callAlarm = function (reasonDeviceId, value) {
        return AlarmModel.getByReasonDeviceId(reasonDeviceId)
            .then(function (alarm) {
                if (!alarm) {
                    return;
                }
                if (!alarms[alarm.ACTION_DEVICE_ID]) {
                    return;
                }

                if (value < alarm.ALARM_YZONE_FROM || value > alarm.ALARM_YZONE_TO) {
                    return alarms[alarm.ACTION_DEVICE_ID].addReasonDevice(reasonDeviceId);
                } else {
                    return alarms[alarm.ACTION_DEVICE_ID].removeReasonDevice(reasonDeviceId);
                }
            })
    }

    return Singleton;
})();

module.exports = Singleton;