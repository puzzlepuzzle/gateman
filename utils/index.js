var moment = require('moment');

function arrayDecimate(array, maxCount, addEmpty){
    if (!array || !maxCount || array.length <= maxCount){
        return array;
    }

    addEmpty = addEmpty != undefined ? addEmpty: true;

    var delimiter = (array.length - maxCount)/maxCount;
    var result = [];
    var sum = 0;
    for (var i=0; i< array.length; i++){
        if (sum >=1){
            sum--;
            addEmpty && result.push("")
        } else{
            sum+=delimiter;
            result.push(array[i])
        }
    }
    return result;
}

function removeUtcOffset(date) {
    return moment(date).add(moment(date).utcOffset(), 'm').utc().toDate()
}


exports.arrayDecimate = arrayDecimate;
exports.removeUtcOffset = removeUtcOffset;