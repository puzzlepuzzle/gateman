var Bookshelf = require('bookshelf');
var config = require('../config');
var log = require('../log')(module);

log.debug(config.get('mysql:options'));
var configDb =  config.get('mysql:options');
configDb.charset = 'utf8';
configDb.timezone = '+0:00'

var knex = require('knex')({
    client: 'mysql',
    connection: configDb
});
var DB = require('bookshelf')(knex);

module.exports = DB;