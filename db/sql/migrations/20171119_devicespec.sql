USE gateman;

DELIMITER $$
DROP PROCEDURE IF EXISTS TMP_PATCH_DEVICESPEC$$
CREATE PROCEDURE TMP_PATCH_DEVICESPEC()
BEGIN
    
    DROP TABLE IF EXISTS devicespec;
    CREATE TABLE devicespec (
        DEVICESPEC_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        DEVICE_IDS TEXT,
        DEVICESPEC_SN VARCHAR(255),
        DEVICE_ADDRESS INT,
        UNIQUE(DEVICESPEC_SN)
    ) ENGINE=InnoDB;
    
    INSERT INTO devicespec (DEVICE_IDS, DEVICESPEC_SN, DEVICE_ADDRESS)
        SELECT
                GROUP_CONCAT(DEVICE_ID), NULL, DEVICE_ADDRESS
            FROM 
                device
            WHERE 
                DEVICE_ADDRESS <> 0
            GROUP BY
                DEVICE_ADDRESS;
    
    ALTER TABLE device ADD COLUMN DEVICESPEC_ID INT;
    
    UPDATE
            device D
            JOIN devicespec DS 
                ON FIND_IN_SET(D.DEVICE_ID, DS.DEVICE_IDS)
        SET
            D.DEVICESPEC_ID = DS.DEVICESPEC_ID;
    
    ALTER TABLE device 
        ADD FOREIGN KEY (DEVICESPEC_ID) REFERENCES devicespec(DEVICESPEC_ID),
        DROP COLUMN DEVICE_ADDRESS;
    
    ALTER TABLE devicespec DROP COLUMN DEVICE_IDS;
    
    UPDATE
            devicespec
        SET
            DEVICESPEC_SN = CASE DEVICE_ADDRESS
                WHEN '13'     THEN '13388'	    		
                WHEN '14'     THEN '13389'				
                WHEN '16'     THEN '13391'			
                WHEN '17'     THEN '13392'			
                WHEN '18'     THEN '13393'			
                WHEN '19'     THEN '13394'			
                WHEN '20'     THEN '13395'			
                WHEN '21'     THEN '13396'			
                WHEN '22'     THEN '13397'			
                WHEN '23'     THEN '13398'			
                WHEN '24'     THEN '13399'			
                WHEN '25'     THEN '13400'			
                WHEN '26'     THEN '13401'			
                WHEN '1'      THEN '14023'			
                WHEN '2'      THEN '14024'			
                WHEN '3'      THEN '14025'			
                WHEN '4'      THEN '14026'			
                WHEN '5'      THEN '14027'			
                WHEN '6'      THEN '14028'			
                WHEN '7'      THEN '14029'			
                WHEN '8'      THEN '14030'			
                WHEN '9'      THEN '14031'			
                WHEN '10'     THEN '14032'			
                WHEN '101'    THEN '62'			    
                WHEN '102'    THEN '63'			    
                WHEN '103'    THEN '64'			    
                WHEN '104'    THEN '65'			    
                WHEN '105'    THEN '66'			    
                WHEN '106'    THEN '67'			    
                WHEN '107'    THEN '68'			    
                WHEN '108'    THEN '69'			    
                WHEN '109'    THEN '70'			    
                WHEN '110'    THEN '71'			    
                WHEN '111'    THEN '72'
                WHEN '11'     THEN '14033'
                WHEN '15'     THEN '13390'
                WHEN '12'     THEN '14034'
                ELSE NULL
            END;
    
    INSERT IGNORE INTO devicespec (DEVICE_ADDRESS, DEVICESPEC_SN) VALUES
         ('13'  ,'13388'),
         ('14'  ,'13389'),				
         ('16'  ,'13391'),
         ('17'  ,'13392'),
         ('18'  ,'13393'),
         ('19'  ,'13394'),
         ('20'  ,'13395'),
         ('21'  ,'13396'),
         ('22'  ,'13397'),
         ('23'  ,'13398'),
         ('24'  ,'13399'),
         ('25'  ,'13400'),
         ('26'  ,'13401'),
         ('1'   ,'14023'),
         ('2'   ,'14024'),
         ('3'   ,'14025'),
         ('4'   ,'14026'),
         ('5'   ,'14027'),
         ('6'   ,'14028'),
         ('7'   ,'14029'),
         ('8'   ,'14030'),
         ('9'   ,'14031'),
         ('10'  ,'14032'),
         ('101' ,'62'),
         ('102' ,'63'),
         ('103' ,'64'),
         ('104' ,'65'),
         ('105' ,'66'),
         ('106' ,'67'),
         ('107' ,'68'),
         ('108' ,'69'),
         ('109' ,'70'),
         ('110' ,'71'),
         ('111' ,'72'),
         ('11'  ,'14033'),
         ('15'  ,'13390'),
         ('12'  ,'14034');

    
END$$
DELIMITER ;

CALL PATCH(
    'create table `device_sn`', 
    'CALL TMP_PATCH_DEVICESPEC');
    
DROP PROCEDURE IF EXISTS TMP_PATCH_DEVICESPEC;
