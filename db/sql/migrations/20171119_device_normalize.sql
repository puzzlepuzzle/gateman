USE gateman;

DELIMITER $$
DROP PROCEDURE IF EXISTS TMP_PATCH_NORMALIZE_DEVICE$$
CREATE PROCEDURE TMP_PATCH_NORMALIZE_DEVICE()
BEGIN
    
    ALTER TABLE devicetype
        ADD COLUMN DEVICE_BYTE_START INT, 
        ADD COLUMN DEVICE_BYTE_COUNT INT;
    
    UPDATE
            devicetype
        SET
            DEVICE_BYTE_START = CASE DEVICETYPE_ID 
                                    WHEN 1 THEN 2
                                    WHEN 2 THEN 1
                                    WHEN 3 THEN 1
                                    ELSE NULL
                                END,
            DEVICE_BYTE_COUNT = CASE  
                                    WHEN DEVICETYPE_ID IN (1,2,3) THEN 1
                                    ELSE NULL
                                END;
    
    ALTER TABLE device 
        DROP COLUMN DEVICE_BYTE_START,
        DROP COLUMN DEVICE_BYTE_COUNT;
        
    
END$$
DELIMITER ;

CALL PATCH(
    "alter table `device` & `devicetype` (normalize `device`)", 
    'CALL TMP_PATCH_NORMALIZE_DEVICE');
    
DROP PROCEDURE IF EXISTS TMP_PATCH_NORMALIZE_DEVICE;
