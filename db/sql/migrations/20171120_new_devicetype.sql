USE gateman;

CALL PATCH('new devicetypes: oven(°C) + oven(%)', "
INSERT INTO devicetype (DEVICETYPE_ID, DEVICETYPE_ABBR, DEVICETYPE_NAME, DEVICETYPE_REPORTING, DEVICE_BYTE_START, DEVICE_BYTE_COUNT) VALUES
(7, '°C', 'ОВЕН-Датчик температуры'	,1, 258, 1),
(8,  '%', 'ОВЕН-Датчик влажности'   ,1, 259, 1);");
