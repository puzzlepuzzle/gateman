USE gateman;

CALL PATCH('create table `avgind` from table `indication` + recreate table `indication`. Part 1', '
RENAME TABLE indication TO avgind;');
CALL PATCH('create table `avgind` from table `indication` + recreate table `indication`. Part 2', '
ALTER TABLE avgind ADD COLUMN INDICATION_CORRECTED_VALUE decimal(14,6) DEFAULT NULL;');

CALL PATCH('create table `avgind` from table `indication` + recreate table `indication`. Part 3', '
CREATE TABLE `indication` (
  `INDICATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DEVICE_ID` int(11) NOT NULL,
  `INDICATION_VALUE` decimal(14,6) NOT NULL,
  `INDICATION_REGDATE` datetime NOT NULL,
  PRIMARY KEY (`INDICATION_ID`),
  KEY `DEVICE_ID` (`DEVICE_ID`,`INDICATION_REGDATE`),
  CONSTRAINT `indication_ibfk_1` FOREIGN KEY (`DEVICE_ID`) REFERENCES `device` (`DEVICE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);');
