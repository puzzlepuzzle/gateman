USE gateman;

CALL PATCH('add search index on `roomcheck` (ROOMCHECK_FROM)', 'CREATE INDEX IDX_roomcheck_from ON roomcheck (ROOMCHECK_FROM)');
CALL PATCH('add search index on `roomcheck` (DATE_CREATE)', 'CREATE INDEX IDX_roomcheck_create ON roomcheck (DATE_CREATE)');
