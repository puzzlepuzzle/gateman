USE gateman;

CALL CREATE_USER(
    'admin',
    '$2a$08$QlA2gTBda4JCh8Z4/eSyb.gbacR/hEdpARwyqsQnLKp8E3XYbL//.',
    1,
    'Администратор',
    'admin_sign.png'
);

CALL CREATE_USER(
    'test',
    '$2a$08$rfBZWyxCAo1nr9wlLGFE1uc8/EcJCO5.HYmOz3qREb8P/CtWNjeVm',
    2,
    'Тестовый пользователь',
    'test_user_sign.png'
);

CALL CREATE_USER(
    'director',
    '$2a$08$InOpAynEqAMu2d7k8M6DrOyrofTAYSmtTdrUOboUudCNxXjsxdxRq',
    3,
    'Тестовый руководитель',
    'test_director_sign.png'
);

CALL CREATE_USER(
    "ruzavin",
    '$2a$08$MYtBNLfGb/tteU6GbrEuy.NeHwWlCe11TlMvrf3oT6CaxT6rnrKJe',
    3,
    "Главный инженер Рузавин С.В.",
    "1.jpg"
);
CALL CREATE_USER(
    "morozova",
    '$2a$08$52wmksNInsqgeb/5Ji6nmeIbuDKXsGnwudcRuJ7YdhDS6hiu8aFmi',
    3,
    "Начальник складского хозяйства Морозова Н.П.",
    "2.jpg"
);
CALL CREATE_USER(
    "saar",
    '$2a$08$Bq/kBy3jMdbQgo61psLQ.eTWCVFSUUuX6WmogziksuKFvBZ9rJgAO',
    3,
    "Кладовщик склада Саар С.К.",
    "3.jpg"
);
CALL CREATE_USER(
    "tokarev",
    '$2a$08$rs2xBUm0dNdgIhSagDR1L.oDf1C6WdZLGCBqnGliGC0Tt4ggJjozu',
    3,
    "Сменный мастер Токарев М.В.",
    "4.jpg"
);
CALL CREATE_USER(
    "oreshkina",
    '$2a$08$Nn0klVFds63PfEa/x3EQHuRkDS1KY/LYQ.kAdYaMZE4v3lCkWyS56',
    3,
    "Сменный мастер Орешкина А.В.",
    "5.jpg"
);
CALL CREATE_USER(
    "samojlova",
    '$2a$08$KnDhX7aNCRjLO3Xc2X06XuVCTKIdMyxUI7ftGeZEk2AVYiB9uyfXK',
    3,
    "Сменный мастер Самойлова Н.Г.",
    "6.jpg"
);
CALL CREATE_USER(
    "shabalina",
    '$2a$08$NnSB0znCyZX2DOXupBuddufw8BwkzgLSL41zkxiHsRwXnQj2mYAQ6',
    3,
    "Сменный мастер Шабалина А.К.",
    "7.jpg"
);
CALL CREATE_USER(
    "soloveva",
    '$2a$08$bcBoXLj6gxrn1UoImBevmew7GcsZ2M3sQ6alezSQk.g6FcwtTvAZK',
    3,
    "начальник участка получения таблеток и капсул Соловьева В.А.",
    "8.jpg"
);
CALL CREATE_USER(
    "mefodeva",
    '$2a$08$lpqHUuJ8/loPP/wIfa0Fi.SZEBtp08BHQwNcvDX4octocOt5jhSSO',
    3,
    "ЗИДП Мефодьева И.Ю.",
    "9.jpg"
);
CALL CREATE_USER(
    "laeva",
    '$2a$08$ef24zpZU.q9TGb/ei4zO2eiMFY9MejvtwL1x9cdHAh7exndAPoXsm',
    3,
    "Начальник ОПУ Лаева А.А.",
    "10.jpg"
);
CALL CREATE_USER(
    "stashkina",
    '$2a$08$HUMHOjYlu5x1dhyS.5tW0eQvcZRks/uLFUXKFt.wD7MdJfPO.xTHW',
    3,
    "Контролер отдела контроля качества Сташкина Л.И.",
    "11.jpg"
);
CALL CREATE_USER(
    "sitdikova",
    '$2a$08$pL5qMB8.EdBAXtykFBzZnu.6agZMo/Is2K.WbYW59cD26iWt22f.a',
    3,
    "Контролер отдела контроля качества Ситдикова Л.Ф.",
    "12.jpg"
);
CALL CREATE_USER(
    "viktorova",
    '$2a$08$wpJC1R7ZEl.7STxO.1Dtru0dW78XvgT6CuqJMRWu6og7QuqVpqxf.',
    3,
    "Контролер отдела контроля качества Викторова И.В.",
    "13.jpg"
);
CALL CREATE_USER(
    "chistyakova",
    '$2a$08$2PhAjvIqIP.pyIuJvj/Kme1A97lig9e4PdQR9pL89eQXFdaEk3rKq',
    3,
    "Заведующая МБЛ Чистякова С.Р.",
    "14.jpg"
);
CALL CREATE_USER(
    "nanysheva",
    '$2a$08$8L9d2n9nv3.eouyp5E8ZTu3FeKhfnr1yYWJLBi2EtIRIhhjS6KFiW',
    3,
    "Микробиолог Нанышева В.Ш.",
    "15.jpg"
);
CALL CREATE_USER(
    "zashixina",
    '$2a$08$lofCOh1SD1a4ZOOBzKpO5OdymrqT6pHpSkhOZBoe89FnYOo9NWpj2',
    3,
    "Начальник ОКК Зашихина Е.Ф.",
    "16.jpg"
);
CALL CREATE_USER(
    "kostareva",
    '$2a$08$a3ECWLrNSIo55hMQB8CdguBlHL9CGpQjF9RBPqj94bJHsHJ8q/JvK',
    3,
    "Заместитель начальника ОКК Костарева Л.В.",
    "17.jpg"
);
CALL CREATE_USER(
    "barashkov",
    '$2a$08$WdZYYVeCRAtGIgEPpZFUO.H49sfILvoJW3IjXcmg8yK4pyYgH3Ux6',
    3,
    "Исполнительный директор Барашков В.Ф.",
    NULL
);
CALL CREATE_USER(
    "chizhova",
    '$2a$08$AP85CtTwB37Q484Syls4VeOmFQke619askXm86l5QtiZJefwXzxzK',
    2,
    "Начальник ООК Чижова М.В.",
    NULL
);
CALL CREATE_USER(
    "shablakova",
    '$2a$08$VoPT8lUoLr/NopfGOTRfyuBCtNJsJ0RzkpKqWmqR5aKZMatVO.2zu',
    2,
    "Начальник КО Шаблакова А.С.",
    NULL
);
CALL CREATE_USER(
    "kladov",
    '$2a$08$AiKLguaMvXP47tablHFcbOfYu.bBriVWWIre16TYbhucZKMPZvzGu',
    2,
    "Инженер КИПиА Кладов И.Г.",
    NULL
);



