USE gateman;

REPLACE INTO devicetype (DEVICETYPE_ID, DEVICETYPE_ABBR, DEVICETYPE_NAME) VALUES
(1, '°C'     , 'Датчик температуры'      ),
(2, '%'      , 'Датчик влажности'        ),
(3, 'Па'     , 'Датчик перепада давления'),
(4, 'door'   , 'Датчик двери'            ),
(5, 'alarm'  , 'Сигнализация'            ),
(6, 'Па'     , 'Составной счетчик'       );
