USE gateman;

INSERT INTO roles (ROLE_ID, ROLE_ABBR, ROLE_NAME) VALUE (1, 'Admin', 'Администратор' );
INSERT INTO roles (ROLE_ID, ROLE_ABBR, ROLE_NAME) VALUE (2, 'Viewer', 'Пользователь' );
INSERT INTO roles (ROLE_ID, ROLE_ABBR, ROLE_NAME) VALUE (3, 'Director', 'Руководитель' );
