USE gateman;


DELIMITER $$

/** **********************
SYSPARAMS START >>>>>>
** ***********************/
/*
переключение режима "скрытность"
*/
DROP PROCEDURE IF EXISTS SWITCH_STEALTH_MODE$$
CREATE PROCEDURE SWITCH_STEALTH_MODE()
BEGIN

    CALL SET_SYSPARAM_VALUE(
        'STEALTH_MODE',
        NOT IFNULL(GET_SYSPARAM_VALUE('STEALTH_MODE'), 0)
    );

END$$


/*
Получение значения параметра по коду
*/
DROP FUNCTION IF EXISTS GET_SYSPARAM_VALUE$$
CREATE FUNCTION GET_SYSPARAM_VALUE(
    iSYSPARAM_CODE VARCHAR(255)
)
RETURNS VARCHAR(255)
BEGIN
        RETURN (SELECT SYSPARAM_VALUE FROM sysparam WHERE SYSPARAM_CODE = iSYSPARAM_CODE);

END$$

/*
Создание пользователя
*/
DROP PROCEDURE IF EXISTS SET_SYSPARAM_VALUE$$
CREATE PROCEDURE SET_SYSPARAM_VALUE(
    iSYSPARAM_CODE VARCHAR(255),
    iSYSPARAM_VALUE VARCHAR(255)
)
BEGIN
    
    UPDATE 
            sysparam 
        SET 
            SYSPARAM_VALUE = iSYSPARAM_VALUE,
            SYSPARAM_UPDATED = NOW()
        WHERE 
            SYSPARAM_CODE = iSYSPARAM_CODE;

END$$

/** **********************
SYSPARAMS END <<<<<<
** ***********************/

/*
Создание пользователя
*/
DROP PROCEDURE IF EXISTS CREATE_USER$$
CREATE PROCEDURE CREATE_USER(
    iUSER_LOGIN VARCHAR(50),
    iUSER_PASSWORD VARCHAR(100),
    iROLE_ID INT,
    iPERSON_NAME VARCHAR(100),
    iSIGNATURE_FILE_NAME VARCHAR(100)
)
BEGIN
    DECLARE vPERSON_ID INT;

    START TRANSACTION;

    INSERT IGNORE INTO person (PERSON_ID, PERSON_NAME, SIGNATURE_FILE_NAME) VALUES
        (NULL, iPERSON_NAME, iSIGNATURE_FILE_NAME);

    SET vPERSON_ID = (SELECT PERSON_ID FROM person WHERE PERSON_NAME = iPERSON_NAME);

    REPLACE INTO users (USER_ID, USER_LOGIN, USER_PASSWORD, ROLE_ID, PERSON_ID) VALUES
        (NULL, iUSER_LOGIN, iUSER_PASSWORD, iROLE_ID, vPERSON_ID);

    COMMIT;

END$$

/*
Корректировка отчета
*/
DROP PROCEDURE IF EXISTS ROOMCHECK_CORRECTION$$
CREATE PROCEDURE ROOMCHECK_CORRECTION(
    iROOMCHECK_ID INT
)
BEGIN
    DECLARE vROOMCHECK_FROM, vROOMCHECK_TO DATETIME;
    
    SELECT 
            ROOMCHECK_FROM, ROOMCHECK_TO 
                INTO 
                vROOMCHECK_FROM, vROOMCHECK_TO
        FROM
            roomcheck 
        WHERE
            ROOMCHECK_ID = iROOMCHECK_ID;
    
    -- формируем список датчиков
    DROP TEMPORARY TABLE IF EXISTS tt_device_lst;
    CREATE TEMPORARY TABLE tt_device_lst AS
        SELECT
                D.DEVICE_ID,
                A.ALARM_GZONE_FROM,
                A.ALARM_GZONE_TO,
                A.ALARM_YZONE_FROM,
                A.ALARM_YZONE_TO
            FROM
                roomcheck RCH
                LEFT JOIN roomchdevt RCHDT
                    ON RCH.ROOMCHECK_ID = RCHDT.ROOMCHECK_ID
                JOIN device D
                    ON RCH.ROOM_ID = D.ROOM_ID
                    AND (
                        RCHDT.DEVICETYPE_ID = D.DEVICETYPE_ID 
                        OR RCHDT.DEVICETYPE_ID IS NULL)
                LEFT JOIN alarm A
                    ON D.DEVICE_ID = A.REASON_DEVICE_ID
            WHERE
                RCH.ROOMCHECK_ID = iROOMCHECK_ID
            GROUP BY 
                D.DEVICE_ID;
                
    DROP TEMPORARY TABLE IF EXISTS tt_avgind_prelst;
    CALL EXEC_SQL(CONCAT_WS(' ',"
        CREATE TEMPORARY TABLE tt_avgind_prelst AS
            SELECT
                    I.DEVICE_ID,
                    I.INDICATION_ID,
                    I.INDICATION_VALUE,
                    CAST(NULL AS DECIMAL(14,6)) AS INDICATION_CORRECTED_VALUE
                FROM
                      avgind I
                        WHERE I.DEVICE_ID IN (",(SELECT GROUP_CONCAT(D.DEVICE_ID) FROM tt_device_lst D),")
                        AND I.INDICATION_REGDATE >= '",vROOMCHECK_FROM,"'
                        AND I.INDICATION_REGDATE < '",vROOMCHECK_TO,"'"));
    
    -- формируем список показаний по датчикам
    DROP TEMPORARY TABLE IF EXISTS tt_avgind_lst;
    CREATE TEMPORARY TABLE tt_avgind_lst AS
        SELECT
                D.DEVICE_ID,
                I.INDICATION_ID,
                I.INDICATION_VALUE,
                D.ALARM_GZONE_FROM,
                D.ALARM_GZONE_TO,
                D.ALARM_YZONE_FROM,
                D.ALARM_YZONE_TO,
                @CENTER := (D.ALARM_YZONE_FROM + D.ALARM_YZONE_TO) / 2 AS ALARM_CENTER,
                @RADIUS := (D.ALARM_YZONE_TO - D.ALARM_YZONE_FROM) / 2 AS ALARM_RADIUS,
                I.INDICATION_VALUE - @CENTER AS DELTA,
                CAST(NULL AS DECIMAL(14,6)) AS INDICATION_CORRECTED_VALUE
            FROM
                tt_device_lst D
                JOIN tt_avgind_prelst I
                    ON D.DEVICE_ID = I.DEVICE_ID;
    
    CREATE INDEX IDX_tt_avgind_lst_id ON tt_avgind_lst (INDICATION_ID);
    CREATE INDEX IDX_tt_avgind_lst_dev ON tt_avgind_lst (DEVICE_ID);
    
    -- формируем список сжимающих коеффициентов
    DROP TEMPORARY TABLE IF EXISTS tt_k_compression;
    CREATE TEMPORARY TABLE tt_k_compression AS
        SELECT
                DEVICE_ID,
                -- оклонение от центра поделить на радиус зоны
                GREATEST(MAX(ABS(DELTA/ALARM_RADIUS)),1) AS KK
            FROM
                tt_avgind_lst
            GROUP BY
                DEVICE_ID;
    
    
    -- генерируем новое показание
    UPDATE 
            tt_avgind_lst I
            JOIN tt_k_compression KC
                ON I.DEVICE_ID = KC.DEVICE_ID
        SET 
            I.INDICATION_CORRECTED_VALUE = I.ALARM_CENTER + DELTA / KC.KK;
    
    -- собственно корректировка
    START TRANSACTION;
    
    UPDATE roomcheck SET ROOMCHECK_CORRECTED = 1 WHERE ROOMCHECK_ID = iROOMCHECK_ID;
    
    UPDATE 
            avgind I
            JOIN tt_avgind_lst TI
                ON I.INDICATION_ID = TI.INDICATION_ID
        SET
            -- берем по приоритетно:
            I.INDICATION_CORRECTED_VALUE = COALESCE(TI.INDICATION_CORRECTED_VALUE, I.INDICATION_CORRECTED_VALUE);
    

    COMMIT;

END$$

/*
Корректировка всех старых показаний
*/
DELIMITER $$
DROP PROCEDURE IF EXISTS __CORRECTION_OLD_ALL$$
CREATE PROCEDURE __CORRECTION_OLD_ALL()
BEGIN
  DECLARE vCOUNT_ALL, vCOUNTER INT;
  
  DROP TEMPORARY TABLE IF EXISTS tt_roomcheck_to_correct_lst;
  CREATE TEMPORARY TABLE tt_roomcheck_to_correct_lst AS
        SELECT ROOMCHECK_ID FROM roomcheck WHERE ROOMCHECK_CORRECTED = 0;
  
  SET vCOUNT_ALL = (SELECT COUNT(*) FROM tt_roomcheck_to_correct_lst);
  SET vCOUNTER = 0;
    
  -- Блок может быть вложенным в любой другой блок (LOOP, WHILE, IF, BEGIN etc)
  CURSOR_BLOCK : BEGIN -- Все переменные, объявленные в этом блоке, находятся в области видимости блока
    DECLARE vROOMCHECK_ID INT;
    DECLARE cCUR CURSOR FOR SELECT ROOMCHECK_ID FROM tt_roomcheck_to_correct_lst;
    DECLARE EXIT HANDLER FOR NOT FOUND CLOSE cCUR; -- EXIT HANDLER выкинет из блока "CURSOR_BLOCK", процедура при этом продолжит выполнение
    OPEN cCUR;
    CURSOR_LOOP : LOOP
      FETCH cCUR INTO vROOMCHECK_ID;
      SET vCOUNTER = vCOUNTER + 1;
      SELECT CONCAT_WS(' ', vCOUNTER, '/', vCOUNT_ALL) AS PROCESSING, vROOMCHECK_ID AS ROOMCHECK_ID;
      -- SELECT 'Я ничего не откорректировал, это ТЕСТ! Измени процедуру!';
      CALL ROOMCHECK_CORRECTION(vROOMCHECK_ID);
    END LOOP CURSOR_LOOP;
    
    SELECT -1 AS RES; -- ! Этот код никогда не выполнится !
  END CURSOR_BLOCK;
  SELECT 1 AS RES; -- Этот код будет выполняться всегда


END$$

/*
DELIMITER ;
CALL __CORRECTION_OLD_ALL;
*/

/*
Создание пользователя
*/
DROP FUNCTION IF EXISTS ROOMCHECK_NAME$$
CREATE FUNCTION ROOMCHECK_NAME(
    iROOMCHECK_ID INT
)
RETURNS TEXT
BEGIN
        RETURN (
            SELECT 
                CONCAT_WS(' ', 
                    'Отчет от', T.DATE_CREATE, 
                    'c', T.ROOMCHECK_FROM, 
                    'по', T.ROOMCHECK_TO, 
                    CONCAT('(пользователь ', IFNULL(P.PERSON_NAME, '<пусто>'),')')
                )
            FROM
                roomcheck T
                LEFT JOIN person P
                    ON T.PERSON_ID = P.PERSON_ID
            WHERE
                T.ROOMCHECK_ID = iROOMCHECK_ID
        );

END$$



/*
Генерация усреднненого показания
*/
DROP PROCEDURE IF EXISTS CREATE_AVGIND$$
CREATE PROCEDURE CREATE_AVGIND(
    iDEVICE_ID INT,     -- ID датчика
    iFROM DATETIME,     -- дата-время, С которой берем показания для генерации
    iTO DATETIME,       -- дата-время, ПО которую берем показания для генерации
    iREGDATE DATETIME   -- дата-время, которое будет у сгенерированного показания
)
PROC : BEGIN
    DECLARE vINDICATIONS_COUNT INT;
    DECLARE vINDICATIONS_SUMMA DECIMAL(14,6);
    DECLARE vNEW_VALUE DECIMAL(14,6);
    
    DROP TEMPORARY TABLE IF EXISTS tt_indications;
    CREATE TEMPORARY TABLE tt_indications AS
        SELECT
                INDICATION_ID,
                INDICATION_VALUE,
                INDICATION_REGDATE
            FROM
                indication
            WHERE
                DEVICE_ID = iDEVICE_ID
                AND INDICATION_REGDATE >= iFROM
                AND INDICATION_REGDATE < iTO;
    
    SELECT
            COUNT(*), SUM(INDICATION_VALUE) INTO vINDICATIONS_COUNT, vINDICATIONS_SUMMA
        FROM
            tt_indications;
    
    DROP TEMPORARY TABLE tt_indications;
    
    IF vINDICATIONS_COUNT = 0 THEN
        SELECT 0 AS RES, CONCAT_WS(' ', 'Нет показаний за интервал c', iFROM, 'по', iTO) AS MSG;
        LEAVE PROC;
    END IF;
    
    SET vNEW_VALUE = vINDICATIONS_SUMMA / vINDICATIONS_COUNT;

    START TRANSACTION;

    INSERT INTO avgind (DEVICE_ID, INDICATION_VALUE, INDICATION_REGDATE) VALUES
        (iDEVICE_ID, vNEW_VALUE, iREGDATE);

    COMMIT;
    
    SELECT 1 AS RES, LAST_INSERT_ID() AS INDICATION_ID, vNEW_VALUE AS VAL;

END$$


DROP PROCEDURE IF EXISTS DEVICE_INDICATION_FROM_TO_LST$$
CREATE PROCEDURE DEVICE_INDICATION_FROM_TO_LST(
    iDEVICE_ID INT,
    iFROM DATETIME,
    iTO DATETIME,
    iCORRECTED BOOLEAN
)
BEGIN

    SELECT
            INDICATION_ID,
            DEVICE_ID,
            IF(
                 iCORRECTED,
                 COALESCE(INDICATION_CORRECTED_VALUE, INDICATION_VALUE),
                 INDICATION_VALUE
            ) AS INDICATION_VALUE,
            INDICATION_REGDATE
        FROM
            avgind I
        WHERE
            DEVICE_ID = iDEVICE_ID
            AND INDICATION_REGDATE > iFROM
            AND INDICATION_REGDATE < iTO
        ORDER BY
            INDICATION_REGDATE;

END$$


DROP FUNCTION IF EXISTS CORRECT_AVGIND$$
CREATE FUNCTION CORRECT_AVGIND(
    iINDICATION_ID INT
)
RETURNS DECIMAL(14,6)
BEGIN
    
    RETURN (
        SELECT 
                INDICATION_VALUE 
            FROM (
                SELECT
                        -- реальное показание
                        @REAL_INDICATION_VALUE := COALESCE(
                            I.INDICATION_CORRECTED_VALUE, I.INDICATION_VALUE) AS REAL_INDICATION_VALUE,
                        -- нужно ли значение корректировать
                        @IS_VALUE_TO_CORRECT := (
                            @REAL_INDICATION_VALUE > A.ALARM_YZONE_TO
                            OR @REAL_INDICATION_VALUE < A.ALARM_YZONE_FROM
                        ) AS IS_VALUE_CORRECTED,
                        -- скорректированное показание
                        IF(
                            @IS_VALUE_TO_CORRECT,
                            IF(@REAL_INDICATION_VALUE < A.ALARM_YZONE_FROM, 
                                A.ALARM_YZONE_FROM + RAND() * (A.ALARM_GZONE_FROM - A.ALARM_YZONE_FROM),
                                A.ALARM_YZONE_TO - RAND() * (A.ALARM_YZONE_TO - A.ALARM_GZONE_TO)
                            ),
                            @REAL_INDICATION_VALUE
                        ) AS INDICATION_VALUE,
                        I.INDICATION_REGDATE
                    FROM
                        avgind I
                        LEFT JOIN alarm A
                            ON A.REASON_DEVICE_ID = I.DEVICE_ID
                    WHERE
                        I.INDICATION_ID = iINDICATION_ID
        ) T);

END$$


/*
Получить список Датчиков участвующих в Отчете
*/
DROP PROCEDURE IF EXISTS ROOMCHECK_DEVICE_LST$$
CREATE PROCEDURE ROOMCHECK_DEVICE_LST(
    iROOMCHECK_ID INT            -- ID Отчета
)
BEGIN

    SELECT
            D.DEVICE_ID,
            D.DEVICETYPE_ID,
            D.ROOM_ID,
            D.DEVICE_NAME,
            D.DEVICE_IP,
            D.DEVICE_COM,
            D.DEVICE_COM_SPEED,
            DS.DEVICE_ADDRESS,
            DT.DEVICE_BYTE_START,
            DT.DEVICE_BYTE_COUNT,
            A.ALARM_ID,
            A.REASON_DEVICE_ID,
            A.ACTION_DEVICE_ID,
            A.ALARM_GZONE_FROM,
            A.ALARM_GZONE_TO,
            A.ALARM_YZONE_FROM,
            A.ALARM_YZONE_TO,
            DT.DEVICETYPE_ABBR,
            DT.DEVICETYPE_NAME
        FROM
            roomcheck RCH
            LEFT JOIN roomchdevt RCHDT
                ON RCH.ROOMCHECK_ID = RCHDT.ROOMCHECK_ID
            JOIN device D
                ON RCH.ROOM_ID = D.ROOM_ID
                AND (
                    RCHDT.DEVICETYPE_ID = D.DEVICETYPE_ID 
                    OR RCHDT.DEVICETYPE_ID IS NULL)
            LEFT JOIN devicespec DS
                ON D.DEVICESPEC_ID = DS.DEVICESPEC_ID
            LEFT JOIN alarm A
                ON A.REASON_DEVICE_ID = D.DEVICE_ID
            JOIN devicetype DT
                ON DT.DEVICETYPE_ID = D.DEVICETYPE_ID
        WHERE
            RCH.ROOMCHECK_ID = iROOMCHECK_ID
        ;
    
END$$


/*
Получить список Типов датчиков в комнате
*/
DROP PROCEDURE IF EXISTS ROOM_DEVICETYPE_LST$$
CREATE PROCEDURE ROOM_DEVICETYPE_LST(
    iROOM_ID INT,             -- ID комнаты
    iDEVICETYPE_REPORTING INT -- 0 только "системные"/ не отчетные, 1 - только отчетные, NULL - все
)
BEGIN

    SELECT
            DT.DEVICETYPE_ID,
            DT.DEVICETYPE_ABBR,
            DT.DEVICETYPE_NAME
        FROM
            device D
            JOIN devicetype DT
                ON DT.DEVICETYPE_ID = D.DEVICETYPE_ID
        WHERE
            D.ROOM_ID = iROOM_ID
            AND IF(iDEVICETYPE_REPORTING IS NOT NULL, DT.DEVICETYPE_REPORTING = iDEVICETYPE_REPORTING, TRUE)
        GROUP BY 
            DT.DEVICETYPE_ID
        ;
    
END$$


/*
Получить список Отчетов
*/
DROP PROCEDURE IF EXISTS DEVICE_CNG$$
CREATE PROCEDURE DEVICE_CNG(
    iDEVICE_ID INT, 
    iDEVICESPEC_ID INT
)
PROC : BEGIN
    -- проверка на существование
    IF NOT EXISTS (SELECT 1 FROM device WHERE DEVICE_ID = iDEVICE_ID) THEN
        SELECT 'Не существует Датчика с таким ID' AS MSG, 0 AS RES;
        LEAVE PROC;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM devicespec WHERE DEVICESPEC_ID = iDEVICESPEC_ID) THEN
        SELECT 'Не существует Спецификации Датчика с таким ID' AS MSG, 0 AS RES;
        LEAVE PROC;
    END IF;
    
    UPDATE
            device
        SET
            DEVICESPEC_ID = iDEVICESPEC_ID
        WHERE
            DEVICE_ID = iDEVICE_ID;
    
END$$

/*
Получить список Отчетов
*/
DROP PROCEDURE IF EXISTS ROOMCHECK_LST$$
CREATE PROCEDURE ROOMCHECK_LST(
    iFROM DATETIME,      -- дата С
    iTO DATETIME,        -- дата По
    iLIMIT INT,          -- Количество 
    iOFFSET INT          -- Сдвиг
    
)
PROC : BEGIN
    -- проверка на заполненность
    IF iLIMIT IS NULL OR iOFFSET IS NULL THEN
        SELECT 'LIMIT и OFFSET обязательны к заполнению' AS MSG, 0 AS RES;
        LEAVE PROC;
    END IF;
    -- проверка назначения limit и offset
    IF NOT(iLIMIT > 0 AND iLIMIT < 1000) THEN
        SELECT 'LIMIT имеет некорректное значение (должен быть: 0 < LIMIT < 1000)' AS MSG, 0 AS RES;
        LEAVE PROC;
    END IF;
    IF NOT(iOFFSET >= 0) THEN
        SELECT 'OFFSET имеет некорректное значение (должен быть: 0 < OFFSET)' AS MSG, 0 AS RES;
        LEAVE PROC;
    END IF;
    
    SET @gen_COLUMNS = 'COUNT(*)';
    
    SET @gen_WHERE = CONCAT_WS(' ',
                IF(iFROM IS NOT NULL, CONCAT('RCH.ROOMCHECK_FROM >= "', iFROM, '"'), 'TRUE'),' AND
                ',IF(iTO IS NOT NULL, CONCAT('RCH.ROOMCHECK_FROM < "', iTO, '"'), 'TRUE'));
    
    CALL EXEC_SQL(CONCAT_WS(' ','SET @res_COUNT = (SELECT COUNT(*) FROM roomcheck RCH WHERE',@gen_WHERE,')'));
    
    CALL EXEC_SQL(
        CONCAT_WS(' ',@gen_SELECT_BODY,'
            SELECT
                @res_COUNT AS _COUNT,
                RCH.ROOMCHECK_ID,
                RCH.ROOM_ID,
                RCH.ROOMCHECK_FROM,
                RCH.ROOMCHECK_TO,
                RCH.VERIFIED,
                RCH.DATE_CREATE,
                RCH.DATE_UPDATE,
                RCH.ROOMCHECK_CORRECTED,
                RCH.PERSON_ID,
                P.PERSON_NAME,
                R.ROOM_NAME
            FROM
                roomcheck RCH
                JOIN person P
                    ON RCH.PERSON_ID = P.PERSON_ID
                JOIN room R
                    ON RCH.ROOM_ID = R.ROOM_ID
            WHERE
                ',@gen_WHERE,'
            ORDER BY 
                RCH.ROOMCHECK_FROM DESC
            LIMIT
                ', iLIMIT ,'
            OFFSET 
                ', iOFFSET
            ));
    
END$$


/*
Получить список Отчетов
*/
DROP PROCEDURE IF EXISTS ROOMCHECK_ITM$$
CREATE PROCEDURE ROOMCHECK_ITM(
    iROOMCHECK_ID INT

)
PROC : BEGIN
    -- проверка на заполненность
    IF NOT EXISTS (SELECT 1 FROM roomcheck WHERE ROOMCHECK_ID = iROOMCHECK_ID) THEN
        SELECT 'Не найден отчет таким ID' AS MSG, 0 AS RES;
        LEAVE PROC;
    END IF;
    SELECT
                RCH.ROOMCHECK_ID,
                RCH.ROOM_ID,
                RCH.ROOMCHECK_FROM,
                RCH.ROOMCHECK_TO,
                RCH.VERIFIED,
                RCH.DATE_CREATE,
                RCH.DATE_UPDATE,
                RCH.ROOMCHECK_CORRECTED,
                RCH.PERSON_ID,
                P.PERSON_NAME,
                R.ROOM_NAME,
                CAST(GROUP_CONCAT(DT.DEVICETYPE_ID) AS CHAR) AS DEVICETYPE_IDS
            FROM
                roomcheck RCH
                JOIN person P
                    ON RCH.PERSON_ID = P.PERSON_ID
                JOIN room R
                    ON RCH.ROOM_ID = R.ROOM_ID
                LEFT JOIN roomchdevt RCHDT
                    ON RCHDT.ROOMCHECK_ID = RCH.ROOMCHECK_ID
                JOIN devicetype DT
                    ON RCHDT.DEVICETYPE_ID = DT.DEVICETYPE_ID
                    OR RCHDT.DEVICETYPE_ID IS NULL
            WHERE
                RCH.ROOMCHECK_ID = iROOMCHECK_ID
            GROUP BY
                RCH.ROOMCHECK_ID;

END$$


/*
Добавление отчета
*/    
DROP PROCEDURE IF EXISTS ROOMCHECK_ADD$$
CREATE PROCEDURE ROOMCHECK_ADD(
    iROOM_ID INT,               -- ID помещения, по которому создается отчет
    iFROM DATETIME,             -- дата-время отчета С
    iTO DATETIME,               -- дата-время отчета ПО
    iUSER_ID INT,               -- ID пользователя, создающего Отчет
    iVERIFIED INT,              -- флаг одобренности Отчета
    iDEVICETYPES VARCHAR(255),  -- список Типов датчиков, которые нужно выводить в отчете
                                --     (формат через запятую: '1,2,17,103')
                                --     или NULL или '' если выводить нужно все
    iROOMCHECK_DESC TEXT        -- примечание отчета
)
ROOMCHECK_ADD : BEGIN
    DECLARE 
        vPERSON_ID, 
        vROOMCHECK_ID, 
        vDEVICETYPES_FINDED_COUNT,
        vDEVICETYPES_COMMA_COUNT INT;
    
    SET iDEVICETYPES = NULLIF(TRIM(iDEVICETYPES),'');

    SET vPERSON_ID = (SELECT PERSON_ID FROM users WHERE USER_ID = iUSER_ID);
    
    IF iFROM >= iTO THEN
        SELECT 'Дата начала должна быть меньше даты конца' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF vPERSON_ID IS NULL THEN
        SELECT 'Не найдена персона' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF iFROM IS NULL OR iTO IS NULL THEN
        SELECT 'Дата начала и дата конца должны быть заполнены' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF IFNULL(iVERIFIED,-1) NOT IN (0,1) THEN
        SELECT 'Флаг согласован/не согласован должен быть заполнен 1 или 0' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM room WHERE ROOM_ID = iROOM_ID) THEN
        SELECT 'Не найдено помещение' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    
    SET vDEVICETYPES_COMMA_COUNT = CHAR_LENGTH(iDEVICETYPES) - CHAR_LENGTH(REPLACE(iDEVICETYPES,',',''));
    SET vDEVICETYPES_FINDED_COUNT = (SELECT COUNT(*) FROM devicetype WHERE FIND_IN_SET(DEVICETYPE_ID, iDEVICETYPES));
    
    IF iDEVICETYPES IS NOT NULL AND vDEVICETYPES_COMMA_COUNT + 1 <> vDEVICETYPES_FINDED_COUNT THEN
        SELECT 'Некоректные значения Типов датчиков' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    
    
    -- получаем список Отчетов, пересекающихся с создаваемым
    DROP TEMPORARY TABLE IF EXISTS tt_cross_roomheck;
    CREATE TEMPORARY TABLE tt_cross_roomheck AS
        SELECT
                ROOMCHECK_ID
            FROM
                roomcheck 
            WHERE
                ROOM_ID = iROOM_ID
                AND ROOMCHECK_FROM < iTO
                AND ROOMCHECK_TO > iFROM;
    -- если есть пересекающиеся Отчеты - выводим ошибку
    IF EXISTS (SELECT 1 FROM tt_cross_roomheck) THEN
        SELECT 
                CONCAT_WS(' ',
                    'Пересечение с уже существующими Отчетами:',
                    GROUP_CONCAT(
                        ROOMCHECK_NAME(T.ROOMCHECK_ID)
                    )
                ) AS ERR
            FROM
                tt_cross_roomheck T;
        LEAVE ROOMCHECK_ADD;
    END IF;
    
    
    START TRANSACTION;        
    
    INSERT INTO roomcheck (ROOM_ID, PERSON_ID, ROOMCHECK_FROM, ROOMCHECK_TO, VERIFIED, DATE_CREATE, ROOMCHECK_DESC)
        VALUES (iROOM_ID, vPERSON_ID, iFROM, iTO, iVERIFIED, NOW(), iROOMCHECK_DESC);
    
    SET vROOMCHECK_ID = LAST_INSERT_ID();
    
    INSERT INTO roomchdevt (ROOMCHECK_ID, DEVICETYPE_ID)
        SELECT
                DISTINCT vROOMCHECK_ID, DEVICETYPE_ID
            FROM
                devicetype
            WHERE
                FIND_IN_SET(DEVICETYPE_ID, iDEVICETYPES);
    
    COMMIT;
    
    SELECT vROOMCHECK_ID AS RES;
    
END$$


/*
Добавление отчета
*/    
DROP PROCEDURE IF EXISTS ROOMCHECK_CNG$$
CREATE PROCEDURE ROOMCHECK_CNG(
    iROOMCHECK_ID INT,          -- ID отчета
    
    iFROM DATETIME,             -- дата-время отчета С
    iTO DATETIME,               -- дата-время отчета ПО
    iUSER_ID INT,               -- ID пользователя, создающего Отчет
    iVERIFIED INT,              -- флаг одобренности Отчета
    iDEVICETYPES VARCHAR(255),  -- список Типов датчиков, которые нужно выводить в отчете
                                --     (формат через запятую: '1,2,17,103')
                                --     или NULL или '' если выводить нужно все
    iROOMCHECK_DESC TEXT        -- примечание отчета
)
ROOMCHECK_ADD : BEGIN
    DECLARE 
        vROOM_ID,
        vPERSON_ID, 
        vDEVICETYPES_FINDED_COUNT,
        vDEVICETYPES_COMMA_COUNT INT;
    
    SET iDEVICETYPES = NULLIF(TRIM(iDEVICETYPES),'');

    SET vPERSON_ID = (SELECT PERSON_ID FROM users WHERE USER_ID = iUSER_ID);
    SET vROOM_ID = (SELECT ROOM_ID FROM roomcheck WHERE ROOMCHECK_ID = iROOMCHECK_ID);
    
    IF iFROM >= iTO THEN
        SELECT 'Дата начала должна быть меньше даты конца' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF vPERSON_ID IS NULL THEN
        SELECT 'Не найдена персона' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF iFROM IS NULL OR iTO IS NULL THEN
        SELECT 'Дата начала и дата конца должны быть заполнены' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF IFNULL(iVERIFIED,-1) NOT IN (0,1) THEN
        SELECT 'Флаг согласован/не согласован должен быть заполнен 1 или 0' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM room WHERE ROOM_ID = vROOM_ID) THEN
        SELECT 'Не найдено помещение' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    
    SET vDEVICETYPES_COMMA_COUNT = CHAR_LENGTH(iDEVICETYPES) - CHAR_LENGTH(REPLACE(iDEVICETYPES,',',''));
    SET vDEVICETYPES_FINDED_COUNT = (SELECT COUNT(*) FROM devicetype WHERE FIND_IN_SET(DEVICETYPE_ID, iDEVICETYPES));
    
    IF iDEVICETYPES IS NOT NULL AND vDEVICETYPES_COMMA_COUNT + 1 <> vDEVICETYPES_FINDED_COUNT THEN
        SELECT 'Некоректные значения Типов датчиков' AS ERR;
        LEAVE ROOMCHECK_ADD;
    END IF;
    
    
    -- получаем список Отчетов, пересекающихся с создаваемым
    DROP TEMPORARY TABLE IF EXISTS tt_cross_roomheck;
    CREATE TEMPORARY TABLE tt_cross_roomheck AS
        SELECT
                ROOMCHECK_ID
            FROM
                roomcheck 
            WHERE
                ROOM_ID = vROOM_ID
                AND ROOMCHECK_FROM < iTO
                AND ROOMCHECK_TO > iFROM
                AND ROOMCHECK_ID <> iROOMCHECK_ID;
    -- если есть пересекающиеся Отчеты - выводим ошибку
    IF EXISTS (SELECT 1 FROM tt_cross_roomheck) THEN
        SELECT 
                CONCAT_WS(' ',
                    'Пересечение с уже существующими Отчетами:',
                    GROUP_CONCAT(
                        ROOMCHECK_NAME(T.ROOMCHECK_ID)
                    )
                ) AS ERR
            FROM
                tt_cross_roomheck T;
        LEAVE ROOMCHECK_ADD;
    END IF;
    
    
    START TRANSACTION;  
    
    UPDATE
            roomcheck
        SET
            PERSON_ID = vPERSON_ID, 
            ROOMCHECK_FROM = iFROM, 
            ROOMCHECK_TO = iTO, 
            VERIFIED = iVERIFIED, 
            DATE_UPDATE = NOW(),
            ROOMCHECK_DESC = iROOMCHECK_DESC
        WHERE
            ROOMCHECK_ID = iROOMCHECK_ID;
    
    DELETE FROM roomchdevt WHERE ROOMCHECK_ID = iROOMCHECK_ID;
    
    INSERT INTO roomchdevt (ROOMCHECK_ID, DEVICETYPE_ID)
        SELECT
                DISTINCT iROOMCHECK_ID, DEVICETYPE_ID
            FROM
                devicetype
            WHERE
                FIND_IN_SET(DEVICETYPE_ID, iDEVICETYPES);
    
    COMMIT;
    
    SELECT iROOMCHECK_ID AS RES;
    
END$$

DROP PROCEDURE IF EXISTS EXEC_SQL$$
CREATE PROCEDURE EXEC_SQL(
    iSQL TEXT
)
BEGIN

    SET @_SQL = iSQL;
    PREPARE STMT FROM @_SQL;
    EXECUTE STMT;
    DEALLOCATE PREPARE STMT;

END$$

DROP PROCEDURE IF EXISTS CALC_COMPLEX_INDICATION$$
CREATE PROCEDURE CALC_COMPLEX_INDICATION(
    iCOMPLEX_DEVICE_ID INT,
    iDATETIME DATETIME,
    iINTERVAL_SEC INT
)
PROC : BEGIN
    DECLARE vCOMPLEX_ID INT;
    DECLARE vALIAS, vFORMULA, vTMP VARCHAR(255);
    DECLARE vVAL DECIMAL(14,6);

    DECLARE done INT DEFAULT FALSE;
    DECLARE cur CURSOR FOR SELECT DEVICE_FALIAS, INDICATION_VALUE FROM tt_compind;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    SELECT COMPLEX_ID, COMPLEX_FORMULA INTO vCOMPLEX_ID, vFORMULA FROM complex WHERE DEVICE_ID = iCOMPLEX_DEVICE_ID;

    IF vCOMPLEX_ID IS NULL OR vFORMULA IS NULL THEN
        SELECT 0 AS RES, 'Не найдена информация о составном счетчике' AS MSG;
        LEAVE PROC;
    END IF;

    DROP TEMPORARY TABLE IF EXISTS tt_compdev;
    CREATE TEMPORARY TABLE tt_compdev AS
        SELECT
                CD.DEVICE_ID,
                CD.COMPLEX_ID,
                CD.DEVICE_FALIAS
            FROM
                compdev CD
            WHERE
                CD.COMPLEX_ID = vCOMPLEX_ID;

    IF (SELECT COUNT(*) FROM tt_compdev) = 0 THEN
        SELECT 0 AS RES, 'Не найдены дочерние счетчики составного' AS MSG;
        LEAVE PROC;
    END IF;

    DROP TEMPORARY TABLE IF EXISTS tt_compind;
    CREATE TEMPORARY TABLE tt_compind AS
          SELECT
                  TT.DEVICE_ID,
                  MAX(TT.COMPLEX_ID) AS COMPLEX_ID,
                  MAX(TT.DEVICE_FALIAS) AS DEVICE_FALIAS,
                  MAX(I.INDICATION_REGDATE) AS INDICATION_REGDATE,
                  CAST(NULL AS DECIMAL(16,6)) AS INDICATION_VALUE
              FROM
                  tt_compdev TT
                  LEFT JOIN indication I
                      ON TT.DEVICE_ID = I.DEVICE_ID
                      AND I.INDICATION_REGDATE BETWEEN (iDATETIME - INTERVAL iINTERVAL_SEC SECOND) AND iDATETIME
              GROUP BY
                  TT.DEVICE_ID;
      UPDATE
              tt_compind TT
              JOIN indication I
                  ON TT.DEVICE_ID = I.DEVICE_ID
                  AND TT.INDICATION_REGDATE = I.INDICATION_REGDATE
          SET
             TT.INDICATION_VALUE = I.INDICATION_VALUE;

    IF (SELECT COUNT(*) FROM tt_compind WHERE INDICATION_VALUE IS NULL) > 0 THEN
        SELECT 0 AS RES, 'Не все показания актуальны' AS MSG;
        LEAVE PROC;
    END IF;

    SET vTMP = vFORMULA;

    OPEN cur;

    read_loop: LOOP
        FETCH cur INTO vALIAS, vVAL;
        IF done THEN
          LEAVE read_loop;
        END IF;

        SET vTMP = REPLACE(vTMP, vALIAS, vVAL);

    END LOOP;

    CALL EXEC_SQL(CONCAT('SET @VAL = ', vTMP));

    INSERT INTO indication (DEVICE_ID, INDICATION_REGDATE, INDICATION_VALUE) VALUES (iCOMPLEX_DEVICE_ID, iDATETIME, @VAL);

    SELECT 1 AS RES, @VAL AS VAL;

END$$

DELIMITER ;
