USE gateman; 

SET @DEFAULT_ALARM_ADDR = NULL;

DROP TEMPORARY TABLE IF EXISTS tt_alarm;
CREATE TEMPORARY TABLE tt_alarm (ADDR INT, GFR INT, GTO INT, YFR INT, YTO INT, TYPE VARCHAR(100), REASON_DEVICE_ID INT, ALARM_DEVICE_ID INT, ERR TEXT);
INSERT INTO tt_alarm (ADDR, GFR, GTO, YFR, YTO, TYPE, ALARM_DEVICE_ID) VALUES
(13,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(14,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(16,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(17,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(18,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(19,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(20,11,26,10,27,'Температура', @DEFAULT_ALARM_ADDR),
(21,11,26,10,27,'Температура', @DEFAULT_ALARM_ADDR),
(22,11,26,10,27,'Температура', @DEFAULT_ALARM_ADDR),
(23,11,26,10,27,'Температура', @DEFAULT_ALARM_ADDR),
(24,11,26,10,27,'Температура', @DEFAULT_ALARM_ADDR),
(25,11,26,10,27,'Температура', @DEFAULT_ALARM_ADDR),
(26,16,24,15,25,'Температура', @DEFAULT_ALARM_ADDR),
(1,11,26,10,27,'Температура', 1223),
(2,11,26,10,27,'Температура', 1221),
(3,11,26,10,27,'Температура', 1229),
(4,11,26,10,27,'Температура', 1219),
(5,11,26,10,27,'Температура', 1218),
(6,11,26,10,27,'Температура', 1207),
(7,11,26,10,27,'Температура', 1204),
(8,11,26,10,27,'Температура', 1203),
(9,11,26,10,27,'Температура', 1306),
(10,11,26,10,27,'Температура', 1222),
(101,11,200,10,201,'Перепад давления', 1223),
(102,11,200,10,201,'Перепад давления', 1221),
(103,11,200,10,201,'Перепад давления', 1229),
(104,11,200,10,201,'Перепад давления', 1219),
(105,11,200,10,201,'Перепад давления', 1218),
(106,11,200,10,201,'Перепад давления', @DEFAULT_ALARM_ADDR),
(107,11,200,10,201,'Перепад давления', 1207),
(108,11,200,10,201,'Перепад давления', 1204),
(109,11,200,10,201,'Перепад давления', 1203),
(110,11,200,10,201,'Перепад давления', 1306),
(111,11,200,10,201,'Перепад давления', @DEFAULT_ALARM_ADDR),
(13,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(14,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(16,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(17,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(18,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(19,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(20,10,75,0,85,'Влажность', @DEFAULT_ALARM_ADDR),
(21,10,75,0,85,'Влажность', @DEFAULT_ALARM_ADDR),
(22,10,75,0,85,'Влажность', @DEFAULT_ALARM_ADDR),
(23,10,75,0,85,'Влажность', @DEFAULT_ALARM_ADDR),
(24,10,75,0,85,'Влажность', @DEFAULT_ALARM_ADDR),
(25,10,75,0,85,'Влажность', @DEFAULT_ALARM_ADDR),
(26,16,64,15,65,'Влажность', @DEFAULT_ALARM_ADDR),
(1,10,75,0,85,'Влажность', 1223),
(2,10,75,0,85,'Влажность', 1221),
(3,10,75,0,85,'Влажность', 1229),
(4,10,75,0,85,'Влажность', 1219),
(5,10,75,0,85,'Влажность', 1218),
(6,10,75,0,85,'Влажность', 1207),
(7,10,75,0,85,'Влажность', 1204),
(8,10,75,0,85,'Влажность', 1203),
(9,10,75,0,85,'Влажность', 1306),
(10,10,75,0,85,'Влажность', 1222);


UPDATE
        tt_alarm TT
        JOIN devicetype DT
            ON CASE TT.TYPE
                    WHEN 'Температура' THEN DT.DEVICETYPE_ID = 1
                    WHEN 'Влажность' THEN DT.DEVICETYPE_ID = 2
                    WHEN 'Перепад давления' THEN DT.DEVICETYPE_ID = 3
                END
        JOIN device D
            ON D.DEVICE_ADDRESS = TT.ADDR
            AND D.DEVICETYPE_ID = DT.DEVICETYPE_ID
    SET
        TT.REASON_DEVICE_ID = D.DEVICE_ID
    ;


UPDATE tt_alarm SET ERR = 'Не найден датчик' WHERE ERR IS NULL AND REASON_DEVICE_ID IS NULL;

SELECT ERR, COUNT(*) FROM tt_alarm GROUP BY ERR;

TRUNCATE alarm;

INSERT IGNORE INTO alarm (REASON_DEVICE_ID, ACTION_DEVICE_ID, ALARM_YZONE_FROM, ALARM_GZONE_FROM, ALARM_GZONE_TO, ALARM_YZONE_TO)
    SELECT 
            REASON_DEVICE_ID, ALARM_DEVICE_ID, YFR, GFR, GTO, YTO
        FROM
            tt_alarm
        WHERE 
            ERR IS NULL;

-- сигнализация на двери
INSERT IGNORE INTO alarm (REASON_DEVICE_ID, ACTION_DEVICE_ID, ALARM_YZONE_FROM, ALARM_GZONE_FROM, ALARM_GZONE_TO, ALARM_YZONE_TO) VALUES
(204 , 1204, 0,  0, 300, 300),
(207 , 1207, 0,  0, 300, 300),
(218 , 1218, 0,  0, 300, 300),
(219 , 1219, 0,  0, 300, 300),
(221 , 1221, 0,  0, 300, 300),
(306 , 1306, 0,  0, 300, 300)
;

-- составные датчики
INSERT IGNORE INTO alarm (REASON_DEVICE_ID, ACTION_DEVICE_ID, ALARM_YZONE_FROM, ALARM_GZONE_FROM, ALARM_GZONE_TO, ALARM_YZONE_TO) VALUES
(204229 , 1204, 11,200,10,201),
(207229 , 1207, 11,200,10,201),
(218229 , 1218, 11,200,10,201),
(219229 , 1219, 11,200,10,201),
(221229 , 1221, 11,200,10,201)
;
