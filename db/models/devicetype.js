var DB = require('./../db');
var Promise  = require('bluebird');

var Model = DB.Model.extend({
    tableName: 'devicetype',
    idAttribute: 'DEVICETYPE_ID',
},{
    getAll: Promise.method(function(){
        return this.fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    }),

    getByRoom: Promise.method(function(roomid){
        return this.query(function(qb) {
            qb.join('device','device.DEVICETYPE_ID','=','devicetype.DEVICETYPE_ID');
            qb.join('room','device.ROOM_ID','=','room.ROOM_ID');
            qb.where('room.ROOM_ID', roomid);
        }).fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    })
});

module.exports = Model;