var DB = require('./../db');
var Promise = require('bluebird');
var removeUtcOffset = require('../../utils').removeUtcOffset;

var getDevicesForRoomcheck = function (roomcheckId) {
    console.log('CALL ROOMCHECK_DEVICE_LST(?)', [roomcheckId]);
    return DB.knex.raw('CALL ROOMCHECK_DEVICE_LST(?)', [roomcheckId])
        .then(function (res) {
            return res[0][0]
        })
};

var switchStealthMode = function () {
    console.log('CALL SWITCH_STEALTH_MODE()', []);
    return DB.knex.raw('CALL SWITCH_STEALTH_MODE()', [])
};

var getStealthMode = function () {
    console.log('SELECT GET_SYSPARAM_VALUE("STEALTH_MODE") AS RES', []);
    return DB.knex.raw('SELECT GET_SYSPARAM_VALUE("STEALTH_MODE") AS RES', [])
        .then(function (res) {
            return res[0][0].RES
        })
};

var getRoomchecks = function (date_from, date_to, limit, offset) {
    console.log('CALL ROOMCHECK_LST(?, ?, ?, ?)', [date_from, date_to, limit, offset]);
    return DB.knex.raw('CALL ROOMCHECK_LST(?, ?, ?, ?)', [date_from, date_to, limit, offset])
        .then(function (res) {
            return res[0][0]
        })
};

var getRoomcheck = function (roomcheckId) {
    console.log('CALL ROOMCHECK_ITM(?)', [roomcheckId]);
    return DB.knex.raw('CALL ROOMCHECK_ITM(?)', [roomcheckId])
        .then(function (res) {
            return res[0][0][0]
        })
        .then(function (res) {
            if (res.RES === 0){
                throw new Error(res.MSG);
                return;
            } else {
                return res
            }
        })
};

var createRoomheck = function (roomId, date_from, date_to, user_id, verified, devicetypes, desc) {
    verified = verified ? 1 : 0;
    console.log('CALL ROOMCHECK_ADD(?, ?, ?, ?, ?, ?, ?)', [roomId, date_from, date_to, user_id, verified, devicetypes, desc]);
    return DB.knex.raw('CALL ROOMCHECK_ADD(?, ?, ?, ?, ?, ?, ?)', [roomId, date_from, date_to, user_id, verified, devicetypes, desc])
        .then(function (res) {
            return res[0][0][0]
        })
        .then(function (res) {
            if (!res.RES){
                throw new Error(res.ERR);
                return;
            } else {
                return res.RES
            }
        })
};

var updateRoomcheck = function (roomcheck_id, date_from, date_to, user_id, verified, devicetypes, desc) {
    verified = verified ? 1 : 0;
    console.log('CALL ROOMCHECK_CNG(?, ?, ?, ?, ?, ?, ?)', [roomcheck_id, date_from, date_to, user_id, verified, devicetypes, desc]);
    return DB.knex.raw('CALL ROOMCHECK_CNG(?, ?, ?, ?, ?, ?, ?)', [roomcheck_id, date_from, date_to, user_id, verified, devicetypes, desc])
        .then(function (res) {
            return res[0][0][0]
        })
        .then(function (res) {
            if (!res.RES){
                throw new Error(res.ERR);
                return;
            } else {
                return res.RES
            }
        })
};

var getIndicationsByDeviceFromTo = function (deviceId, date_from, date_to, corrected) {
    console.log('CALL DEVICE_INDICATION_FROM_TO_LST(?, ?, ?, ?)', [deviceId, date_from, date_to, corrected]);
    return DB.knex.raw('CALL DEVICE_INDICATION_FROM_TO_LST(?, ?, ?, ?)', [deviceId, date_from, date_to, corrected])
        .then(function (res) {
            return res[0][0]
        })
};

var getRoomDevicetypes = function (roomId, reporting) {
    console.log('CALL ROOM_DEVICETYPE_LST(?, ?)', [roomId, reporting]);
    return DB.knex.raw('CALL ROOM_DEVICETYPE_LST(?, ?)', [roomId, reporting])
        .then(function (res) {
            return res[0][0]
        })
};

var amendRoomcheck = function (roomcheckId) {
    console.log('CALL ROOMCHECK_CORRECTION(?)', [roomcheckId]);
    return DB.knex.raw('CALL ROOMCHECK_CORRECTION(?)', [roomcheckId])
};

var editDevice = function (deviceid, devicespecid) {
    console.log('CALL DEVICE_CNG(?, ?)', [deviceid, devicespecid]);
    return DB.knex.raw('CALL DEVICE_CNG(?, ?)', [deviceid, devicespecid])
};

var calcComplexIndication = function (deviceId, date, oldIntervalInMilliseconds) {
    var newDate = removeUtcOffset(date);
    var secInterval = Math.round(oldIntervalInMilliseconds/1000);
    console.log('CALL CALC_COMPLEX_INDICATION(?, ?, ?)', [deviceId, newDate, secInterval]);
    return DB.knex.raw('CALL CALC_COMPLEX_INDICATION(?, ?, ?)', [deviceId, newDate, secInterval])
        .then(function (res) {
            return res[0][0][0] 
        })
        .then(function (res) {
            if (!res.RES){
                throw new Error(res.MSG);
                return;
            } else {
                return res.VAL
            }
        })
};

var calcAverageIndication = function (deviceId, date, oldIntervalInMilliseconds) {
    var fromDate = new Date(date);
    fromDate.setMilliseconds(fromDate.getMilliseconds()-oldIntervalInMilliseconds);

    var fromDateClean = removeUtcOffset(fromDate);
    var newDate = removeUtcOffset(date);

    console.log('CALL CREATE_AVGIND(?, ?, ?, ?)', [deviceId, fromDateClean, newDate, newDate]);
    return DB.knex.raw('CALL CREATE_AVGIND(?, ?, ?, ?)', [deviceId, fromDateClean, newDate, newDate])
        .then(function (res) {
            return res[0][0][0]
        })
        .then(function (res) {
            if (!res.RES){
                throw new Error(res.MSG);
                return;
            } else {
                return res.VAL
            }
        })
};

module.exports = {
    switchStealthMode: switchStealthMode,
    getStealthMode: getStealthMode,
    createRoomheck: createRoomheck,
    getIndicationsByDeviceFromTo: getIndicationsByDeviceFromTo,
    calcComplexIndication: calcComplexIndication,
    calcAverageIndication: calcAverageIndication,
    getRoomchecks: getRoomchecks,
    getDevicesForRoomcheck: getDevicesForRoomcheck,
    getRoomcheck: getRoomcheck,
    amendRoomcheck: amendRoomcheck,
    editDevice: editDevice,
    getRoomDevicetypes: getRoomDevicetypes,
    updateRoomcheck: updateRoomcheck
};
