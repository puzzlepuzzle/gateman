var DB = require('./../db');
var Promise  = require('bluebird');

var Model = DB.Model.extend({
    tableName: 'person',
    idAttribute: 'PERSON_ID',
},{
    getById: Promise.method(function (id) {
        return this.where({PERSON_ID: id}).fetch()
            .then(function (c) {
                return c.toJSON()
            })
    })
});

module.exports = Model;