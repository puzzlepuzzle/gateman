var DB = require('./../db');
var Promise  = require('bluebird');

var Model = DB.Model.extend({
    tableName: 'devicespec',
    idAttribute: 'DEVICESPEC_ID',
},{
    getAll: Promise.method(function(){
        return this.fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    })
});

module.exports = Model;
