var DB = require('./../db');
var Promise  = require('bluebird');

var Model = DB.Model.extend({
    tableName: 'room',
    idAttribute: 'ROOM_ID',
},{
    getById: Promise.method(function (id) {
        return this.where({ROOM_ID: id}).fetch()
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getAll: Promise.method(function(){
        return this.fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    })
});

module.exports = Model;