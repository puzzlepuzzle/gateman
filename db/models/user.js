var DB = require('./../db');
var bcrypt = require('bcrypt-nodejs');
var Promise  = require('bluebird');
var Person = require("./person");

var User = DB.Model.extend({
    tableName: 'users',
    idAttribute: 'USER_ID',

    // generating a hash
    generateHash: function (password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    },
    person: function () {
        return this.belongsTo(Person, 'PERSON_ID');
    },
    // checking if password is valid
    validPassword: function (password, savedPassword) {
        //console.log('validPassword', password, savedPassword, bcrypt.compareSync(password, savedPassword))
        return bcrypt.compareSync(password, savedPassword);
    }
},{
    getPerson: Promise.method(function (userid) {
        return this.query(function (qb) {
            qb.where('USER_ID', userid);
        }).fetch({withRelated: ['person']})
            .then(function (c) {
                if (c){
                    return {
                        user: c.toJSON(),
                        person: c.related('person').toJSON()
                    }
                }
            })
    })
});

module.exports = User;