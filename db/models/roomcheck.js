var DB = require('./../db');
var Promise = require('bluebird');
var Room = require("./room");
var Person = require("./person");

var Model = DB.Model.extend({
    tableName: 'roomcheck',
    idAttribute: 'ROOMCHECK_ID',
    room: function () {
        return this.belongsTo(Room, 'ROOM_ID');
    },
    person: function () {
        return this.belongsTo(Person, 'PERSON_ID');
    }
}, {
    getById: Promise.method(function (id) {
        return this.where({ROOMCHECK_ID: id}).fetch({withRelated: ['room','person']})
            .then(function (c) {
                if (!c){
                    return null;
                }
                return {
                    roomcheck: c.toJSON(),
                    room: c.related('room').toJSON(),
                    person: c.related('person').toJSON()
                }
            })
    })
});

module.exports = Model;