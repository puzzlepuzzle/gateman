var DB = require('./../db');
var Promise = require('bluebird');
var Devicetype = require("./devicetype");
var Room = require("./room");
var Alarm = require("./alarm");
var DeviceSpec = require("./devicespec");

var Model = DB.Model.extend({
    tableName: 'device',
    idAttribute: 'DEVICE_ID',
    room: function () {
        return this.belongsTo(Room, 'ROOM_ID');
    },
    type: function () {
        return this.belongsTo(Devicetype, 'DEVICETYPE_ID');
    },
    alarm: function () {
        return this.hasOne(Alarm, 'REASON_DEVICE_ID');
    },
    devicespec: function () {
        return this.belongsTo(DeviceSpec, 'DEVICESPEC_ID');
    }
}, {
    getById: Promise.method(function (id) {
        return this.where({DEVICE_ID: id}).fetch()
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getAll: Promise.method(function () {
        return this.fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getByTypes: Promise.method(function (types) {
        return this.query(function (qb) {
            qb.whereIn('DEVICETYPE_ID', types);
        }).fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getByTypesWithRoomAndTypeAndSpec: Promise.method(function (types) {
        return this.query(function (qb) {
            qb.whereIn('DEVICETYPE_ID', types);
        }).fetchAll({withRelated: ['room', 'type', 'devicespec']})
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getByTypesWithAlram: Promise.method(function (types) {
        return this.query(function (qb) {
            qb.whereIn('DEVICETYPE_ID', types);
        }).fetchAll({withRelated: ['alarm', 'devicespec', 'type']})
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getByRoomWithAlram: Promise.method(function (roomId) {
        return this.query(function (qb) {
            qb.where('ROOM_ID', roomId);
        }).fetchAll({withRelated: ['alarm']})
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getByRoomWithAlarmAndType: Promise.method(function (roomId) {
        return this.query(function (qb) {
            qb.where('ROOM_ID', roomId);
            qb.orderBy('DEVICETYPE_ID');
        }).fetchAll({withRelated: ['alarm','type']})
            .then(function (c) {
                return c.toJSON()
            })
    }),

    getByIdAll: Promise.method(function (deviceid) {
        return this.query(function (qb) {
            qb.where('DEVICE_ID', deviceid);
        }).fetch({withRelated: ['room', 'type', 'devicespec']})
            .then(function (c) {
                return {
                    device: c.toJSON(),
                    type: c.related('type').toJSON(),
                    room: c.related('room').toJSON(),
                    spec: c.related('devicespec').toJSON()
                }
            })
    }),

    getByIdMinMax: Promise.method(function (deviceid) {
        return this.query(function (qb) {
            qb.where('DEVICE_ID', deviceid);
        }).fetch({withRelated: ['alarm']})
            .then(function (c) {
                var alarm = c.related('alarm').toJSON()
                return {
                    device: c.toJSON(),
                    alarm: {
                        gMax: alarm.ALARM_GZONE_TO,
                        gMin: alarm.ALARM_GZONE_FROM,
                        yMax: alarm.ALARM_YZONE_TO,
                        yMin: alarm.ALARM_YZONE_FROM
                    }
                }
            })
    }),

    getByIdAlarm: Promise.method(function (deviceid) {
        return this.query(function (qb) {
            qb.where('DEVICE_ID', deviceid);
        }).fetch({withRelated: ['alarm']})
            .then(function (c) {
                return c.related('alarm').toJSON()
            })
    })
});

module.exports = Model;
