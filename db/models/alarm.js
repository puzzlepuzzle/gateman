var DB = require('./../db');
var Promise  = require('bluebird');

var Model = DB.Model.extend({
    tableName: 'alarm',
    idAttribute: 'ALARM_ID',
},{
    getAll: Promise.method(function(){
        return this.fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    }),
    getByReasonDeviceId: Promise.method(function(id){
        return this.where({REASON_DEVICE_ID: id}).fetch()
            .then(function (c) {
                return c ? c.toJSON(): null;
            })
    }),
    getByActionDeviceId: Promise.method(function(id){
        return this.where({ACTION_DEVICE_ID: id}).fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    })
});

module.exports = Model;