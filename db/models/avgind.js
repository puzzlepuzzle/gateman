var DB = require('./../db');
var Promise = require('bluebird');
var Device = require("./device");

var Model = DB.Model.extend({
    tableName: 'avgind',
    device: function() {
        return this.belongsTo(Device, 'DEVICE_ID');
    }
}, {
    getLastByDevice: Promise.method(function (deviceId) {
        return this.query(function (qb) {
            qb.columns(['*', DB.knex.raw(
                "IF(GET_SYSPARAM_VALUE('STEALTH_MODE'), CORRECT_AVGIND(INDICATION_ID), INDICATION_VALUE) AS INDICATION_VALUE")]);
            qb.where('DEVICE_ID', deviceId).orderBy('INDICATION_REGDATE', 'DESC').limit(1);
        }).fetch({withRelated: ['device']})
            .then(function (indication) {
                if (indication){
                    return indication.related('device').related('type').fetch()
                        .then(function (type) {
                            return indication.related('device').related('alarm').fetch()
                                .then(function (alarm) {
                                    return {indication: indication.toJSON(), type: type.toJSON(), alarm: alarm ? alarm.toJSON(): {}};
                                })
                        })
                } else {
                    return null
                }
            })
    }),
    getByDeviceFromTo: Promise.method(function (deviceId, date_from, date_to) {
        return this.query(function (qb) {
            qb.where('DEVICE_ID', deviceId).andWhere('INDICATION_REGDATE', '>', date_from).andWhere('INDICATION_REGDATE', '<', date_to).orderBy('INDICATION_REGDATE', 'ASC');
        }).fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    })
});

module.exports = Model;
