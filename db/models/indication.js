var DB = require('./../db');
var Promise = require('bluebird');
var Device = require("./device");
var DeviceTypeList = require( "../device_type_list");
var removeUtcOffset = require('../../utils').removeUtcOffset;

var Model = DB.Model.extend({
    tableName: 'indication',
    device: function() {
        return this.belongsTo(Device, 'DEVICE_ID');
    }
}, {
    newIndicator: Promise.method(function (value, deviceId) {
        var _this = this;
        return Device.getById(deviceId)
            .then(function (device) {
                var type = device.DEVICETYPE_ID;
                var indicationValue = value;
                if (type == DeviceTypeList.types.temperature || type == DeviceTypeList.types.humidity){
                    indicationValue = indicationValue/ 100;
                } else if (type == DeviceTypeList.types.pressure){
                    indicationValue = indicationValue/ 10;
                }
                return new _this({DEVICE_ID: deviceId, INDICATION_VALUE: indicationValue, INDICATION_REGDATE: removeUtcOffset(new Date())})
                    .save()
                    .then(function (model) {
                        return model.toJSON()
                    });
            })
    }),
    getLastByDevice: Promise.method(function (deviceId) {
        return this.query(function (qb) {
            // qb.columns(['*', DB.knex.raw('GET_ZONE_LEVEL(DEVICE_ID, INDICATION_VALUE) AS INDICATION_LEVEL')]);
            qb.where('DEVICE_ID', deviceId).orderBy('INDICATION_REGDATE', 'DESC').limit(1);
        }).fetch({withRelated: ['device']})
            .then(function (indication) {
                if (indication){
                    return indication.related('device').related('type').fetch()
                        .then(function (type) {
                            return indication.related('device').related('alarm').fetch()
                                .then(function (alarm) {
                                    return {indication: indication.toJSON(), type: type.toJSON(), alarm: alarm ? alarm.toJSON(): {}};
                                })
                        })
                } else {
                    return null
                }
            })
    }),
    getByDeviceFromTo: Promise.method(function (deviceId, date_from, date_to) {
        return this.query(function (qb) {
            qb.where('DEVICE_ID', deviceId).andWhere('INDICATION_REGDATE', '>', date_from).andWhere('INDICATION_REGDATE', '<', date_to).orderBy('INDICATION_REGDATE', 'ASC');
        }).fetchAll()
            .then(function (c) {
                return c.toJSON()
            })
    }),
    // getByDeviceFromToPerChecks: Promise.method(function (deviceId, date_from, date_to) {
    //     return  this.query(function(qb) {
    //             return DB.knex.raw('CALL DEVICE_INDICHECK_FROM_TO_LST(?, ?, ?)', [deviceId, date_from, date_to])
    //         }).fetchAll().then(function (c) {
    //             return c.toJSON()
    //         });
    // })

});

module.exports = Model;
