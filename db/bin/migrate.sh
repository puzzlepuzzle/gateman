#!/usr/bin/env bash
# WITH DROP DATABASE

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SQL_DIR="${DIR}/../sql"

function sql {
    echo $1
    sudo mysql < ${SQL_DIR}/$1
    # mysql --user=$dbUser --password=$dbPass < $1
}

sql migrations/procs.sql

sql migrations/20171026_patches.sql
sql migrations/20171026_avgind.sql
sql migrations/20171026_roomcheckdet.sql
sql migrations/20171027_roomcheck_indexes.sql
sql migrations/20171103_roomcheck_corrected.sql
sql migrations/20171110_devicetype_reporting.sql
sql migrations/20171118_engeneer.sql
sql migrations/20171119_devicespec.sql
sql migrations/20171119_device_normalize.sql
sql migrations/20171120_new_devicetype.sql
sql migrations/20171126_roomcheck_desc.sql
sql migrations/20171126_roomcheck_desc.sql
sql migrations/20180121_sysparam.sql
