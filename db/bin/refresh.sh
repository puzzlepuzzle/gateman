#!/usr/bin/env bash
# CLEAR TABLES AND FILL

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SQL_DIR="${DIR}/../sql"

function sql {
    echo $1
    sudo mysql gateman < ${SQL_DIR}/$1
}

sql procs.sql
