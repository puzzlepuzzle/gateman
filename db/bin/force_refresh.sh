#!/usr/bin/env bash
# WITH DROP DATABASE

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SQL_DIR="${DIR}/../sql"

function sql {
    echo $1
    sudo mysql gateman < ${SQL_DIR}/$1
    # mysql --user=$dbUser --password=$dbPass < $1
}

function _sql {
    echo $1
    sudo mysql < ${SQL_DIR}/$1
    # mysql --user=$dbUser --password=$dbPass < $1
}

_sql init.sql
sql procs.sql

sql func.sql
    
sql role.sql
sql users.sql
sql floor.sql
sql room.sql
sql devicetype.sql
sql device.sql
sql indication.sql
sql alarm.sql

${DIR}/migrate.sh
