var DeviceTypeList = function () {
}

DeviceTypeList.types = {
    temperature: 1
    , humidity: 2
    , pressure: 3
    , door: 4
    , alarm: 5
    , complex: 6
}
module.exports = DeviceTypeList;
