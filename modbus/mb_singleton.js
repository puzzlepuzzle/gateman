var Promise = require('bluebird');
var config = require('../config');
var modbus = require('modbus-rtu');
var MBPort = require('./mb_port');
var DeviceTypeList = require( "../db/device_type_list");

var Singleton = (function () {
    var instance;
    var ports = {};

// Приватные методы и свойства
    function read (port,  speed, params) {
        return new Promise(function (resolve, reject) {
            if (!port) {
                reject(new Error('no port for queue'));
                return;
            }
            if (!ports[port]){
                ports[port] = new MBPort(port, speed)
            }
            ports[port].addReadOperation(params, resolve, reject);
        })
    };

    function startRead (port, speed, params) {
        return new Promise(function (resolve, reject) {
            if (!port) {
                reject(new Error('no port for queue'));
                return;
            }
            if (!ports[port]){
                ports[port] = new MBPort(port, speed)
            }
            ports[port].addStartReadOperation(params, resolve, reject);
        })
    };

// Конструктор
    function Singleton() {
        if (!instance)
            instance = this;
        else return instance;
    }

// Публичные методы

    Singleton.prototype.read = function (port, speed, device, address, count) {
        return read(port, speed, [port, speed, device, address, count])
    }

    Singleton.prototype.startRead = function (port, speed) {
        return startRead(port, speed, [port, speed])
    }

    Singleton.prototype.readFake = function (typeId) {
        return Promise.delay(1000)
            .then(function(){
                var indicationValue = (21 + Math.random())*100;
                if (typeId == DeviceTypeList.types.pressure){
                    indicationValue = (100 + Math.random()) * 10;
                }
                return indicationValue;
            })
    }

    return Singleton;
})()

module.exports = Singleton;