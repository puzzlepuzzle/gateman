var log = require('../log')(module);
var Promise = require('bluebird')
var config = require('../config');
var domain = require('domain');
var modbus = require('modbus-rtu');
var SerialPort = require('serialport').SerialPort;

function MBPort(port, speed) {
    if (!port || !speed) {
        throw new Error("no port or speed for MBQueue")
    }
    this.port = port;
    this.speed = speed;
    this.queue = [];
    this.currentDoingOperations = 0;
    this.maxDoingOperations = 1;
    this.timeoutOfDoingOperations = config.get('mb_timeout');
    this.delayOfDoingOperations = config.get('mb_delay');

    this.master = null;
}

MBPort.prototype.init = function () {
    var self = this;
    return new Promise(function (resolve, reject) {
        var d = domain.create();
        d.on('error', function (er) {
            log.error('error', er, er.stack);
            self.master = null;
            //currentDoingOperations[port] = 0;
            reject(er);
        })
        d.run(function () {
            var serialPort = new SerialPort(self.port, {
                baudrate: self.speed
            });

            //create ModbusMaster instance and feed them serial port
            return new modbus.Master(serialPort)
                .then(function (new_master) {
                    self.master = new_master;
                    //currentDoingOperations[port] = 0;
                    resolve(self.master);
                    log.debug('port', self.port, 'STARTED');
                })
                .catch(function (err) {
                    self.master = null;
                    reject(err);
                })
        })
    })
}
MBPort.prototype.addReadOperation = function (params, resolve, reject) {
    return this.addOperation(this.read, params, resolve, reject)
}

MBPort.prototype.addStartReadOperation = function (params, resolve, reject) {
    return this.addOperation(this.startRead, params, resolve, reject)
}

MBPort.prototype.addOperation = function (methodPromise, params, resolve, reject) {
    if (!methodPromise) {
        reject(new Error('no method for queue'));
        return;
    }
    var op = {method: methodPromise, params: params, resolve: resolve, reject: reject}
    this.queue.push(op);
    this.dequeue()
}

MBPort.prototype.read = function (DEVICE_COM, DEVICE_COM_SPEED, DEVICE_ADDRESS, DEVICE_BYTE_START, DEVICE_BYTE_COUNT) {
    var self = this;
    var promise;
    if (!this.master) {
        promise = this.init()
    } else {
        promise = Promise.resolve(this.master)
    }
    return promise
        .then(function (master) {
            return master.readInputRegisters(DEVICE_ADDRESS, DEVICE_BYTE_START, DEVICE_BYTE_COUNT)
                .then((function(valueArr){
                    if (!valueArr || valueArr.length != 1){
                        throw new Error('We read Not correct value' + valueArr)
                    }
                    return valueArr[0];
                }))
                .timeout(self.timeoutOfDoingOperations, 'too long read in queue ' + DEVICE_COM + '  ' + DEVICE_COM_SPEED
                    + '  ' + DEVICE_ADDRESS + '  ' + DEVICE_BYTE_START + '  ' + DEVICE_BYTE_COUNT)
        })
}

MBPort.prototype.startRead = function (DEVICE_COM, DEVICE_COM_SPEED) {
    var self = this;
    var promise;
    if (!this.master) {
        promise = this.init()
    } else {
        promise = Promise.resolve(this.master)
    }
    return promise
        .then(function (master) {
            return master.startRead(0x00, 0x20, 0x01)
                .timeout(self.timeoutOfDoingOperations, 'too long start Read in queue' + DEVICE_COM + '  ' + DEVICE_COM_SPEED)
        })
}


MBPort.prototype.dequeue = function () {
    var self = this;
    if (!this.queue.length) {
        log.debug('queue is empty', this.port);
        return;
    }

    if (this.currentDoingOperations >= this.maxDoingOperations) {
        log.debug('queue is bisy', this.port);
        return;
    }

    this.currentDoingOperations++;
    var operation = this.queue.shift();
    log.debug('op in queue is DOING', operation.params);
    operation.method.apply(this, operation.params)
        .then(function (result) {
            operation.resolve(result);
        })
        .catch(function (err) {
            operation.reject(err);
        })
        .delay(self.delayOfDoingOperations)
        .then(function () {
            self.currentDoingOperations--;
            self.dequeue();
        })
}

module.exports = MBPort