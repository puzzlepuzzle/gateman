var Singleton = require('./../../modbus/mb_singleton');
var Device = require("./../../db/models/device");
var Promise = require("bluebird");
var s = new Singleton();

var testDevice = [
    {DEVICE_COM: "/dev/ttyr00", DEVICE_COM_SPEED: 19200, DEVICE_ADDRESS: 16 , DEVICE_BYTE_START: 1, DEVICE_BYTE_COUNT: 1},
    {DEVICE_COM: "/dev/ttyr00", DEVICE_COM_SPEED: 19200, DEVICE_ADDRESS: 14 , DEVICE_BYTE_START: 2, DEVICE_BYTE_COUNT: 1},
]

return Device.getByTypes([1, 2, 3])
// return Promise.resolve(testDevice)
    .then(function (result) {
        console.log("START DONE")
        var ar = result.slice()
        var par = [];
        for (var key in ar) {
            var f = function (device) {
                //console.log('device', device);
                // console.log('start device',  device.DEVICE_ADDRESS, device.DEVICE_BYTE_START, device.DEVICE_BYTE_COUNT);
                return s.read(device.DEVICE_COM, device.DEVICE_COM_SPEED, device.DEVICE_ADDRESS, device.DEVICE_BYTE_START, device.DEVICE_BYTE_COUNT)
                    .then(function (data) {
                        console.log('device', device.DEVICE_COM, device.DEVICE_COM_SPEED, device.DEVICE_ADDRESS, device.DEVICE_BYTE_START, device.DEVICE_BYTE_COUNT, "result: ", data)
                    })
                    .timeout(30000)
                    .catch(function (err) {
                        console.error('device ERRRRRR',device.DEVICE_COM, device.DEVICE_COM_SPEED,  device.DEVICE_ADDRESS, device.DEVICE_BYTE_START, device.DEVICE_BYTE_COUNT, "err: ", err)
                    })
            }
            par.push(f(ar[key]))
        }
        return Promise.all(par);
    })
    .catch((err)=> {
        console.error('ERRRRRRRRRRRRR', err)
    })
    .then(()=>{
        console.log('END OF TEST')
    })
