//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_kue
var Promise = require("bluebird");
var log = require('../../log/index')(module);
var assert = require('chai').assert;

describe('TEST ALARMS', function () {

    var AS = require("./../../alarm/alarm_singleton");
    var AlarmModel = require("../../db/models/alarm");
    var Indication = require("../../db/models/indication");

    before(function (done) {
        return new AS().initAllAlarms()
            .then(function () {
                return new AS().offAllAlarms();
            })
            .then(function(){
                done()
            })
    });
    
    it('initAlarms', function () {
        return new AS().initAllAlarms()
            .then(function () {
                return AlarmModel.getAll()
                    .then(function (alarms) {
                        var actionDeviceDict = {};
                        for (var key in alarms) {
                            if (alarms[key].ACTION_DEVICE_ID) {
                                actionDeviceDict[alarms[key].ACTION_DEVICE_ID] = true;
                                assert.isOk(new AS().getCurrentAlarms()[alarms[key].ACTION_DEVICE_ID], 'was not init alarm ' + alarms[key])
                            }
                        }
                        assert.equal(Object.keys(actionDeviceDict).length, Object.keys(new AS().getCurrentAlarms()).length, 'wrong length of inited alarms')
                    })
            })
    })

    it('call Alarm', function () {
        var testAlarmActionId = 1204;
        var testAlarms;
        return AlarmModel.getByActionDeviceId(testAlarmActionId)
            .then(function (alarms) {
                testAlarms = alarms;
                return new AS().callAlarm(testAlarms[0].REASON_DEVICE_ID, testAlarms[0].ALARM_YZONE_TO + 10)
            })
            .then(function () {
                return Indication.getLastByDevice(testAlarmActionId)
            })
            .then(function (indicationInfo) {
                assert.isOk(indicationInfo, "alarm should have indication")
                assert.equal(indicationInfo.indication.INDICATION_VALUE, 1, 'alarm should be on')
                return new AS().callAlarm(testAlarms[1].REASON_DEVICE_ID, testAlarms[1].ALARM_YZONE_FROM - 10)
            })
            .then(function () {
                return Indication.getLastByDevice(testAlarmActionId)
            })
            .then(function (indicationInfo) {
                assert.equal(indicationInfo.indication.INDICATION_VALUE, 1, 'alarm should be on')
                return new AS().callAlarm(testAlarms[0].REASON_DEVICE_ID, testAlarms[0].ALARM_YZONE_TO)
            })
            .then(function () {
                return Indication.getLastByDevice(testAlarmActionId)
            })
            .then(function (indicationInfo) {
                assert.equal(indicationInfo.indication.INDICATION_VALUE, 1, 'alarm should be on')
                return new AS().callAlarm(testAlarms[1].REASON_DEVICE_ID, testAlarms[1].ALARM_YZONE_FROM)
            })
            .then(function () {
                return Indication.getLastByDevice(testAlarmActionId)
            })
            .then(function (indicationInfo) {
                assert.equal(indicationInfo.indication.INDICATION_VALUE, 0, 'alarm should be off');
                return new AS().callAlarm(testAlarms[1].REASON_DEVICE_ID, testAlarms[1].ALARM_YZONE_FROM - 10)
            })
            .then(function () {
                return new AS().offAllAlarms()
            })
            .then(function () {
                return Indication.getLastByDevice(testAlarmActionId)
            })
            .then(function (indicationInfo) {
                assert.equal(indicationInfo.indication.INDICATION_VALUE, 0, 'alarm should be off');
            })
    })


})