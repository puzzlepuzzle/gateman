//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_simple
var Promise = require("bluebird");
var assert = require('chai').assert;
var log = require('../../log/index')(module);

describe('TEST CONFIG', function () {

    it('read config', function(){
        var config = require('../../config/index');

        assert.isOk(config.get("port"), 'no port');
        assert.isOk(config.get("mysql:options"), 'no mysql options');

        log.info("port", config.get("port"));
        log.info("mysql:options", config.get("mysql:options"));
        log.info("debug", config.get("debug"));
        log.info("start_queue", config.get("start_queue"));
        log.info("start_read", config.get("start_read"));
        log.info("start_alarm", config.get("start_alarm"));
        log.info("log_level", config.get("log_level"));
        log.info("mb_timeout", config.get("mb_timeout"));
        log.info("mb_delay", config.get("mb_delay"));
        log.info("device_task_frequency", config.get("device_task_frequency"));
        log.info("alarm_max", config.get("alarm_max"));
        log.info("chart_points_count", config.get("chart_points_count"));
        log.info("kue_port", config.get("kue_port"));
    })
    
});


