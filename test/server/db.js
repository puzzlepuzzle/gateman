//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_DB
var Promise = require("bluebird");
var log = require('../../log/index')(module);
var DeviceTypeList = require("../../db/device_type_list.js");
var assert = require('chai').assert;
var moment = require('moment');

describe('TEST DB', function () {

    it('count of all device types', function () {
        var Device = require("./../../db/models/device");
        var f = function (type, count){
            return Device.getByTypes([type])
                .then(function (result) {
                    assert.equal(result.length, count, 'not correct temperature devices')
                })
        }
        return Promise.all([
            f(DeviceTypeList.types.temperature, 23),
            f(DeviceTypeList.types.humidity, 23),
            f(DeviceTypeList.types.pressure, 11),
            f(DeviceTypeList.types.door,7),
            f(DeviceTypeList.types.alarm,10),
        ])
    });

    it('count of alarms', function () {
        var Alarm = require("./../../db/models/alarm");

        return Alarm.getAll()
            .then(function (result) {
                assert.equal(result.length, 63, 'not correct alarms count')
            })
    });

    it('count of devicetypes', function () {
        var DT = require("./../../db/models/devicetype");

        return DT.getAll()
            .then(function (result) {
                assert.equal(result.length, 5, 'not correct devicetypes count')
            })
    });

    it('count of rooms', function () {
        var Room = require("./../../db/models/room");

        return Room.getAll()
            .then(function (result) {
                assert.equal(result.length, 25, 'not correct rooms count')
            })
    });

    it('we should find person of user with id = 1', function () {
        var User = require("./../../db/models/user");

        return User.getPerson(1)
            .then(function (result) {
                assert.isOk(result, 'user 1 not found');
                assert.isOk(result.person, 'person of user 1 not found');
                assert.equal(result.person.PERSON_NAME, 'Администратор', 'not correct admin-person name');
                assert.equal(result.person.SIGNATURE_FILE_NAME, 'admin_sign.png', 'not correct admin-person signatures file name');
            })
    });

    it('check calling procedure and returning result', function () {
        var Procs = require("./../../db/models/procs");

        return Procs.createRoomheck(1,'2016-06-18 17:45:57','2016-06-18 17:51:57', 1, 1)
    });

    it('last indication for device 1,2,3,4 types', function () {
        var Indication = require("./../../db/models/indication");
        var Device = require("./../../db/models/device");

        return Device.getByTypesWithAlram([
                DeviceTypeList.types.temperature,
                DeviceTypeList.types.humidity,
                DeviceTypeList.types.pressure,
                // DeviceTypeList.types.door
            ])
            .then(function (devices) {
                var promises = [];
                for (var key in devices){
                    promises.push((function(device){
                        return Indication.getLastByDevice(device.DEVICE_ID)
                            .then(function (indicationInfo) {
                                assert.isOk(indicationInfo, 'empty last indication info for ' + device.DEVICE_ID)
                                assert.isOk(indicationInfo.indication.INDICATION_VALUE, 'empty last indication value for device_id ' + device.DEVICE_ID)
                                var diffMin = moment().diff(moment(indicationInfo.indication.INDICATION_REGDATE), 'minutes');
                                assert.isOk(diffMin < 5, 'too old ' + indicationInfo.indication.INDICATION_REGDATE + ' last value for ' + device.DEVICE_ID);
                            })
                    })(devices[key]))
                }

                return Promise.all(promises)
            })

    });

    it.only('check calling procedure calcAverageIndication and returning result ', function () {
        var Procs = require("./../../db/models/procs");
        var DB = require('./../../db/db');
        var moment = require('moment');

        return Procs.calcAverageIndication(1, new Date(), 190000)
            .then(function (result) {
                console.log(result)
            })

        // return Procs.getIndicationsByDeviceFromTo(24,'2016-12-08 00:00:57','2016-12-09 17:51:57')
        //     .then(function (result) {
        //         console.log(result)
        //     })

        // var date = new Date();
        // var d = moment(date).add(moment(date).utcOffset(), 'm').utc().toDate()
        // return DB.knex.raw('select ?;', [d])
        //     .then(function (result) {
        //         console.log(result[0][0])
        //     })
    });
});