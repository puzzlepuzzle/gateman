//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_kue
var Promise = require("bluebird");
var log = require('../../log/index')(module);
var assert = require('chai').assert;

describe('TEST KUE', function () {

    var Queue = require("./../../kue/queue");
    var q = new Queue();

    before(function (done) {
        q.clean()
        .then(function(){
            done()
        })
    });


    it('long task', function () {
        var cb = function (data) {
            return Promise.delay(500).then(function () {
                return data;
            })
        }
        q.registerTask("test1", cb);
        var job_id = null;
        return q.addTask("test1", "!!test data!!")
            .then(function (_job_id) {
                assert.isOk(_job_id, 'job_id is null');
                job_id = _job_id
                return q.getState(job_id)
            })
            .then(function (job) {
                assert.isOk(job, "job is null");
                assert.equal(job.state(), Queue.jobState.inactive, 'job state not correct: ' + job.state());
                assert.equal(job.data, "!!test data!!", 'job data not correct: ' + job.data);
                return job_id;
            })
            .delay(300)
            .then(function (job_id) {
                return q.getState(job_id)
            })
            .then(function (job) {
                assert.equal(job.state(), Queue.jobState.active, 'job state not correct: ' + job.state());
                return job_id;
            })
            .delay(300)
            .then(function (job_id) {
                return q.getState(job_id)
                    .catch(function(err){
                        assert.isOk(err.message.indexOf("No job was found with id") > -1, "not correct error")
                        return null;
                    })
            })
            .then(function (res) {
                assert.isNotOk(res, "task should be removed")
            })
    })


    it('delay for task', function () {

        var cb = function (data) {
            return Promise.delay(500).then(function () {
                return data;
            })
        }

        q.registerTask("test2", cb);
        var job_id = null;
        return q.addTask("test2", "!!test data!!", Queue.priority.high, 1000)
            .then(function (_job_id) {
                job_id = _job_id
                return q.getState(job_id)
            })
            .then(function (job) {
                assert.equal(job.state(), Queue.jobState.delayed, 'job state not correct: ' + job.state());
                return job_id;
            })
            .delay(1100)
            .then(function (job_id) {
                return q.getState(job_id)
            })
            .then(function (job) {
                assert.equal(job.state(), Queue.jobState.active, 'job state not correct: ' + job.state());
                return job_id;
            })
            .delay(500)
            .then(function (job_id) {
                return q.getState(job_id)
                .catch(function(err){
                    assert.isOk(err.message.indexOf("No job was found with id") > -1, "not correct error")
                    return null;
                })
            })
            .then(function (res) {
                assert.isNotOk(res, "task should be removed")
            })
    })
})