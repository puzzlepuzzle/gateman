//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000
var superagent = require('superagent');
var app = require('../../server')
var request = require('supertest')(app);
var agent = superagent.agent();
var log = require('../../log/index')(module);
var Promise = require("bluebird");
var AlarmModel = require("../../db/models/alarm.js");
var assert = require('chai').assert;
var config = require('../../config/index');

var testAccount = {
    "login": "admin",
    "password": "1"
};

var test_api = function (mode, api, params) {
    return new Promise((function (resolve, reject) {
        var req = mode == "post" ? request.post(api).send(params) : request.get(api).query(params);
        agent.attachCookies(req);
        req.set({'x-requested-with': 'XMLHttpRequest'})
            .end(function (err, res) {
                if (err) {
                    return reject(err)
                } else {
                    return resolve(res);
                }
            })

    }))
        .then(function (res) {
            assert.isOk(res, 'empty res');
            assert.equal(res.status, 200, 'res status not 200: ' + res.status + ', error: ' + (res.body ? res.body.message : ''));
            return res;
        });
}

var login = function () {
    return test_api('post', '/auth/login', testAccount)
        .then(function (res) {
            agent.saveCookies(res);
            return agent;
        })
};


describe('API TEST', function () {

    var loginAgent;
    before(function () {
        return login()
            .then(function (authAgent) {
                loginAgent = authAgent;
            })
    });


    it('api/device/lastindication', function () {
        return test_api('get', '/api/device/lastindication/5', null)
            .then(function (res) {
                assert.isOk(res.body, 'empty body');
                assert.isOk(res.body.value, 'empty value field');
                assert.isOk(res.body.type, 'empty type field');
                assert.isOk(res.body.alarm, 'empty alarm field');
            })
    });

    it('api/rooms', function () {
        return test_api('get', '/api/rooms', null)
            .then(function (res) {
                assert.isOk(res.body, 'empty body');
                assert.equal(res.body.length, 25, 'wrong rooms count');
            });
    });

    it("api/room/indications", function () {
        return test_api('get', '/api/room/indications/1131/2014-01-01/2017-01-01', null)
            .then(function (res) {
                assert.isOk(res.body, 'empty body');
                assert.equal(res.body.length, 2, 'wrong devices count');

                for (var key in res.body) {
                    var o = res.body[key];
                    assert.isOk(o.device, 'empty device field');
                    assert.isOk(o.data, 'empty data field');
                    assert.isOk(o.data.length, 'null data length');
                    assert.isOk(o.regdates, 'empty regdates field');
                    assert.equal(o.data.length, o.regdates.length, 'wrong length of regdates or data');
                    assert.isOk(o.maxCount, 'empty maxCount field');
                }
            });
    });

    it('door/open', function () {
        var Indication = require("./../../db/models/indication");
        var testValue = 40;
        var testId = 205;
        return test_api('get', '/door/open/' + testId + '/' + testValue, null)
            .then(function (res) {
                return Indication.getLastByDevice(testId)
            })
            .then(function (indicationInfo) {
                assert.isOk(indicationInfo, 'empty last indication info for ' + testId)
                assert.equal(indicationInfo.indication.INDICATION_VALUE, testValue, 'wrong last indication value for device_id ' + testId)
            });
    });

    it('door/close', function () {
        var Indication = require("./../../db/models/indication");
        var testValue = 0;
        var testId = 205;
        return test_api('get', '/door/close/' + testId + '/' + testValue, null)
            .then(function (res) {
                return Indication.getLastByDevice(testId)
            })
            .then(function (indicationInfo) {
                assert.isOk(indicationInfo, 'empty last indication info for ' + testId)
                assert.equal(indicationInfo.indication.INDICATION_VALUE, testValue, 'wrong last indication value for device_id ' + testId)
            });
    });

    it('door/open bad value', function () {
        var Indication = require("./../../db/models/indication");
        var testValue = 540;
        var testId = 204;
        var alarm;
        return test_api('get', '/door/open/' + testId + '/' + testValue, null)
            .then(function (res) {
                return Indication.getLastByDevice(testId)
            })
            .then(function (indicationInfo) {
                assert.isOk(indicationInfo, 'empty last indication info for ' + testId)
                assert.equal(indicationInfo.indication.INDICATION_VALUE, testValue, 'wrong last indication value for device_id ' + testId)
                if (+config.get("start_alarm")) {
                    return AlarmModel.getByReasonDeviceId(testId)
                        .then(function (_alarm) {
                            alarm = _alarm;
                            assert.isOk(alarm, 'no alarm for ' + testId);
                            assert.isOk(alarm.ACTION_DEVICE_ID, 'no alarm.ACTION_DEVICE_ID for ' + testId);
                            return Indication.getLastByDevice(alarm.ACTION_DEVICE_ID)
                        })
                        .then(function (indicationInfo) {
                            assert.isOk(indicationInfo, 'empty last indication info for alarm ' + alarm.ACTION_DEVICE_ID)
                            assert.equal(indicationInfo.indication.INDICATION_VALUE, 1, 'wrong last indication value for alarm, device_id ' + alarm.ACTION_DEVICE_ID);
                        })
                }
            })
    });

    after(function (done) {
        var AS = require("./../../alarm/alarm_singleton");
        return new AS().initAllAlarms()
            .then(function () {
                return new AS().offAllAlarms();
            })
            .then(function(){
                done()
            })
    });
});


