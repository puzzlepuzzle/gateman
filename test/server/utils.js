//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_simple
var Promise = require("bluebird");
var log = require('../../log/index')(module);
var assert = require('chai').assert;
var bcrypt = require('bcrypt-nodejs');

describe('TEST UTILS', function () {
    
    it('decimate array', function(){
        var decimate = require('../../utils/index').arrayDecimate;
        var arr = [];
        var maxCount = 10;
        var arrCount = 15;
        for(var i = 0; i<arrCount; i++){
            arr.push(i);
        }
        log.info("arr length:", arr.length, "maxCount:", maxCount);
        log.info("arr:", JSON.stringify( arr));
        var newarr = decimate(arr, maxCount, false)
        log.info("new arr", JSON.stringify(newarr), "new arr length:", newarr.length);

        assert.equal(newarr.length, maxCount, "not correct length of new array")
    })

    it("create passwords for users", function(){
        for(var i = 0; i<2; i++){
            var randomstring = Math.random().toString(36).slice(-8);
            var encPwd = bcrypt.hashSync(randomstring, bcrypt.genSaltSync(8), null);
            console.log(randomstring, encPwd);
        }

    })

});


