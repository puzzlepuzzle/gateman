//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_MB
var Promise = require("bluebird");
var log = require('../../log/index')(module);
var assert = require('chai').assert;
var config = require('../../config');


describe('TEST PERSON', function () {
    it.skip('speed of  mysql2 for mysql procedure', function () {
        var mysql = require('mysql2/promise'); // or require('mysql2').createConnectionPromise
        return mysql.createConnection({
            host     : config.get('mysql:options:host'),
            user     : config.get('mysql:options:user'),
            password : config.get('mysql:options:password'),
            database : config.get('mysql:options:database')
        })
            .then((conn) => conn.execute('CALL DEVICE_INDICHECK_FROM_TO_LST(?, ?, ?)',[5, "2016-07-01", "2016-07-30"]))
            // .then((conn) => conn.execute('select * from device LIMIT 2;'))
            .then((res) => console.log("res:", res[0][0]))
        
    })

    it.skip('speed of knex for mysql procedure', function () {

        var procs  = require("./../../db/models/procs");
        return procs.getByDeviceFromToPerChecks(5, "2016-07-01", "2016-07-30")
            .then((res) => console.log("res:", res))


    })




    it('Procs.getByDeviceFromToPerChecks', function () {
        var Procs = require("./../../db/models/procs");
        var  roomid=105;
        var from="2016-07-08 13:00";
        var to="2016-07-09 13:00";
        return Procs.getByDeviceFromToPerChecks(roomid,from,to)
            .then(function (result) {
                console.log("res:",  result)
            })
    });

    it('Procs.createRoomheck', function () {
        var Procs = require("./../../db/models/procs");
        var  roomid=105;
        var from="2016-07-08 13:00";
        var to="2016-07-09 13:00";
        var verified=true;
        return Procs.createRoomheck(roomid, from, to, 1, verified)
            .then(function (result) {
                console.log("res:",  result)
            })
    });


})