//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000 test_MB
var Promise = require("bluebird");
var log = require('../../log/index')(module);
var assert = require('chai').assert;
var config = require('../../config');
var DeviceTypeList = require("../../db/device_type_list.js");
var MBSingleton = require('../../modbus/mb_singleton');

describe('TEST MB', function () {
    it('modbus read for all device 1,2,3 types and check limits', function () {
        if (+config.get("debug")) {
            return;
        }
        var Device = require("./../../db/models/device");
        return Device.getByTypesWithAlram([
                DeviceTypeList.types.temperature,
                DeviceTypeList.types.humidity,
                DeviceTypeList.types.pressure
            ])
            .then(function (result) {
                var par = []
                for (var key in result) {
                    var f = function (device) {

                        return new MBSingleton().read(device.DEVICE_COM, device.DEVICE_COM_SPEED, device.devicespec.DEVICE_ADDRESS, device.type.DEVICE_BYTE_START, device.type.DEVICE_BYTE_COUNT)
                            .then((value)=> {
                                var type = device.DEVICETYPE_ID;
                                var indicationValue = value;
                                if (type == DeviceTypeList.types.temperature || type == DeviceTypeList.types.humidity) {
                                    indicationValue = indicationValue / 100;
                                } else if (type == DeviceTypeList.types.pressure) {
                                    indicationValue = indicationValue / 10;
                                }
                                assert.isOk(indicationValue < device.alarm.ALARM_GZONE_TO,
                                    "value " + indicationValue + " > ALARM_GZONE_TO " + device.alarm.ALARM_GZONE_TO);
                                assert.isOk(indicationValue < device.alarm.ALARM_YZONE_TO,
                                    "value " + indicationValue + " > ALARM_YZONE_TO " + device.alarm.ALARM_YZONE_TO);
                                assert.isOk(indicationValue > device.alarm.ALARM_GZONE_FROM,
                                    "value " + indicationValue + " < ALARM_GZONE_FROM " + device.alarm.ALARM_GZONE_FROM);
                                assert.isOk(indicationValue > device.alarm.ALARM_YZONE_FROM,
                                    "value " + indicationValue + " < ALARM_YZONE_FROM " + device.alarm.ALARM_YZONE_FROM);

                                //log.info('value: ', value[0], 'device: ', JSON.stringify(device));
                            });
                    }
                    par.push(f(result[key]))
                }
                return Promise.all(par);
            })
    })

})
