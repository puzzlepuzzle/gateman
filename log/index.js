var winston = require('winston');
var ENV = process.env.NODE_ENV;
var config = require('../config')
// can be much more flexible than that O_o
function getLogger(module) {

    var path = module.filename.split('/').slice(-2).join('/');

    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                colorize: true,
                level: config.get('log_level'),
                label: path
            })
        ]
    });
}

module.exports = getLogger;