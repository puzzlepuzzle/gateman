import React from 'react';
var rest = require("./rest");
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import { ReactTableDefaults } from 'react-table'

var moment = require('moment');


Object.assign(ReactTableDefaults, {
    previousText: 'Предыдущаяя',
    nextText: 'Следующая',
    loadingText: 'Загрузка...',
    noDataText: 'Не найдено ни одного датчика',
    pageText: 'Страница',
    ofText: 'из',
    rowsText: 'датчиков',
})

class DevicesView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            specs: [],
            types: [],
            pages: null,
            loading: true,
            apiProps: {}
        }
        this.getData = this.getData.bind(this);
        this.renderEditable = this.renderEditable.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount () {
        this.getData();
    }

    getData() {
        this.setState(Object.assign({}, this.state, {loading: true}));

        var state = {loading: false};
        rest.get('/api/devicespecs')
            .then((specs) => {
                state.specs = specs.sort((a, b)=>{
                    return a.DEVICESPEC_SN - b.DEVICESPEC_SN;
                });
                return rest.get('/api/devicetypes')
            })
            .then((types) => {
                state.types = types;
                return rest.get('/api/devices')
            })
            .then((data) => {
                state.data = data.map(item => {
                    item.DEVICESPEC_SN = item.devicespec.DEVICESPEC_SN;
                    return item;
                });

                this.setState(Object.assign({}, this.state, state));
            })
            .catch((err) => {
                console.error("Ошибка при получении датчиков : %s", err ? JSON.stringify(err) : '-');
                var newState = Object.assign({}, this.state, {loading: false});
                this.setState(newState);
            })
    }

    save(value, cellInfo) {
        console.log("EDIT", value, cellInfo);

        const newSpec = this.state.specs.find(spec => spec.DEVICESPEC_SN == value);

        if (!newSpec) {
            console.log('нет такой спеки');
            return;
        }

        this.setState(Object.assign({}, this.state, {loading: true}));

        rest.post('/api/devices/' + cellInfo.row.DEVICE_ID, {devicespecid: newSpec.DEVICESPEC_ID})
            .then(() => {
                const data = [...this.state.data];

                data[cellInfo.index][cellInfo.column.id] = value;

                var newState = Object.assign({}, this.state, {
                    data,
                    loading: false
                });
                this.setState(newState);
            })
            .catch((err) => {
                console.log("Ошибка при сохранении:", err);
                var newState = Object.assign({}, this.state, {loading: false});
                this.setState(newState);
            })
    }

    renderEditable(cellInfo) {
        return (
            <select
                onChange={event => this.save(event.target.value, cellInfo)}
                style={{ width: "100%" }}
                value={this.state.data[cellInfo.index][cellInfo.column.id]}
            >
                {
                    this.state.specs.map((spec)=>{
                        return <option value={spec.DEVICESPEC_SN} key={spec.DEVICESPEC_SN}>{spec.DEVICESPEC_SN}</option>
                    })
                }
            </select>
        );
    }

    render() {
        const columns = [{
                Header: 'Комната',
                accessor: 'room.ROOM_NAME'
            },{
                Header: 'Тип датчика',
                accessor: 'type.DEVICETYPE_NAME',
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    }
                    return row[filter.id] === filter.value;
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                    >
                        <option value="all">Все</option>
                        {
                            this.state.types.map((type)=>{
                                return <option value={type.DEVICETYPE_NAME} key={type.DEVICETYPE_ID}>{type.DEVICETYPE_NAME}</option>
                            })
                        }
                    </select>
            },{
                Header: 'Заводской номер датчика',
                accessor: 'DEVICESPEC_SN',
                Cell: this.renderEditable,
                filterable: false
            }, {
                Header: 'ID датчика',
                    accessor: 'DEVICE_ID',
                show: false
            }];

            return  <ReactTable
                columns={columns}
                data={this.state.data}
                 sortable
                 filterable
                 defaultPageSize={20}
                defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]).toLowerCase().includes(filter.value.toLowerCase())}
                className="-striped -highlight"
                 loading={this.state.loading}
            />
    }
}

export default DevicesView;
