import React from 'react';
var auth = require('./auth.js');

import {
    requestLogout, receiveLogout, updateLogin, logoutError
} from './actions/login_actions'

import {connect} from 'react-redux'


function select(state) {
    return state
}
export default connect(select)(React.createClass({

        contextTypes: {
            router: React.PropTypes.object.isRequired
        },

        componentDidMount() {
            var _this = this;
            auth.logout()
                .then((result) => {
                    // console.log('logout', result, document.cookie);

                    _this.props.dispatch(receiveLogout());

                    this.setState({text: "Теперь Вы не залогинены"})
                    const {location} = _this.props

                    _this.context.router.replace('/')
                })
                .catch((err) => {
                    console.error('logout E', err);
                    _this.props.dispatch(logoutError(err))
                    return this.setState({text: err})
                });
        },

        render() {
            return <p>{this.state.text}</p>
        },

        getInitialState() {
            return {
                text: "Обращение к серверу..."
            }
        }
    })
);