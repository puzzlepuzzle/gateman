import React from 'react';
import {connect} from 'react-redux'

export function requireAuthentication(Component) {

    function select(state) {
        return {
            loggedIn: state.login.isAuthenticated
        }
    }
    
    return connect(select)(React.createClass({

        contextTypes: {
            router: React.PropTypes.object.isRequired
        },

        propTypes: {
                loggedIn: React.PropTypes.bool.isRequired
        },


        componentWillMount() {
            this.checkAuth();
        },

        componentWillReceiveProps(nextProps) {
            this.checkAuth();
        },

        checkAuth() {
            if (!this.props.loggedIn) {
                this.context.router.push('/login')
            }
        },


        render() {
            return (
                <div>
                    {this.props.loggedIn
                        ? <Component {...this.props}/>
                        : ''
                    }
                </div>
            )

        }
    }))

}
//https://github.com/reactjs/react-router/issues/1063
//https://preact.gitbooks.io/react-book/content/flux/auth.html
//https://github.com/joshgeller/react-redux-jwt-auth-example