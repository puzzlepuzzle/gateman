export const ERROR = 'ERROR'
export const CLEAN_ERROR = 'CLEAN_ERROR'

export function setError(error) {
    return {
        type: ERROR,
        creds: error
    }
}

export function cleanError() {
    return {
        type: CLEAN_ERROR
    }
}