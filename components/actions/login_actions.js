export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE'
export const LOGIN_UPDATE = 'LOGIN_UPDATE'

export function requestLogin(creds) {
    return {
        type: LOGIN_REQUEST,
        creds: creds
    }
}

export function receiveLogin(info) {
    return {
        type: LOGIN_SUCCESS,
        info: info
    }
}

export function loginError(message) {
    return {
        type: LOGIN_FAILURE,
        message
    }
}

export function requestLogout() {
    return {
        type: LOGOUT_REQUEST
    }
}

export function receiveLogout() {
    return {
        type: LOGOUT_SUCCESS
    }
}

export function logoutError(message) {
    return {
        type: LOGOUT_FAILURE,
        message
    }
}

export function updateLogin(isLogged) {
    return {
        type: LOGIN_UPDATE,
        isAuthenticated: isLogged
    }
}