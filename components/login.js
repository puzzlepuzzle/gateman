import React from 'react';
var auth = require('./auth.js');

import {
    requestLogin, receiveLogin, updateLogin, loginError
} from './actions/login_actions'

import {connect} from 'react-redux'


function select(state) {
    return state
}
export default connect(select)( React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            error: ""
        }
    },

    handleSubmit(event) {
        event.preventDefault()
        var _this = this;

        const email = this.refs.login.value
        const pass = this.refs.pass.value

        auth.login(email, pass)
            .then((result) => {
                // console.log('login', result, document.cookie);

                _this.props.dispatch(receiveLogin(result));

                const {location} = _this.props

                if (location.state && location.state.nextPathname) {
                    _this.context.router.replace(location.state.nextPathname)
                } else {
                    _this.context.router.replace('/')
                }
            })
            .catch((err) => {
                console.error('login E', err);
                _this.props.dispatch(loginError(err))
                return this.setState({error: err.responseJSON.message})
            });
    },

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="form-login">
                <h2>Вход</h2>
                <div className="form-group">
                    <label >Логин</label>
                    <input type="text" className="form-control" placeholder="Логин" required=""
                           autofocus="" ref="login"/>
                </div>
                <div className="form-group">
                    <label >Пароль</label>
                    <input type="password" className="form-control" placeholder="Пароль" required="" ref="pass"
                           />
                </div>

                <button className="btn btn-lg btn-primary " type="submit">Вход</button>
                {this.state.error &&  <p className="text-danger">{this.state.error}</p>}
            </form>
        )
    }
})
);