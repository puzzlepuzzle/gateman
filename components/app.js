import React from 'react';
import NavLink from './NavLink'
import DigitalClock from './DigitalClock'
var rest = require("./rest");
import {connect} from 'react-redux'

//require("bootstrap/dist/css/bootstrap.css");

class AppView extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="main-div" >
                <div className="container-main" >{this.props.children}</div>
            </div>
        )
    }

}
function select(state) {
    return state
}
export default connect(select)(AppView);