var React = require('react');
import Room from './room';
import IndicationDescription from './indication_description';

export default React.createClass({

    render() {
        return <div>
            {this.props.params.floorNo &&  <h3>План {this.props.params.floorNo} этажа</h3>}

            {(() => {
                var floor = this.props.params.floorNo;
                if (!floor) return "Не выбран этаж";
                if (floor == 1) {
                    return <Room roomId="room_1" backgroundPath='/images/План номерации комнат + 0000 1 с проходами.jpg'
                                 indicators={[
                  {x:370, y:412, up: true, ids:[6,5]},
                  {x:568, y:466, up:true, ids:[8,7]},
                  {x:779, y:412, up:true, ids:[10,9]},
                  {x:1042, y:412, up:true, ids:[12,11]},
                  {x:1177, y:412, up:true, ids:[14,13]},
                  {x:384, y:530, up:false, ids:[17, 18]},
                  {x:497, y:527, up:false, ids:[15, 16]},
                  ]}
                                 alarms={[]}
                                 doors={[]}
                    />;
                }
                else if (floor == 2) {
                    return <Room roomId="room_2" backgroundPath='/images/План номерации комнат на отм + 3300 1 с проходами.jpg'
                                 indicators={[
                   {x:370, y:360, up: true, ids:[21,20,19]},
                  {x:535, y:360, up:true, ids:[204229, 24, 23,22]},
                  {x:873, y:365, up:true, ids:[207229, 27, 26,25]},
                  {x:966, y:420, up:true, ids:[29, 28]},
                  {x:374, y:575, up:false, ids:[31, 32, 33]},
                  {x:470, y:575, up:false, ids:[34, 35]},
                  {x:568, y:575, up:false, ids:[36, 37, 38, 221229]},
                  {x:700, y:555, up:false, ids:[39, 40, 41, 220229]},
                  {x:803, y:558, up:false, ids:[42, 43, 44, 219229]},
                  {x:945, y:572, up:false, ids:[45, 46, 47, 218229]},
                  {x:975, y:442, up:false, ids:[30]},
                  ]}
                                 alarms={[
                                 {x:370, y:412, id:1203},
                                 {x:490, y:380, id:1204},
                                 {x:900, y:380, id:1207},
                                 {x:1000, y:450, id:1229},
                                 {x:910, y:530, id:1218},
                                 {x:800, y:520, id:1219},
                                 {x:570, y:510, id:1221},
                                 {x:470, y:530, id:1222},
                                 {x:400, y:530, id:1223},
                                 ]}
                                 doors={[
                                 {x:640, y:435, id:204},
                                 {x:663, y:425, id:205},
                                 {x:860, y:425, id:207},
                                 {x:947, y:470, id:218},
                                 {x:800, y:470, id:219},
                                 {x:644, y:510, id:221},
                                 ]}
                    />;
                }
                else if (floor == 3) {
                    return <Room roomId="room_3" backgroundPath='/images/План номерации комнат на отм. + 6600 1 с проходами.jpg'
                                 indicators={[
                  {x:111, y:478, up: true, ids:[51,50]},
                  {x:145, y:544, up:true, ids:[62, 61]},
                  {x:366, y:504, up:true, ids:[53]},
                  {x:529, y:374, up:true, ids:[56, 55,54]},
                  {x:70, y:520, up:false, ids:[48, 49]},
                  {x:231, y:619, up:false, ids:[59, 60]},
                  {x:392, y:535, up:false, ids:[57, 58]}
                  ]}
                                 alarms={[
                                 {x:500, y:420, id:1306}
                                 ]}
                                 doors={[
                                 {x:460, y:480, id:306}
                                 ]}
                    />;
                }
                else {
                    return "Такого этажа нет";
                }

            })()}

            {this.props.params.floorNo 
            && this.props.params.floorNo>0
            && this.props.params.floorNo<4
            && <IndicationDescription/>}
            
        </div>

    }
})