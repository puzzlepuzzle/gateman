var rest = require("./rest");
module.exports = {
    login(email, pass) {

        return rest.post('/auth/login', {login: email, password: pass})
        //return fetch('/auth/login', {
        //    method: 'POST',
        //    headers: {
        //        'Accept': 'application/json',
        //        'Content-Type': 'application/json',
        //    },
        //    body: JSON.stringify({
        //        login: email, password: pass
        //    })
        //})
        //    .then((response) => {
        //        return response.json();
        //    })

    },


    logout() {
        return rest.get('/auth/logout')
        //return fetch('/auth/logout')
        //    .then((response) => {
        //        return response.json();
        //    })
    },

    loggedIn() {
        return rest.get('/auth/check_auth')
        //return fetch('/auth/check_auth')
        //    .then((response) => {
        //        return response.json();
        //    })
    }
}
