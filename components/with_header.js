import React from 'react';
import NavLink from './NavLink'
var rest = require("./rest");
import {connect} from 'react-redux'

//require("bootstrap/dist/css/bootstrap.css");

class WithHeaderView extends React.Component {

    static get propTypes() {
        return {
            loggedIn: React.PropTypes.bool.isRequired
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            canOffAlarm: true,
            alarmLinkColor: 'red',
            suherWasGetted: false,
            suherDisabled: false,
            suher: false
        }
        this.clickAlarm = this.clickAlarm.bind(this);
        this.clickSuher = this.clickSuher.bind(this);
    }

    componentDidMount() {
        rest.get('/api/param/stealth')
            .then((res) => {
                var state = Object.assign({}, this.state, {
                    suherWasGetted: true,
                    suher: res.value === "1"
                });
                this.setState(state);
            })
            .catch((err) => {
                console.error("Ошибка при получении stealth : %s", err);
            })
    }

    clickAlarm () {
        if (!this.state.canOffAlarm){
            return;
        }
        var state = Object.assign({}, this.state, {
            canOffAlarm: false,
            alarmLinkColor: 'darkgrey'
        })
        var cb = () =>{
            var state = Object.assign({}, this.state, {
                canOffAlarm: true,
                alarmLinkColor: 'red'
            });
            this.setState(state);
        }

        this.setState(state, ()=> {
            return rest.get('/api/alarms/off')
                .then(( res) => {
                    console.log("off alarm end: %s", res);
                    cb();
                })
                .catch((err) => {
                    console.error("Ошибка при отключении сигнализаций : %s", err);
                    cb();
                })

        });
    }

    clickSuher () {
        if (this.state.suherDisabled){
            return;
        }
        var state = Object.assign({}, this.state, {
            suherDisabled: true
        })

        this.setState(state, ()=> {
            return rest.post('/api/param/stealth/switch')
                .then(() => {
                    this.setState(Object.assign({}, this.state, {
                        suher: !this.state.suher,
                        suherDisabled: false
                    }));
                })
                .catch((err) => {
                    console.error("Ошибка в /api/param/stealth/switchй : %s", err);
                    this.setState(Object.assign({}, this.state, {
                        suherDisabled: false
                    }));
                })

        });
    }

    render() {
        let suherOn, suherOff;

        if (this.state.suher) {
            suherOn = 'btn btn-primary active';
            suherOff = 'btn btn-default';
        } else {
            suherOn = 'btn btn-default';
            suherOff = 'btn btn-primary active';
        }

        return (
            <div className="main-div" >
                <header className="navbar navbar-static-top header-color">
                    <div className="container-main">
                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li><NavLink to="/" onlyActiveOnIndex>Главная</NavLink></li>
                                <li><NavLink to="/floor/1" >1 этаж</NavLink></li>
                                <li><NavLink to="/floor/2" >2 этаж</NavLink></li>
                                <li><NavLink to="/floor/3" >3 этаж</NavLink></li>
                                {!this.props.loggedIn || <li><NavLink to="/new_report">Новый отчет</NavLink></li> }
                                <li><NavLink to="/reports">Архив отчетов</NavLink></li>
                                {this.props.loggedIn && [1, 4].includes(this.props.role) && <li><NavLink to="/devices">Датчики</NavLink></li> }
                                <li><a href="#" style={{color: this.state.alarmLinkColor}} onClick={this.clickAlarm}>Отключить все сигнализации</a></li>
                            </ul>

                            {this.props.loggedIn && [1].includes(this.props.role) && this.state.suherWasGetted && <form className="navbar-form navbar-left" role="search">
                                <div>Режим корректировки</div>
                                <div className="btn-group btn-toggle">
                                    <button className={suherOn} onClick={this.clickSuher} disabled={this.state.suherDisabled}>ON</button>
                                    <button className={suherOff} onClick={this.clickSuher} disabled={this.state.suherDisabled}>OFF</button>
                                </div>
                            </form>}
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    {this.props.loggedIn ? (
                                        <NavLink to="/logout">Выйти</NavLink>
                                    ) : (
                                        <NavLink to="/login">Войти</NavLink>
                                    )}
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>
                <div className="container-main" >{this.props.children}</div>
            </div>
        )
    }

}
function select(state) {
    return {
        loggedIn: state.login.isAuthenticated,
        role: state.login.role
    }
}
export default connect(select)(WithHeaderView);