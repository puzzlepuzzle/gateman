import React from 'react'
var rest = require("./rest");
var DeviceTypeList = require('../db/device_type_list')
var moment = require('moment');

class Indicator extends React.Component {
    static get propTypes() {
        return {
            x: React.PropTypes.number.isRequired,
            y: React.PropTypes.number.isRequired,
            up: React.PropTypes.bool,
            ids: React.PropTypes.arrayOf(React.PropTypes.number).isRequired
        };
    }


    constructor(props) {
        super(props);
        this.state = {};
        this.errorMax = 5;
        this.canUpdate = true;
    }


    componentDidMount() {
        var _this = this;
        _this.updateIndicators();
    }

    componentWillUnmount() {
        // console.log("componentWillUnmount",this.props.ids)
        this.canUpdate = false;
    }

    updateIndicators() {
        var _this = this
        var ids = _this.props.ids.filter(function(item, pos) {
            return _this.props.ids.indexOf(item) == pos;
        });
        for (var key in ids){
            console.log('Начали обновлять датчик %s ', ids[key])
            _this.updateData(ids[key], 0)
        }
    }

    updateData(id, errorsCount) {
        var _this = this;
        if (!_this.canUpdate){
            return;
        }
        if (errorsCount > _this.errorMax){
            console.error("Слишком много ошибок при получении данных для датчика %s", id );
            var newO = {}
            newO[id] = {value : null, valid: false};
            var state = Object.assign({}, _this.state, newO);
            _this.setState(state)
            return;
        }
        return rest.get('/api/device/lastaverageindication/' + id)
            .then((data) => {
                var newO = {}
                var color = "black"
                switch(data.type.DEVICETYPE_ID) {
                    case DeviceTypeList.types.temperature:
                        color = "darkgreen";
                        break;
                    case DeviceTypeList.types.humidity:
                        color = "blue";
                        break;
                    case DeviceTypeList.types.pressure:
                        color = "purple";
                        break;
                }
                var valid = true;
                var validColor = "white";
                if (data.value.INDICATION_VALUE < data.alarm.ALARM_YZONE_FROM || data.value.INDICATION_VALUE > data.alarm.ALARM_YZONE_TO){
                    valid = false;
                    validColor = "red"
                } else if (data.value.INDICATION_VALUE < data.alarm.ALARM_GZONE_FROM || data.value.INDICATION_VALUE > data.alarm.ALARM_GZONE_TO){
                    // valid = false;
                    validColor = "yellow"
                }

                var diffMin = moment().diff(moment(moment(data.value.INDICATION_REGDATE).utc().format("YYYY-MM-DD HH:mm:SS")), 'minutes');
                if (diffMin > 17){
                    validColor = "darkgrey"
                }

                newO[id] = {value : data.value.INDICATION_VALUE, validColor: validColor, valid: valid, abbr: data.type.DEVICETYPE_ABBR,
                color: color};
                var state = Object.assign({}, _this.state, newO);
                // console.log(data, state);
                _this.setState(state)
                _this.setState(state)
                errorsCount = 0;
                setTimeout(() => {
                    _this.updateData(id, errorsCount);
                }, 10000);
            })
            .catch((err) => {
                errorsCount++;
                console.error("Ошибка при получении данных для датчика %s : %s", id, err ? JSON.stringify(err, null, 2) : '-');
                var newO = {}
                newO[id] = null;
                var state = Object.assign({}, _this.state, newO);
                _this.setState(state)
                setTimeout(() => {
                    _this.updateData(id, errorsCount);
                }, 30000);
            })
    }

    render() {
        var _this = this;
        var styleLine = {
            stroke: "blue",
            strokeWidth: 1
        };
        
        var count = 0;
        if (_this.props.ids.length)
            return (
                <g>
                    <line x1={this.props.x} y1={this.props.y} style={styleLine}
                          x2={this.props.x + 50} y2={this.props.up ? this.props.y - 200 : this.props.y + 200} />

                    {_this.props.ids.map(function (id, i){
                                    var indication = _this.state[id];
                                    var backgroundColor = indication ? indication.validColor : "white";
                                    var textColor = indication ? indication.color : "black";
                                    var value = indication && indication.value ? indication.value.toFixed(2) + indication.abbr  : '-';
                                    var y = _this.props.up ? _this.props.y - 200 - 30 - count*30 : _this.props.y + 200 + count*30;
                                    var x = _this.props.x + 50;
                                    count++;
                                    return <g key={i} id={id}>
                                        <rect x={x} y={y} width="90" height="30" fill={backgroundColor} stroke="blue"></rect>
                                        <text x={x + 45} y={y +22 } textAnchor="middle" fontSize="20" fill={textColor}>
                                            { value}
                                        </text>
                                    </g>;
                                })}
                </g>
            )
        else
            return null;
    }


}

export default Indicator;


