import React from 'react'
import {render} from 'react-dom'
import {Router, browserHistory} from 'react-router'
// import routes and pass them into <Router/>
import routes from './routes.jsx'

import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {reducer, getInitialState} from './reducers/reducer'


let store = createStore(reducer, getInitialState(window.APP_STATE))

render(
    <Provider store={store}>
            <Router routes={routes} history={browserHistory}/>
    </Provider>
    , document.getElementById('app'));




