import  React from 'react';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';
import AppView from './app.js';
import WithHeaderView from './with_header.js';
import LoginView from './login.js';
import LogoutView from './logout.js';
import NewReportView from './new_report.js';
import ReportEditView from './report_edit.js';
import ReportsView from './reports.js';
import DevicesView from './devices.js';
import NoMatch from './not_found.js';
import States from './states.js';
import FloorView from './floor.js';

import {requireAuthentication} from './chechAuth.js';

module.exports = (
    <Router history={browserHistory}>
        <Route path="/" component={AppView}>
            <Route  component={WithHeaderView}>
                <IndexRoute component={States}/>
                <Route path="floor/:floorNo" component={FloorView}/>
                <Route path="/login" component={LoginView}/>
                <Route path="/logout" component={LogoutView}/>
                <Route path="/new_report" component={requireAuthentication(NewReportView)}/>
                <Route path="/report_edit/:id" component={requireAuthentication(ReportEditView)}/>
                <Route path="/reports" component={ReportsView}/>
                <Route path="/devices" component={requireAuthentication(DevicesView)}/>
            </Route>
            <Route path="f/:floorNo" component={FloorView}/>
            <Route path="*" component={NoMatch}/>
        </Route>
    </Router >
)
