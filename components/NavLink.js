import React from 'react'
import { Link } from 'react-router'
require("./css/link.css");

export default React.createClass({
    render() {
        return <Link {...this.props} activeClassName="active"/>
    }
})