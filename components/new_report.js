import React from 'react';
var rest = require("./rest");
var Select = require('react-select');
require('react-select/dist/react-select.css');
var decimate = require('../utils').arrayDecimate;

var Datetime = require('react-datetime');
require('react-datetime/css/react-datetime.css');
var moment = require('moment');

var LineChart = require("react-chartjs").Line;

var DeviceTypeList = require('../db/device_type_list')

class NewReportView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rooms: [],
            selectedRoom: null,
            roomsDisabled: true,

            devicetypes: [],
            selectedDevicetypes: [],

            withTable: false,

            verified: true,

            desc: undefined,

            startDate: moment().add(-1, 'days').hours(12).minutes(0),
            endDate: moment().hours(12).minutes(0),

            newData: [],

            reportId: false,
            reportWasCorrected: false,

            buttonDisabled: false,
            error: null
        }
        this.getRooms = this.getRooms.bind(this);
        this.roomChange = this.roomChange.bind(this);
        this.getDevicetypes = this.getDevicetypes.bind(this);
        this.deviceTypeIsChecked = this.deviceTypeIsChecked.bind(this);
        this.deviceTypeIsChanged = this.deviceTypeIsChanged.bind(this);
        this.startChange = this.startChange.bind(this);
        this.endChange = this.endChange.bind(this);
        this.withTableChange = this.withTableChange.bind(this);
        this.verifiedChange = this.verifiedChange.bind(this);
        this.descChange = this.descChange.bind(this);
        this.getNewData = this.getNewData.bind(this);
        this.isDataNotEmptyForAllDevice = this.isDataNotEmptyForAllDevice.bind(this);
        this.createReport = this.createReport.bind(this);
        this.getPrint = this.getPrint.bind(this);
        this.onTrippleClick = this.onTrippleClick.bind(this);
        this.closeError = this.closeError.bind(this);
    }

    getRooms() {
        return rest.get('/api/rooms')
            .then((data) => {
                if (!data || !data.length) {
                    var state = Object.assign({}, this.state, {roomsDisabled: true, rooms: [], selectedRoom: null});
                    this.setError("Ни одного помещения", state);
                } else {
                    var newO = {rooms: data, selectedRoom: data[0]};
                    var state = Object.assign({}, this.state, newO);
                    // console.log("ROOM", data[0]);
                    this.setState(state, ()=>{
                        this.getDevicetypes();
                    })
                    return {options: data};
                }
            })
            .catch((err) => {
                console.error("Ошибка при получении помещений : %s", err ? JSON.stringify(err) : '-');
                var state = Object.assign({}, this.state, {roomsDisabled: true, rooms: [], selectedRoom: null});
                this.setError("Ошибка при получении помещений", state);
            })
    }

    getDevicetypes() {
        if (!this.state.selectedRoom) {
            var state = Object.assign({}, this.state, {devicetypes: [], selectedDevicetypes: []});
            this.setError("Не выбрана комната", state);
            return;
        }

        return rest.get('/api/rooms/' + this.state.selectedRoom.ROOM_ID + '/devicetypes?reporting=1')
            .then((data) => {
                if (!data || !data.length) {
                    var state = Object.assign({}, this.state, {devicetypes: [], selectedDevicetypes: []});
                    this.setError("Ни одного типа датчика", state);
                } else {
                    var selected = data.map(item => item.DEVICETYPE_ID);
                    var newO = {devicetypes: data, selectedDevicetypes: selected, roomsDisabled: false};
                    var state = Object.assign({}, this.state, newO);
                    this.setState(state);
                }
            })
            .catch((err) => {
                console.error("Ошибка при получении типов датчиков : %s", err);
                var state = Object.assign({}, this.state, {devicetypes: [], selectedDevicetypes: []});
                this.setError("Ошибка при получении типов датчиков", state);
            })
    }

    deviceTypeIsChecked(id) {
        return this.state.selectedDevicetypes.indexOf(id) > -1 ? true : false;
    }

    deviceTypeIsChanged(id, event) {
        var selectedDevicetypes = this.state.selectedDevicetypes.filter(item => item !== id);

        if (event.target.checked) {
            selectedDevicetypes.push(id);
        }
        this.setState(Object.assign({}, this.state, {
            selectedDevicetypes,
            newData: [],
            verified: true,
            desc: undefined,
            reportId: null,
            reportWasCorrected: false
        }))
    }

    closeError() {
        var state = Object.assign({}, this.state, {error: ''});
        this.setState(state)
    }

    setError(error, state) {
        var errState = state ? state : {};
        errState.error = error;
        var state = Object.assign({}, this.state, errState);
        this.setState(state);
    }

    checkErrors() {
        if (!this.state.selectedRoom) {
            this.setError('Не выбрана комната')
            return false;
        }
        if (!this.state.startDate || !this.state.endDate) {
            this.setError('Не выбран период отчета');
            return false;
        }

        if (!moment(this.state.startDate).isBefore(this.state.endDate)) {
            this.setError('Дата конца периода должна быть меньше даты начала');
            return false;
        }

        if (!this.state.selectedDevicetypes.length) {
            this.setError('Не выбран ни один датчик');
            return false;
        }
        return true
    }

    getNewData(keepReport, isForReport) {
        var state = keepReport ? Object.assign({}, this.state, {
            buttonDisabled: true,
            newData: null,
        }): Object.assign({}, this.state, {
            buttonDisabled: true,
            newData: null,
            reportId: null,
            reportWasCorrected: false
        })

        this.setState(state, ()=> {
            if (!this.checkErrors()) {
                setTimeout(()=> {
                    var state = Object.assign({}, this.state, {buttonDisabled: false});
                    this.setState(state)
                }, 300)
                return;
            }
            return rest.get('/api/room/indications/' + this.state.selectedRoom.ROOM_ID +
                '/' + this.state.startDate.format("YYYY-MM-DD HH:mm") +
                '/' + this.state.endDate.format("YYYY-MM-DD HH:mm") +
                '/' + this.state.selectedDevicetypes.join(',') +
                '/' + (isForReport ? 1 : 0))
                .then((data) => {
                    var isDataNotEmptyForAllDevice = this.isDataNotEmptyForAllDevice(data);
                    console.log("NEW DATA", data, isDataNotEmptyForAllDevice);
                    var state = Object.assign({}, this.state, {
                        newData: isDataNotEmptyForAllDevice ? this.formatData(data, this.state.withTable) : null,
                        buttonDisabled: false
                    });
                    this.setState(state);
                })
                .catch((err) => {
                    var state = Object.assign({}, this.state, {
                        buttonDisabled: false
                    });
                    this.setState(state);
                    console.error("Ошибка при получении данных с датчиков : %s", err);
                })
        });
    }

    createReport() {
        if (!this.checkErrors())
            return;

        var state = Object.assign({}, this.state, {
            reportId: null,
            buttonDisabled: true
        })
        this.setState(state, ()=> {
            return rest.post('/api/roomchecks/new/', {
                roomid: this.state.selectedRoom.ROOM_ID,
                from: this.state.startDate.format("YYYY-MM-DD HH:mm"),
                to: this.state.endDate.format("YYYY-MM-DD HH:mm"),
                verified: this.state.verified,
                devicetypes: this.state.selectedDevicetypes.join(','),
                desc: this.state.desc
            })
                .then((data) => {
                    console.log("createReport", data);
                    var state = Object.assign({}, this.state, {
                        reportId: data.roomcheckId,
                        buttonDisabled: false
                    });
                    this.setState(state);

                })
                .then(() => {
                    this.getNewData(true, true);
                })
                .catch((err) => {
                    var state = Object.assign({}, this.state, {
                        buttonDisabled: false
                    });
                    this.setState(state);
                    console.log(err);
                    console.error("Ошибка при создании отчета : %s", err.responseJSON.message);
                    this.setError(err.responseJSON.message)

                })
        });
    }

    onTrippleClick(e) {
        if (e.detail !== 3) {
            return;
        }

        if (!this.state.reportId) {
            return;
        }

        if (!this.checkErrors())
            return;

        return rest.post('/api/roomchecks/amend', { roomcheckid: this.state.reportId})
            .then((data) => {
                console.log("correct", data);
                var state = Object.assign({}, this.state, {
                    reportWasCorrected: true
                });
                this.setState(state);
            })
            .then(() => {
                this.getNewData(true, true);
            })
            .catch((err) => {
                console.error("Ошибка при корректировке отчета : %s", err.message);
            })
    }

    init_chart(label, rgba) {
        return {
            label: label,
            fill: false,
            strokeColor: rgba,
            pointColor: rgba,
            backgroundColor: rgba,
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: rgba,
            data: []
        };
    }

    isDataNotEmptyForAllDevice(data) {
        if (!data || !data.length) {
            return false;
        }
        var emptyDevices = 0;
        for (var key in data) {
            var o = data[key];
            if (!o.data || !o.data.length) {
                emptyDevices++;
            }
        }
        if (emptyDevices == data.length)
            return false;
        else
            return true;
    }

    formatData(data, withTable) {
        if (!data || !data.length) {
            throw "Не получены данные с датчиков";
            return;
        }
        var result = [];

        for (var key in data) {
            var o = data[key];
            var table = [];

            if (!o.data || !o.data.length) {
                console.log("Не получены данные с датчика: ", o.device.DEVICE_NAME, o.device.DEVICE_ID);
                continue;
            }

            if (withTable) {
                for (var i = 0; i < o.data.length; i++) {
                    table.push({
                        date: o.regdates[i],
                        val: o.data[i],
                        maxG: o.device.alarm.ALARM_GZONE_TO,
                        minG: o.device.alarm.ALARM_GZONE_FROM,
                        maxY: o.device.alarm.ALARM_YZONE_TO,
                        minY: o.device.alarm.ALARM_YZONE_FROM,
                    })
                }
            }

            var data1 = {
                labels: decimate(o.regdates, o.maxCount),
                // labels: o.regdates,
                datasets: []
            };
            var needLegend = false;
            var color = "rgba(151,187,205,1)";
            switch (o.device.type.DEVICETYPE_ID) {
                case DeviceTypeList.types.temperature:
                    needLegend = true;
                    color = "rgba(0,100,0,1)";
                    break;
                case DeviceTypeList.types.humidity:
                    needLegend = true;
                    color = "rgba(0,0,255,1)";
                    break;
                case DeviceTypeList.types.pressure:
                    needLegend = true;
                    color = "rgba(160,32,240,1)";
                    break;
            }
            var ob = this.init_chart("Датчик", color);
            var gMax = this.init_chart("граница оптимальных значений", "rgba(255,255,0,1)");
            var gMin = this.init_chart("граница оптимальных значений", "rgba(255,255,0,1)");
            var yMax = this.init_chart("граница допустимых значений", "rgba(255,0,0,1)");
            var yMin = this.init_chart("граница допустимых значений", "rgba(255,0,0,1)");

            ob.data = o.data;
            if (!isNaN(o.device.alarm.ALARM_GZONE_TO) && !isNaN(o.device.alarm.ALARM_GZONE_FROM)
                && !isNaN(o.device.alarm.ALARM_YZONE_TO) && !isNaN(o.device.alarm.ALARM_YZONE_FROM)) {

                for (var i = 0; i < ob.data.length; i++) {
                    gMax.data.push(o.device.alarm.ALARM_GZONE_TO);
                    gMin.data.push(o.device.alarm.ALARM_GZONE_FROM);
                    yMax.data.push(o.device.alarm.ALARM_YZONE_TO);
                    yMin.data.push(o.device.alarm.ALARM_YZONE_FROM);
                }

            }
            data1.datasets.push(yMax);
            data1.datasets.push(gMax);
            data1.datasets.push(ob);
            data1.datasets.push(gMin);
            data1.datasets.push(yMin);

            result.push({
                chart: data1,
                name: o.device.DEVICE_NAME,
                needLegend: needLegend,
                table: table,
                color: color
            })
        }
        console.log("formatData", result);
        return result;
    }

    getPrint() {
        if (!this.state.reportId)
            return;
        window.open('/print/roomchecks/' + this.state.reportId);
    }

    roomChange(val) {
        // console.log("roomChange",  val);
        var state = Object.assign({}, this.state, {
            selectedRoom: val,
            newData: [],
            verified: true,
            desc: undefined,
            reportId: null,
            reportWasCorrected: false
        })
        this.setState(state, () => {
            this.getDevicetypes();
        });
    }

    startChange(date) {
        // console.log(date);
        this.setState(Object.assign({}, this.state, {
            startDate: date,
            newData: [],
            verified: true,
            desc: undefined,
            reportId: null,
            reportWasCorrected: false
        }))
    }

    endChange(date) {
        // console.log(date);
        this.setState(Object.assign({}, this.state, {
            endDate: date,
            newData: [],
            verified: true,
            desc: undefined,
            reportId: null,
            reportWasCorrected: false
        }))
    }

    withTableChange() {
        this.setState(Object.assign({}, this.state, {
            withTable: !this.state.withTable
        }))
    }

    verifiedChange() {
        console.log('vvv', !this.state.verified);
        this.setState(Object.assign({}, this.state, {
            verified: !this.state.verified
        }))
    }

    descChange(event) {
        this.setState(Object.assign({}, this.state, {
            desc: event.target.value
        }))
    }

    render() {

        var chartOptions = {
            datasetFill: false,
            pointDot: false,
            animation: false,
            showTooltips: false,
            scales: {
                yAxes: [{
                    ticks: {
                        max: 5,
                        min: 0,
                        stepSize: 0.5
                    }
                }]
            },
            multiTooltipTemplate: "<%=value %>"
        };

        return <div>
            {this.state.error ? <div className="alert alert-danger alert-dismissible my-error">
                <button type="button" className="close" aria-label="Close" onClick={this.closeError}><span aria-hidden="true">&times;</span></button>
                {this.state.error}
            </div> : ''}
            <h3>Новый отчет</h3>
            <br/>
            <div className="panel panel-default" style={{width: '800px'}}>
                <div className="panel-body">
                    <form className="form-horizontal">
                        <div className="form-group">
                            <label className="col-xs-4  control-label">Выберите помещение</label>
                            <div className="col-xs-8">
                                <Select.Async
                                    name="form-room"
                                    value={this.state.selectedRoom}
                                    valueKey="ROOM_ID"
                                    labelKey="ROOM_NAME"
                                    loadOptions={this.getRooms}
                                    onChange={this.roomChange}
                                    disabled={this.roomsDisabled}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-xs-4  control-label">Выберите типы датчиков</label>
                            <div className="col-xs-8">
                                {this.state.devicetypes.length ?
                                    this.state.devicetypes.map((value, i) => {
                                        return <div key={i} className="checkbox">
                                                <label >
                                                    <input type="checkbox"
                                                           checked={this.deviceTypeIsChecked(value.DEVICETYPE_ID)}
                                                           onChange={this.deviceTypeIsChanged.bind(this, value.DEVICETYPE_ID)}
                                                    />{value.DEVICETYPE_NAME}
                                                </label>
                                            </div>
                                    })
                                    : 'Нет типов датчиков'}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-xs-4 control-label">Выберите период</label>
                            <div className="col-xs-4">
                                <Datetime
                                    dateFormat="DD.MM.YYYY"
                                    locale='ru'
                                    value={this.state.startDate}
                                    closeOnSelect={true}
                                    onChange={this.startChange}/>
                            </div>
                            <div className="col-xs-4">
                                <Datetime
                                    dateFormat="DD.MM.YYYY"
                                    locale='ru'
                                    value={this.state.endDate}
                                    closeOnSelect={true}
                                    onChange={this.endChange}/>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-xs-offset-4 col-xs-4">
                                <div className="checkbox">
                                    <label >
                                        <input type="checkbox"
                                               checked={this.state.withTable}
                                               onChange={this.withTableChange}
                                        /> В виде таблицы
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-xs-offset-4 col-xs-4">
                                <button type="button" className="btn btn-primary " disabled={this.state.buttonDisabled}
                                        onClick={this.getNewData}>Получить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {this.state.newData ?
                this.state.newData.map((value, i) => {
                    return <div key={i} style={{marginBottom: '10px'}}>
                        <label>{value.name ? value.name : 'Датчик'}</label>
                        {value.needLegend ?
                            <div>
                                <div><span className="label" style={{backgroundColor: "red", color: "red"}}>-</span> -
                                    границы допустимых значений (max)
                                </div>
                                <div><span className="label"
                                           style={{backgroundColor: "yellow", color: "yellow"}}>-</span> -
                                    границы оптимальных значений (max)
                                </div>
                                <div><span className="label" style={{backgroundColor: value.color, color: value.color}}>-</span> -
                                    Датчик
                                </div>
                                <div><span className="label"
                                           style={{backgroundColor: "yellow", color: "yellow"}}>-</span> -
                                    границы допустимых значений (min)
                                </div>
                                <div><span className="label" style={{backgroundColor: "red", color: "red"}}>-</span> -
                                    границы оптимальных значений (min)
                                </div>
                            </div> : ''}

                        <div>
                            <LineChart key={i} data={value.chart} options={chartOptions} width={1100} height={500} onClick={this.onTrippleClick} />
                        </div>
                        {value.table && value.table.length ?
                            <table className="table table-bordered" style={{maxWidth: '1000px'}}>
                                <tr>
                                    <th>Дата</th>
                                    <th>Значение</th>
                                    <th>Желтая зона Min</th>
                                    <th>Желтая зона Max</th>
                                    <th>Красная зона Min</th>
                                    <th>Красная зона Max</th>
                                </tr>
                                {value.table.map(function (row, j) {
                                    return <tr key={j}>
                                        <td>{row.date}</td>
                                        <td>{row.val.toFixed(1)}</td>
                                        <td>{row.minG}</td>
                                        <td>{row.maxG}</td>
                                        <td>{row.minY}</td>
                                        <td>{row.maxY}</td>
                                    </tr>
                                })}
                            </table>
                            : ""}
                    </div>
                })
                : 'С такими параметрами данных с датчиков нет'}
            <hr/>
            {this.state.newData && this.state.newData.length && !this.state.reportId ?
                <div style={{marginBottom: '20px'}}>
                    <div className="checkbox">
                        <label >
                            <input type="checkbox"
                                   checked={this.state.verified}
                                   onChange={this.verifiedChange}
                            /> Одобрить данные для отчета
                        </label>
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" rows="5" value={this.state.desc} onChange={this.descChange}></textarea>
                    </div>
                    <button type="button" className="btn btn-primary " style={{marginRight:'10px'}}
                            disabled={this.state.buttonDisabled}
                            onClick={this.createReport}>Создать отчет
                    </button>
                </div> : null}
            {this.state.newData && this.state.newData.length && this.state.reportId ?
            <div style={{marginBottom: '20px'}}>
                <button type="button" className="btn btn-primary " style={{marginRight:'10px'}}
                        disabled={this.state.buttonDisabled}
                        onClick={this.getPrint}>Печать отчета
                </button>
                {this.state.reportId && <div>Отчет создан</div>}
                {this.state.reportWasCorrected && <div>Данные отчета скорректированы</div>}
                {this.state.desc && <div>Коментарий:<pre>{this.state.desc}</pre></div>}
            </div>: null}
        </div>
    }

}

export default NewReportView;
