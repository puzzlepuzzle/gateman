import {
    LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS, LOGIN_UPDATE, LOGOUT_FAILURE, LOGOUT_REQUEST
} from './../actions/login_actions'


export default function login_reducer(state, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
        case LOGOUT_REQUEST:
            return {
                isFetching: true,
                isAuthenticated: false,
            }
        case LOGIN_SUCCESS:
            return {
                isFetching: false,
                isAuthenticated: true,
                info: action.info,
                role: action.info.role
            }
        case LOGOUT_SUCCESS:
            return {
                isFetching: false,
                isAuthenticated: false
            }
        case LOGIN_FAILURE:
        case LOGOUT_FAILURE:
            return {
                isFetching: false,
                isAuthenticated: false,
                errorMessage: action.message
            }
        case LOGOUT_SUCCESS:
            return {
                isFetching: false,
                isAuthenticated: false
            }
        case LOGIN_UPDATE:
            return {
                isAuthenticated: action.isAuthenticated
            }
        default:
            return state
    }
}
