import {
    ERROR, CLEAN_ERROR
} from './../actions/error_actions'


export default function error_reducer(state, action) {
    switch (action.type) {
        case ERROR:
            return action.error;
        case CLEAN_ERROR:
            return '';
        default:
            return state
    }
}
