import error_reducer from './error_reducer';
import login_reducer from './login_reducer';

export function getInitialState (json) {
    console.log('getInitialState', json)
    if (json && json.isAuth){
        return {
            login: {
                isFetching: false,
                isAuthenticated: true,
                role: json.role
            }
        }
    } else {
        return {
            login: {
                isFetching: false,
                isAuthenticated: false
            }
        }
    }
}

export function reducer(state, action) {
    return {
        error: error_reducer(state.error, action),
        login: login_reducer(state.login, action)
    }
}