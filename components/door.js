import React from 'react'
var rest = require("./rest");

class Door extends React.Component {
    static get propTypes() {
        return {
            x: React.PropTypes.number.isRequired,
            y: React.PropTypes.number.isRequired,
            id: React.PropTypes.number.isRequired
        };
    }


    constructor(props) {
        super(props);
        this.state = {};
        this.errorMax = 5;
        this.canUpdate = true;
    }


    componentDidMount() {
        var _this = this;
        _this.updateIndicators();
    }

    componentWillUnmount() {
        // console.log("componentWillUnmount",this.props.ids)
        this.canUpdate = false;
    }

    updateIndicators() {
        var _this = this
        _this.updateData(_this.props.id, 0)
    }

    updateData(id, errorsCount) {
        var _this = this;
        if (!_this.canUpdate){
            return;
        }
        if (errorsCount > _this.errorMax){
            console.error("Слишком много ошибок при получении данных для двери %s", id );
            var state = Object.assign({}, _this.state, {value : null, color: "blue", fill: "darkgray"});
            _this.setState(state)
            return;
        }
        return rest.get('/api/device/lastindication/' + id)
            .then((data) => {
                var color = data.value.INDICATION_VALUE ? "blue": "blue";
                var fill = data.value.INDICATION_VALUE ? "red": "green";

                var state = Object.assign({}, _this.state,
                    {value : data.value.INDICATION_VALUE, color: color, fill: fill});
                // console.log(data, state);
                _this.setState(state)
                errorsCount = 0;
                setTimeout(() => {
                    _this.updateData(id, errorsCount);
                }, 3000);
            })
            .catch((err) => {
                errorsCount++;
                console.error("Ошибка при получении данных для двери %s : %s", id, err ? JSON.stringify(err, null, 2) : '-');

                var state = Object.assign({}, _this.state, {value : null, color: "blue", fill: "darkgray"});
                _this.setState(state)
                setTimeout(() => {
                    _this.updateData(id, errorsCount);
                }, 10000);
            })
    }

    render() {
        var _this = this;
        var styleLine = {
            stroke: _this.state.color,
            strokeWidth: 2,
            fill: _this.state.fill
        };

        return (
            <g>
                <rect x={this.props.x} y={this.props.y} width="15" height="15" style={styleLine}></rect>
            </g>
        )
    }


}

export default Door;


