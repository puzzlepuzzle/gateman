import React from 'react';
var rest = require("./rest");
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import { ReactTableDefaults } from 'react-table'
import NavLink from './NavLink';
import {connect} from 'react-redux';

var moment = require('moment');


Object.assign(ReactTableDefaults, {
    previousText: 'Предыдущаяя',
    nextText: 'Следующая',
    loadingText: 'Загрузка...',
    noDataText: 'Не найдено ни одного отчета',
    pageText: 'Страница',
    ofText: 'из',
    rowsText: 'отчетов',
})

class ReportsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages: null,
            loading: true,
            apiProps: {}
        }
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    handleButtonClick(e, state) {
        console.log("click", e, state);
        window.open('/print/roomchecks/' + state.row.ROOMCHECK_ID);
    }

    checkFilterDate(value) {
        var date1 = moment(value, "DD.MM.YYYY HH:mm", true);
        var date2 = moment(value, "DD.MM.YYYY", true);

        if (date1.isValid()) {
            return date1.format("YYYY-MM-DD HH:mm");
        } else if (date2.isValid()) {
            return date2.format("YYYY-MM-DD HH:mm");
        }

        return null;
    }

    render() {
        const columns = [{
                Header: 'ID',
                accessor: 'ROOMCHECK_ID',
                show: false
            }, {
                Header: 'Комната',
                accessor: 'ROOM_NAME',
                filterable: false,
                maxWidth: 150,
            },{
                Header: 'Дата создания',
                accessor: 'DATE_CREATE', // String-based value accessors!
                filterable: false,
                maxWidth: 150,
                Cell: row => (
                    <div>{moment(row.value).utc().format('DD/MM/YYYY')}</div>
                )
            }, {
                Header: 'С',
                accessor: 'ROOMCHECK_FROM',
                filterable: true,
                maxWidth: 150,
                Cell: row => (
                    <div>{moment(row.value).utc().format('DD.MM.YYYY HH:mm')}</div>
                )
            },{
                Header: 'По',
                accessor: 'ROOMCHECK_TO',
                filterable: true,
                maxWidth: 150,
                Cell: row => (
                    <div>{moment(row.value).utc().format('DD.MM.YYYY HH:mm')}</div>
                )
            }, {
                Header: 'Ответственный',
                accessor: 'PERSON_NAME',
                filterable: false
            }, {
                Header: 'Одобрен',
                accessor: 'VERIFIED',
                filterable: false,
                maxWidth: 100,
                Cell: row => (
                    <div>{row.value ? 'Да' : 'Нет'}</div>
                )
            },{
                Header: 'Печать',
                id: 'click-me-button',
                filterable: false,
                Cell: rowState => (
                    <div>
                        <button type="button" className="btn btn-primary "
                                onClick={(e) => this.handleButtonClick(e, rowState)}>Печать
                        </button>
                        {this.props.role && [1,3].includes(this.props.role) ?
                            <NavLink to={"/report_edit/" + rowState.row.ROOMCHECK_ID} className="btn" ><span className="glyphicon glyphicon-edit"></span></NavLink> : ''}
                    </div>)
            }];

            return  <ReactTable
                columns={columns}
                data={this.state.data} // should default to []
                 sortable={false}
                 filterable={true}
                 defaultPageSize={20}
                 className="-striped -highlight"
                 pages={this.state.pages} // should default to -1 (which means we don't know how many pages we have)
                 loading={this.state.loading}
                 manual // informs React Table that you'll be handling sorting and pagination server-side
                 onFetchData={(state, instance) => {
                     console.log("onFetch", state, instance);

                     var apiProps = {limit: state.pageSize, offset: state.pageSize * state.page};
                     if (state.filtered.length) {
                         state.filtered.map(item => {
                            if (item.id === 'ROOMCHECK_FROM') {
                                apiProps.from = this.checkFilterDate(item.value) || this.state.apiProps.from;
                            } else if (item.id === 'ROOMCHECK_TO') {
                                apiProps.to = this.checkFilterDate(item.value) || this.state.apiProps.to;
                            }
                        })
                     }

                     if (this.state.apiProps.limit === apiProps.limit &&
                         this.state.apiProps.offset === apiProps.offset &&
                         this.state.apiProps.from == apiProps.from &&
                         this.state.apiProps.to == apiProps.to) {
                         return;
                     }

                     this.setState(Object.assign({}, this.state, {loading: true, apiProps}));

                     rest.post('/api/roomchecks', apiProps)//state.page})
                         .then((data) => {
                             var newState = Object.assign({}, this.state, {
                                 data,
                                 loading: false,
                                 pages: data.length ? Math.floor(data[0]._COUNT/state.pageSize) : -1
                             });
                             this.setState(newState);
                         })
                         .catch((err) => {
                             console.error("Ошибка при получении отчетов : %s", err ? JSON.stringify(err) : '-');
                             var newState = Object.assign({}, this.state, {data: [], loading: false});
                             this.setState(newState);
                         })
                 }}
            />
    }
}

function select(state) {
    return {
        role: state.login.role
    }
}
export default connect(select)(ReportsView);

