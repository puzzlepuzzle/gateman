import React from 'react'
import Indicator from './indicator';
import Alarm from './alarm';
import Door from './door';

class Room extends React.Component {
    static get propTypes() {
        return {
            backgroundPath: React.PropTypes.string.isRequired,
            indicators: React.PropTypes.arrayOf(
                React.PropTypes.shape({
                    x: React.PropTypes.number.isRequired,
                    y: React.PropTypes.number.isRequired,
                    up: React.PropTypes.bool,
                    ids: React.PropTypes.arrayOf(React.PropTypes.number).isRequired
                })
            ).isRequired,
            alarms: React.PropTypes.arrayOf(
                React.PropTypes.shape({
                    x: React.PropTypes.number.isRequired,
                    y: React.PropTypes.number.isRequired,
                    id: React.PropTypes.number.isRequired
                })
            ).isRequired,
            doors: React.PropTypes.arrayOf(
                React.PropTypes.shape({
                    x: React.PropTypes.number.isRequired,
                    y: React.PropTypes.number.isRequired,
                    id: React.PropTypes.number.isRequired
                })
            ).isRequired,
            roomId: React.PropTypes.string.isRequired
        };
    }

    constructor(props) {
        super(props);
    }

    render() {
        var _this = this;
        var styleDiv = {
            position: 'relative',
        };
        var styleImg = {
            width: '1500px',

        };
        return <div style={styleDiv}>
            <svg height="100%" width="100%" viewBox="0 0 1500 1000" preserveAspectRatio="xMidYMid meet">
                <image xlinkHref={this.props.backgroundPath} x="0" y="0" height="1000px" width="1500px"/>
                {this.props.indicators.map(function (value, i) {
                    return <Indicator key={"" + _this.props.roomId + i} x={value.x} y={value.y} up={value.up} ids={value.ids}/>;
                })}
                {this.props.alarms.map(function (value, i) {
                    return <Alarm key={"" + _this.props.roomId + i} x={value.x} y={value.y} id={value.id}/>;
                })}
                {this.props.doors.map(function (value, i) {
                    return <Door key={"" + _this.props.roomId + i} x={value.x} y={value.y} id={value.id}/>;
                })}
            </svg>
        </div>
    }
}

export default Room;