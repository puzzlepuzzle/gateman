import React from 'react'
var rest = require("./rest");

class IndicationDescription extends React.Component {

    render() {
        var _this = this;
        var styleLine = {
            stroke: "blue",
            strokeWidth: 2
        };

        return (
            <svg height="200" width="1000">
                <rect x="10" y="10" width="15" height="15" style={styleLine} fill="green"></rect>
                <text x="30" y="23"  fontSize="15" >Дверь закрыта</text>

                <rect x="300" y="10" width="15" height="15" style={styleLine} fill="red"></rect>
                <text x="320" y="23"  fontSize="15" >Дверь открыта</text>

                <rect x="600" y="10" width="15" height="15" style={styleLine} fill="darkgrey"></rect>
                <text x="620" y="23"  fontSize="15" >Дверь не доступна</text>

                <circle cx="17" cy="50" r="10" style={styleLine} fill="green"/>
                <text x="30" y="55"  fontSize="15" >Сигнализация выключена</text>

                <circle cx="307" cy="50" r="10" style={styleLine} fill="red"/>
                <text x="320" y="55"  fontSize="15" >Сигнализация включена</text>

                <circle cx="607" cy="50" r="10" style={styleLine} fill="darkgrey"/>
                <text x="620" y="55"  fontSize="15" >Сигнализация не доступна</text>

                <rect x="10" y="80" width="90" height="30" style={styleLine} fill="none"></rect>
                <text x="105" y="100"  fontSize="15" >Текущее показание</text>

                <rect x="10" y="120" width="90" height="30" style={styleLine} fill="yellow"></rect>
                <text x="105" y="140"  fontSize="15" >Показание в желтой зоне</text>

                <rect x="300" y="80" width="90" height="30" style={styleLine} fill="red"></rect>
                <text x="395" y="100"  fontSize="15" >Показание в красной зоне</text>

                <rect x="600" y="80" width="90" height="30" style={styleLine} fill="darkgrey"></rect>
                <text x="695" y="100"  fontSize="15" >Показание устарело</text>

            </svg>
        )
    }


}

export default IndicationDescription;


