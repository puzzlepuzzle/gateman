var util = require('util');
var http = require('http');

// ошибки для выдачи посетителю
function HttpError(status, message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, HttpError);

    this.status = status;
    this.message = message || http.STATUS_CODES[status] || "Error";
}
util.inherits(HttpError, Error);
HttpError.prototype.name = 'HttpError';


function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);

    this.status = 401;
    this.message = message || "Вы не зарегистрировались.";
}
util.inherits(AuthError, Error);
AuthError.prototype.name = 'AuthError';


function ApiError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, ApiError);

    this.status = 405;
    this.message = message || "Ошибка вызова API сервиса";
}
util.inherits(ApiError, Error);
ApiError.prototype.name = 'ApiError';


exports.HttpError = HttpError;
exports.AuthError = AuthError;
exports.ApiError = ApiError;