var Webpack = require('webpack');

module.exports = {

    entry: [
        "./components/main.js",
        ],
    output: {
        //все собранные файлы будут сохраняться в path
        path: __dirname + '/public',
        publicPath: "/public/",
        //название файла, в котором будет объеденины все js файлы
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(woff2|eot|woff|ttf|svg|png|jpg)$/,
                loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
            },
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
};

