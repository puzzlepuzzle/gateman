var log = require('../log')(module);
var AuthError = require('../errors').AuthError;

module.exports = function(req, res, next) {

    log.debug("isAuthenticated:", req.isAuthenticated(), "user:", req.user);
    if (req.isAuthenticated() && req.user && req.user.USER_ID) {
        next();
    } else {
        var err = new AuthError();
        next(err);
    }


};