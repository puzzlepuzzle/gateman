var express = require('express');
var router = express.Router();
var log = require('../log')(module);
var AuthError = require('../errors').AuthError;
var Error = require("../errors")
module.exports = function(passport){
    router.post('/login', function (req, res, next) {
        log.debug('login',req.body);
            passport.authenticate('login', function (err, user, info) {
                if (err) {
                    return next(err)
                }
                if (!user) {
                    return next(new AuthError(info.message));
                }

                req.logIn(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.json(info);
                });
            })(req, res, next);
        }
    );

    router.get('/logout', function (req, res) {
        req.logout();
        res.json("logout ok");
    });


    router.get('/check_auth', function (req, res, next) {
        // if user is authenticated in the session, carry on
        var r = {
            isAuth: req.isAuthenticated(),
            role: req.isAuthenticated() ? req.user.ROLE_ID : "",
            userLogin: req.isAuthenticated() ? req.user.USER_LOGIN : "",
            userId: req.isAuthenticated() ? req.user.USER_ID : ""
        }

        if (req.isAuthenticated() && req.user.DISABLED) {
            log.debug('user ' + req.user.USER_LOGIN + ' is disabled!')
            req.logout();
            r.isAuth = false;
        }

        log.debug('/api/check_auth', r)
        res.json(r);
    });

    router.get('/*', function(req, res, next) {
        var err = new Error.HttpError(404, 'Api auth не найдено');
        next(err);
    });

    return router;
};

