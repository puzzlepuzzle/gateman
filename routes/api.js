var express = require('express');
var router = express.Router();
var log = require('../log')(module);
var Promise = require("bluebird");
var Indication = require("./../db/models/indication");
var Avgind = require("./../db/models/avgind");
var Device = require("./../db/models/device");
var DeviceSpec = require("./../db/models/devicespec");
var Devicetype = require("./../db/models/devicetype");
var Room = require("./../db/models/room");
var Error = require("./../errors")
var decimate = require('../utils').arrayDecimate;
var moment = require('moment');
var config = require('../config');
var AlarmSingleton = require("../alarm/alarm_singleton.js");
var Procs = require("./../db/models/procs");
var DeviceTypeList = require("../db/device_type_list.js");

router.get('/fake', function (req, res, next) {
    log.info(req.url, req.params);
    res.end("fake request")
});

router.get('/alarms/off', function (req, res, next) {
    log.debug(req.url, req.params);
    if (!+config.get("start_alarm")) {
        res.end("Can't do this.");
        return;
    }
    return new AlarmSingleton().offAllAlarms()
        .then(function () {
            res.end("OK")
        })
        .catch(function (err) {
            next(err);
        })
});


router.get('/device/lastindication/:deviceid', function (req, res, next) {
    log.debug(req.url, req.params);
    var device_id = req.params.deviceid;
    if (!device_id) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /device/lastindication/:deviceid'));
        return;
    }
    Indication.getLastByDevice(device_id)
        .then(function (indicationInfo) {
            if (!indicationInfo) {
                next(new Error.ApiError('Не получены данные для ' + device_id));
                return;
            }

            res.json({
                value: indicationInfo.indication,
                type: indicationInfo.type,
                alarm: indicationInfo.alarm
            })
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/device/lastaverageindication/:deviceid', function (req, res, next) {
    log.debug(req.url, req.params);
    var device_id = req.params.deviceid;
    if (!device_id) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /device/lastaverageindication/:deviceid'));
        return;
    }
    Avgind.getLastByDevice(device_id)
        .then(function (indicationInfo) {
            if (!indicationInfo) {
                next(new Error.ApiError('Не получены данные для ' + device_id));
                return;
            }

            res.json({
                value: indicationInfo.indication,
                type: indicationInfo.type,
                alarm: indicationInfo.alarm
            })
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/param/stealth', function (req, res, next) {
    log.debug(req.url, req.params);

    return Procs.getStealthMode()
        .then(function (value) {
            res.json({
                value: value
            })
        })
        .catch(function (err) {
            next(err);
        })
});

router.post('/param/stealth/switch', function (req, res, next) {
    log.debug(req.url, req.params);

    return Procs.switchStealthMode()
        .then(function () {
            res.end("OK")
        })
        .catch(function (err) {
            next(err);
        })
});

router.post('/roomchecks', function (req, res, next) {
    log.debug(req.url, req.params);

    if (!req.body.limit || !req.body.offset) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемые параметры :limit, :offset'));
        return;
    }

    return Procs.getRoomchecks(req.body.from, req.body.to, req.body.limit, req.body.offset)
        .then(function (roomchecks) {
            res.json(roomchecks)
        })
        .catch(function (err) {
            next(err);
        })
});

router.use(require('../middleware/authCheck'));

router.get('/room/indications/:roomid/:from/:to/:devicetypes/:corrected', function (req, res, next) {
    log.debug(req.url, req.params);
    var roomid = req.params.roomid;
    var date_from = req.params.from;
    var date_to = req.params.to;
    var corrected = req.params.corrected;
    var devicetypes = req.params.devicetypes;
    if (!roomid || !date_from || !date_from || !devicetypes || !corrected) {
        next(new Error.ApiError(
            'Не все параметры переданы для вызова. Ожидаемый формат ' +
            '/room/indications/:roomid/:from/:to/:devicetypes/:corrected'));
        return;
    }

    var deviceTypeIds = devicetypes.split(',');

    return Device.getByRoomWithAlarmAndType(roomid)
        .then(function (devices) {
            // console.log(devices);
            if (!devices || !devices.length) {
                throw new Error.ApiError(' Не получено ни одного устройства');
                return;
            }

            var promises = [];
            for (var key in devices) {
                if (deviceTypeIds.indexOf(devices[key].type.DEVICETYPE_ID.toString()) === -1) {
                    continue;
                }
                promises.push((function (device) {
                    return Procs.getIndicationsByDeviceFromTo(device.DEVICE_ID, date_from, date_to, corrected)
                    // return Indication.getByDeviceFromTo(device.DEVICE_ID, date_from, date_to)
                        .then(function (data) {
                            // console.log("++++++++++++");
                            // console.log(device);
                            // console.log("--------");
                            // console.log("============");
                            var result = {
                                device: device,
                                data: [],
                                regdates: [],
                                maxCount: config.get('chart_points_count')
                            }
                            for (var key in data) {
                                var o = data[key];
                                result.data.push(o.INDICATION_VALUE);
                                result.regdates.push(moment.utc(o.INDICATION_REGDATE).format("DD.MM HH.mm"));
                            }

                            return result
                        })
                })(devices[key]))
            }

            return Promise.all(promises)
        })
        .then(function (result) {
            res.json(result)
        })
        .catch(function (err) {
            next(err);
        })
});

router.post('/roomchecks/new', function (req, res, next) {
    log.debug(req.url, req.body);

    if (!req.body.roomid || !req.body.from || !req.body.to || !req.body.devicetypes) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемые параметры :roomid, :from, :to, :verified, :devicetypes'));
        return;
    }

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 3){
        next(new Error.ApiError('Права на создание отчета имеют только руководители или администратор'));
        return;
    }
    if (req.body.verified == true || req.body.verified == 'true' || req.body.verified == 1 || req.body.verified == '1'){
        req.body.verified = true;
    } else {
        req.body.verified = false;
    }

    return Procs.createRoomheck(req.body.roomid, req.body.from, req.body.to, req.user.USER_ID, req.body.verified, req.body.devicetypes, req.body.desc)
        .then(function (roomcheckId) {
            res.json({
                roomcheckId: roomcheckId
            })
        })
        .catch(function (err) {
                next(new Error.ApiError(err.message));
        })
});

router.post('/roomchecks/update', function (req, res, next) {
    log.debug(req.url, req.body);

    if (!req.body.roomcheckid || !req.body.from || !req.body.to || !req.body.devicetypes) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемые параметры :roomcheckid, :from, :to, :verified, :devicetypes'));
        return;
    }

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 3){
        next(new Error.ApiError('Права на изменение отчета имеют только руководители или администратор'));
        return;
    }
    if (req.body.verified == true || req.body.verified == 'true' || req.body.verified == 1 || req.body.verified == '1'){
        req.body.verified = true;
    } else {
        req.body.verified = false;
    }

    return Procs.updateRoomcheck(req.body.roomcheckid, req.body.from, req.body.to, req.user.USER_ID, req.body.verified, req.body.devicetypes, req.body.desc)
        .then(function (roomcheckId) {
            res.json({
                roomcheckId: roomcheckId
            })
        })
        .catch(function (err) {
                next(new Error.ApiError(err.message));
        })
});

router.get('/roomchecks/:roomcheckid/devices', function (req, res, next) {
    log.debug(req.url, req.params);

    if (!req.params.roomcheckid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /roomcheck/:roomcheckid/devices'));
        return;
    }

    return Procs.getDevicesForRoomcheck(req.params.roomcheckid)
        .then(function (devices) {
            res.json({
                devices: devices,
                canSign: req.user.ROLE_ID == 1 || req.user.ROLE_ID == 3
            })
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/roomchecks/:roomcheckid/', function (req, res, next) {
    log.debug(req.url, req.params);

    if (!req.params.roomcheckid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /roomcheck/:roomcheckid'));
        return;
    }

    return Procs.getRoomcheck(req.params.roomcheckid)
        .then(function (item) {
            res.json(item)
        })
        .catch(function (err) {
            next(err);
        })
});

router.post('/roomchecks/amend', function (req, res, next) {
    log.debug(req.url, req.body);

    if (!req.body.roomcheckid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый  параметр :roomcheckid'));
        return;
    }

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 3){
        next(new Error.ApiError('Права на изменение отчета имеют только руководители или администратор'));
        return;
    }

    return Procs.amendRoomcheck(req.body.roomcheckid)
        .then(function () {
            res.end("report was corrected")
        })
        .catch(function (err) {
            next(err);
        })
});


router.get('/rooms', function (req, res) {
    log.debug(req.url, req.params);

    Room.getAll()
        .then(function (rooms) {
            res.json(rooms)
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/rooms/:roomid/devicetypes', function (req, res, next) {
    log.debug(req.url, req.params);

    if (!req.params.roomid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /rooms/:roomid/devicetypes'));
        return;
    }

    return Procs.getRoomDevicetypes(req.params.roomid, req.query.reporting)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
});


router.get('/rooms/:roomid/devices', function (req, res, next) {
    log.debug(req.url, req.params);

    if (!req.params.roomid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /rooms/:roomid/devicetypes'));
        return;
    }
    return Device.getByRoomWithAlarmAndType(req.params.roomid)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/devices', function (req, res, next) {
    log.debug(req.url, req.params);

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 4){
        next(new Error.ApiError('Права на редактирование датчиков имеют только инженеры и администратор'));
        return;
    }

    return Device.getByTypesWithRoomAndTypeAndSpec([1,2,3])
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/devicespecs', function (req, res, next) {
    log.debug(req.url, req.params);

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 4){
        next(new Error.ApiError('Права на редактирование датчиков имеют только инженеры и администратор'));
        return;
    }

    return DeviceSpec.getAll()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/devicetypes', function (req, res, next) {
    log.debug(req.url, req.params);

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 4){
        next(new Error.ApiError('Права на редактирование датчиков имеют только инженеры и администратор'));
        return;
    }

    return Devicetype.getAll()
        .then(function (data) {
            res.json(data.filter(type => {
                return [DeviceTypeList.types.temperature, DeviceTypeList.types.humidity, DeviceTypeList.types.pressure].includes(type.DEVICETYPE_ID);
            }));
        })
        .catch(function (err) {
            next(err);
        })
});


router.get('/devices/:deviceid', function (req, res, next) {
    log.debug(req.url, req.params);

    if (!req.params.deviceid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /rooms/:roomid/devicetypes'));
        return;
    }
    return Device.getById(req.params.deviceid)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
});


router.post('/devices/:deviceid', function (req, res, next) {
    log.debug(req.url, req.params);

    if (req.user.ROLE_ID != 1 && req.user.ROLE_ID != 4){
        next(new Error.ApiError('Права на редактирование датчиков имеют только инженеры и администратор'));
        return;
    }

    if (!req.params.deviceid || !req.body.devicespecid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /devices/:deviceid. Параметры: :devicespecid'));
        return;
    }

    return Procs.editDevice(req.params.deviceid, req.body.devicespecid)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
});

router.get('/*', function (req, res, next) {
    var err = new Error.HttpError(404, 'Api ' + req.url + ' не найдено');
    next(err);
});

module.exports = router;
