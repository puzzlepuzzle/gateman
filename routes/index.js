var express = require('express');
var router = express.Router();
var log = require('../log')(module);
import React from 'react'
import {renderToString} from 'react-dom/server'
import {match, RouterContext} from 'react-router'
var routes = require('../components/routes.jsx')
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {reducer, getInitialState} from '../components/reducers/reducer'


function cb(req, res, next) {
    match({routes, location: req.url}, (err, redirect, props) => {
        if (err) {
            var err = new Errors.HttpError(500, err);
            next(err);
        } else if (redirect) {
            res.redirect(redirect.pathname + redirect.search)
        } else if (props) {

            //var r = {}
            var r = {
                isAuth: req.isAuthenticated(),
                role: req.isAuthenticated() ? req.user.ROLE_ID : "",
                userLogin: req.isAuthenticated() ? req.user.USER_LOGIN : "",
                userId: req.isAuthenticated() ? req.user.USER_ID : ""
            }

            if (req.isAuthenticated() && req.user.DISABLED) {
                log.debug('user ' + req.user.USER_LOGIN + ' is disabled!')
                req.logout();
                r.isAuth = false;
            }

            log.debug('AUTH', r)
            var store = createStore(reducer, getInitialState(r))
            var appHtml = renderToString(
                <Provider store={store}>
                        <RouterContext {...props}/>
                </Provider>
            );
            res.render('index', {appHtml: appHtml, context: JSON.stringify(r)})
        } else {
            next();
        }
    })

}

router.get('/*', cb);

module.exports = router;