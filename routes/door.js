var express = require('express');
var router = express.Router();
var log = require('../log')(module);
var Promise = require("bluebird");
var Indication = require("./../db/models/indication");
var Device = require("./../db/models/device");
var Devicetype = require("./../db/models/devicetype");
var Room = require("./../db/models/room");
var Error = require("./../errors")
var decimate = require('../utils').arrayDecimate;
var moment = require('moment');
var config = require('../config');
var DeviceTypeList = require( "../db/device_type_list");
var AlarmSingleton = require("../alarm/alarm_singleton.js");

var funcTypes = {
    open: 'open',
    close: 'close'
}

var cbCreator = function(func){
    return function (req, res, next) {
        log.debug(req.url, req.params);
        var doorid = req.params.doorid;

        if (!doorid) {
            next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /door/' + func + '/:doorid/:doorvalue'));
            return;
        }
        Device.getById(doorid)
            .then(function (device) {
                if (!device) {
                    next(new Error.ApiError('Нет устройства с id ' + doorid));
                    return;
                }
                if (device.DEVICETYPE_ID != DeviceTypeList.types.door) {
                    next(new Error.ApiError('Устройство с id ' + doorid + ' не дверь'));
                    return;
                }
                var value = req.params.doorvalue;
                if (func == funcTypes.close){
                    value = 0;
                }
                return Indication.newIndicator(value, doorid)
            })
            .then(function (newValue) {
                log.debug("door SAVED: ", JSON.stringify(newValue))
                if (+config.get("start_alarm")){
                    return new AlarmSingleton().callAlarm(newValue.DEVICE_ID, newValue.INDICATION_VALUE);
                }
            })
            .then(function (newValue) {
                res.end("OK");
            })
            .catch(function (err) {
                next(err);
            })
    }
}
router.get('/'+funcTypes.open+'/:doorid/:doorvalue', cbCreator(funcTypes.open));
router.get('/'+funcTypes.close+'/:doorid/:doorvalue', cbCreator(funcTypes.close));

router.get('/*', function (req, res, next) {
    var err = new Error.HttpError(404, 'Api ' + req.url + ' не найдено');
    next(err);
});

module.exports = router;
