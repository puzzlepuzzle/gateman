var express = require('express');
var router = express.Router();
var log = require('../log')(module);
var Promise = require("bluebird");

var Roomcheck = require("./../db/models/roomcheck");
var Device = require("./../db/models/device");
var Room = require("./../db/models/room");
var Error = require("./../errors")
var decimate = require('../utils').arrayDecimate;

var moment = require('moment');
var config = require('../config');

var d3 = require('d3');
var jsdom = require('jsdom');
var doc = jsdom.jsdom();
var DeviceTypeList = require("../db/device_type_list");
var Procs = require("../db/models/procs");

//router.use(require('../middleware/authCheck'));

var createChart = function (id, data, maxG, minG, maxY, minY, color, deviceType) {
    // console.log(data)
    // console.log(maxG, minG, maxY, minY)
    d3.select(doc.body).append('div').attr('id', id);
    // define dimensions of graph
    var m = [120, 20, 50, 80]; // margins
    var w = 1000 - m[1] - m[3]; // width
    var h = 600 - m[0] - m[2]; // height
    var parseDate = d3.time.format("%H.%M.%S %d.%m.%y").parse;
    // create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)

    // X scale will fit all values from data[] within pixels 0-w
    data = data.map((function (el) {
        el.date = parseDate(el.date)
        return el
    }))
    var yvals = [maxG, minG, maxY, minY];
    data.map(function (el) {
        yvals.push(el.val)
    })
    var xScale = d3.time.scale().range([0, w]).domain(d3.extent(data, function (d) {
        return d.date;
    }));
    // Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
    var yScale = d3.scale.linear().domain([Math.floor(0.95 * d3.min(yvals)), Math.floor(1.05 * d3.max(yvals))]).range([h, 0]);
    // automatically determining max range can work something like this
    // var y = d3.scale.linear().domain([0, d3.max(data)]).range([h, 0]);
    // create a line function that can convert data[] into x and y points
    var lineCreator = function (methodY) {
        return d3.svg.line()
        // assign the X function to plot our line as we wish
            .x(function (d) {
                // console.log("X", d,xScale(d.date))
                // return the X coordinate where we want to plot this datapoint
                return xScale(d.date);
            })
            .y(function (d) {
                // console.log("Y", d,methodY(d.val))
                // return the Y coordinate where we want to plot this datapoint
                return methodY(d.val);
            })
    }
    var line = lineCreator(yScale)
    var maxGline = lineCreator(function (d) {
        return yScale(maxG)
    })
    var minGline = lineCreator(function (d) {
        return yScale(minG)
    })
    var maxYline = lineCreator(function (d) {
        return yScale(maxY)
    })
    var minYline = lineCreator(function (d) {
        return yScale(minY)
    })
    // Add an SVG element with the desired dimensions and margin.
    var graph = d3.select(doc.getElementById(id)).append("svg:svg")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2]);

    var legend = graph.append("g")
        .attr("class", "legend")
        //.attr("x", w - 65)
        //.attr("y", 50)
        .attr("height", 100)
        .attr("width", 150)
        .attr('transform', 'translate( 40 ,20)')

    var colors = [[color, 1]];
    var names = [ "- " + deviceType.toLowerCase()];
    if (!isNaN(minG) && !isNaN(maxG)
        && !isNaN(minY) && !isNaN(maxY)) {
        colors = [["yellow", 1], ["red", 1], ["red", 0.2], [color, 1]];
        names = ["- границы оптимальных значений (" + minG + " .. " + minG + ")",
            "- границы допустимых значений (" + minY + " .. " + maxY + ")",
            "- недопустимые значения (<" + minG + ", >" + maxG + ")",
            "- " + deviceType.toLowerCase()];
    }
    legend.selectAll('rect')
        .data(colors)
        .enter()
        .append("rect")
        .attr("x", m[1])
        .attr("y", function (d, i) {
            return i * 20;
        })
        .attr("width", 15)
        .attr("height", 15)
        .style("fill", function (d) {
            return d[0];
        })
        .style("fill-opacity", function (d) {
            return d[1];
        })
    legend.selectAll('text')
        .data(names)
        .enter()
        .append("text")
        .attr("font-size", 14)
        .attr("x", m[1] + 20)
        .attr("y", function (d, i) {
            return i * 20 + 13;
        })
        .text(function (d) {
            return d;
        });


    var chart = graph.append("svg:g")
        .attr("transform", "translate(" + m[3] + "," + (names.length* 20 + 30) + ")");
    // create yAxis
    var xAxis = d3.svg.axis().scale(xScale).tickFormat(d3.time.format("%H.%M %d.%m"))
    //.ticks(d3.time.hours, 2)
    // .tickSize(-h + m[0] + m[2]).tickSubdivide(true);
    // Add the x-axis.
    chart.append("svg:g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + h + ")")
        .call(xAxis)
        .selectAll('.x .tick text')
        .call(function (t) {
            t.each(function (d) {
                var self = d3.select(this);
                var s = self.text().split(' ');
                self.text(null);
                self.append("tspan")
                    .attr("x", 0)
                    .attr("dy", "1.1em")
                    .text(s[0]);
                self.append("tspan")
                    .attr("x", 0)
                    .attr("dy", "1.1em")
                    .text(s[1]);
            })
        })



    // .selectAll("text")
    // .style("text-anchor", "end")
    // .attr("dx", "-.8em")
    // .attr("dy", ".15em")
    // .attr("transform", function(d) {
    //     return "rotate(-65)"
    // });
    ;
    // create left yAxis
    var yAxisLeft = d3.svg.axis().scale(yScale).orient("left");
    // .tickSize(-w + m[1] - m[3]).tickSubdivide(true)
    // Add the y-axis to the left
    chart.append("svg:g")
        .attr("class", "y axis")
        // .attr("transform", "translate(-25,0)")
        .call(yAxisLeft);

    // Add the line by appending an svg:path element with the data line we created above
    // do this AFTER the axes above so that the line is above the tick-lines
    chart.append("rect").attr("fill", "red").attr("fill-opacity", 0.1)
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", w)
        .attr("height", yScale(maxY));

    chart.append("rect").attr("fill", "red").attr("fill-opacity", 0.1)
        .attr("x", 0)
        .attr("y", yScale(minY))
        .attr("width", w)
        .attr("height", h - yScale(minY));
    chart.append("svg:path").attr("stroke", "yellow").attr("d", maxGline(data));
    chart.append("svg:path").attr("stroke", "yellow").attr("d", minGline(data));
    chart.append("svg:path").attr("stroke", "red").attr("d", maxYline(data));
    chart.append("svg:path").attr("stroke", "red").attr("d", minYline(data));

    // graph.selectAll("dot")
    //     .data(data)
    //     .enter().append("circle")
    //     .attr("r", 1)
    //     .attr("cx", function(d) { return xScale(d.date); })
    //     .attr("cy", function(d) { return yScale(d.val); });

    chart.append("svg:path").attr("stroke", color).attr("d", line(data));

    var svg = d3.select(doc.getElementById(id)).node().outerHTML;
    d3.select(doc.getElementById(id)).remove();

    return svg;
}
router.get('/roomchecks/:roomcheckid', function (req, res, next) {
    log.debug(req.url, req.params);
    var roomcheckid = req.params.roomcheckid;

    if (!roomcheckid) {
        next(new Error.ApiError('Не все параметры переданы для вызова. Ожидаемый формат /roomchecks/:roomcheckid'));
        return;
    }
    var roomcheck;
    var room;
    var person;
    return Roomcheck.getById(roomcheckid)
        .then(function (item) {
            if (!item) {
                throw new Error.ApiError('Отчета с таким id не существует');
                return;
            }
            roomcheck = item.roomcheck;
            room = item.room;
            person = item.person;
            return Procs.getDevicesForRoomcheck(roomcheckid)
        })
        .then(function (devices) {
            if (!devices || !devices.length) {
                throw new Error.ApiError(' Не получено ни одного устройства');
                return;
            }

            var promises = [];
            for (var key in devices) {
                promises.push((function (device) {
                    return Procs.getIndicationsByDeviceFromTo(device.DEVICE_ID, roomcheck.ROOMCHECK_FROM, roomcheck.ROOMCHECK_TO, 1)
                    // return Indication.getByDeviceFromTo(device.DEVICE_ID, date_from, date_to)
                        .then(function (data) {
                            // console.log("++++++++++++");
                            // console.log(data);
                            // console.log("--------");
                            // console.log("============");
                            var result = {
                                device: device,
                                data: [],
                                maxCount: config.get('chart_points_count')
                            }
                            for (var key in data) {
                                var o = data[key];
                                result.data.push({
                                    val: o.INDICATION_VALUE,
                                    date: moment.utc(o.INDICATION_REGDATE).format("HH.mm.ss DD.MM.YY")
                                });
                            }
                            var color = "steelblue";
                            switch (device.DEVICETYPE_ID) {
                                case DeviceTypeList.types.temperature:
                                    color = "darkgreen";
                                    break;
                                case DeviceTypeList.types.humidity:
                                    color = "blue";
                                    break;
                                case DeviceTypeList.types.pressure:
                                    color = "purple";
                                    break;
                            }
                            // console.log(result.data);
                            // result.data = decimate(result.data, result.maxCount, false);
                            result.svg = createChart(device.DEVICE_ID, result.data,
                                device.ALARM_GZONE_TO, device.ALARM_GZONE_FROM, device.ALARM_YZONE_TO, device.ALARM_YZONE_FROM, color, device.DEVICETYPE_NAME)

                            return result
                        })
                })(devices[key]))
            }

            return Promise.all(promises)
        })
        .then(function (dataForChart) {
            res.render('print_devices', {
                data: dataForChart,
                roomcheck,
                room,
                person,
                moment: moment,
                from: moment.utc(roomcheck.ROOMCHECK_FROM).format("DD.MM.YYYY HH:mm"),
                to: moment.utc(roomcheck.ROOMCHECK_TO).format("DD.MM.YYYY HH:mm"),
                user: req.user ? req.user.USER_LOGIN : '-'
            });
        })
        .catch(function (err) {
            next(err);
        })
});


router.get('/*', function (req, res, next) {
    var err = new Error.HttpError(404, 'Api ' + req.url + ' не найдено');
    next(err);
});

module.exports = router;
