var Queue = require("./queue");
var Promise = require('bluebird')
var log = require('../log')(module);
var MBSingleton = require('../modbus/mb_singleton');
var Device = require("./../db/models/device");
var Indication = require("./../db/models/indication");
var Alarm = require("./../db/models/alarm");
var config = require('./../config');
var DeviceTypeList = require("../db/device_type_list.js");
var Init = {};
var AlarmSingleton = require("../alarm/alarm_singleton");
var Procs = require("./../db/models/procs");

Init.initTasks = function () {
    var queue = new Queue();

    queue.clean()
        .then(()=>{
            Queue.startInterface();

            registerTasks(queue);
            if (+config.get("start_read")){
                 return initTasksByDevices(queue);
            }
            
        })

}

Init.taskTypes = {
    device: "device",
    device_start: "device_start",
    alarm: "alarm",
    complex: "complex",
    average: "average"
}

function registerTasks(queue) {

    function cb_device(job) {
        var currentDevice = job.data;
        var device;
        var devicespec;
        var devicetype;

        return Device.getByIdAll(currentDevice.DEVICE_ID)
            .then(function (data) {
                device = data['device'];
                devicespec = data['spec'];
                devicetype = data['type'];
            })
            .then(function () {
                if (+config.get("debug")){
                    return new MBSingleton().readFake(device.DEVICETYPE_ID)
                } else {
                    return new MBSingleton().read(device.DEVICE_COM, device.DEVICE_COM_SPEED, devicespec.DEVICE_ADDRESS, devicetype.DEVICE_BYTE_START, devicetype.DEVICE_BYTE_COUNT);
                }
            })
            .then(function (value) {
                return Indication.newIndicator(value, device.DEVICE_ID)
            })
            .then(function (newValue) {
                log.debug("device SAVED: ", JSON.stringify(device), newValue)
                return newValue;
            })
            .catch(function (err) {
                log.error("device indication error: ", device, err, err.stack)
            })
            .then(function () {
                return queue.addTask(Init.taskTypes.device, device, Queue.priority.normal, config.get('device_task_frequency'))
            })
            .catch(function (err) {
                log.error("device add task error: ", device, err)
            })
        // .delay(500)


    }

    function cb_device_start(job) {
        var currentDevice = job.data;
        var device;

        return Device.getById(currentDevice.DEVICE_ID)
            .then(function (data) {
                device = data;
            })
            .then(function () {
                if (+config.get("debug")){
                    return new MBSingleton().readFake(device.DEVICE_COM, device.DEVICE_COM_SPEED)
                } else {
                    return new MBSingleton().startRead(device.DEVICE_COM, device.DEVICE_COM_SPEED)
                }
            })
            .then(function () {
                log.debug("device START DONE: ", JSON.stringify(device), " ")
            })
            .catch(function (err) {
                log.error("device start error: ", device, err)
            })
            .then(function () {
                return queue.addTask(Init.taskTypes.device_start, device, Queue.priority.normal, config.get('device_start_task_frequency'))
            })
            .catch(function (err) {
                log.error("device start add task error: ", device,  err)
            })
        // .delay(500)


    }

    function cb_complex(job) {
        var device = job.data;

        return Procs.calcComplexIndication(device.DEVICE_ID, new Date(), config.get('complex_task_frequency'))
            .then(function (newValue) {
                log.debug("complex device SAVED: ", JSON.stringify(device), newValue);
                return newValue;
            })
            .catch(function (err) {
                log.error("complex device indicator error: ", device, err, err.stack)
            })
            .then(function () {
                return queue.addTask(Init.taskTypes.complex, device, Queue.priority.normal, config.get('device_task_frequency'))
            })
            .catch(function (err) {
                log.error("complex device add task error: ", device, err)
            })
        // .delay(500)


    }

    function cb_average(job) {
        var device = job.data;

        return Procs.calcAverageIndication(device.DEVICE_ID, new Date(), config.get('average_indication_frequency'))
            .then(function (newValue) {
                log.debug("average value SAVED: ", JSON.stringify(device), newValue);
                return newValue;
            })
            .then(function (newValue) {
                if (+config.get("start_alarm")){
                    return new AlarmSingleton().callAlarm(device.DEVICE_ID, newValue);
                }
            })
            .catch(function (err) {
                log.error("average indication error: ", device, err, err.stack)
            })
            .then(function () {
                return queue.addTask(Init.taskTypes.average, device, Queue.priority.normal, config.get('average_indication_frequency'))
            })
            .catch(function (err) {
                log.error("add average task error: ", device, err)
            })
        // .delay(500)


    }

    queue.registerTask(Init.taskTypes.device_start, cb_device_start);
    queue.registerTask(Init.taskTypes.device, cb_device);
    queue.registerTask(Init.taskTypes.complex, cb_complex);
    queue.registerTask(Init.taskTypes.average, cb_average);
}

function initTasksByDevices(queue) {
    var ports = {}
    return Device.getByTypes([DeviceTypeList.types.temperature, DeviceTypeList.types.humidity, DeviceTypeList.types.pressure])
        .then(function (devices) {
            //devices = devices.slice(0,13)
            for (var i = 0; i < devices.length; i++) {
                // if (devices[i].DEVICE_ID != 5 && devices[i].DEVICE_ID != 6) {
                //     continue;
                // }
                if (!ports[devices[i].DEVICE_COM]){
                    ports[devices[i].DEVICE_COM] = true;
                    log.debug("Чтение порта запущено ", JSON.stringify(devices[i]))
                    queue.addTask(Init.taskTypes.device_start, devices[i], Queue.priority.normal)
                }
                log.debug("Устройство запущено ", JSON.stringify(devices[i]))
                queue.addTask(Init.taskTypes.device, devices[i], Queue.priority.normal);
                queue.addTask(Init.taskTypes.average, devices[i], Queue.priority.normal);
            }
            log.info("Запущено ", devices.length, " задач на считывание датчиков")
        })
        .then(function () {
            return Device.getByTypes([DeviceTypeList.types.complex])
        })
        .then(function (devices) {
            for (var i = 0; i < devices.length; i++) {
                // continue;
                queue.addTask(Init.taskTypes.complex, devices[i], Queue.priority.normal);
                queue.addTask(Init.taskTypes.average, devices[i], Queue.priority.normal);
            }
            log.info("Запущено ", devices.length, " задач на считывание сложных датчиков")
        })
}


module.exports = Init;
