var kue = require('kue')
var Promise = require('bluebird')
Promise.promisifyAll(kue.Job)
var Config = require('../config');
var log = require('../log')(module);

var Queue = function () {
    this._queue = null;
    this.connect();
}

Queue.priority = {
    low: 10
    , normal: 0
    , medium: -5
    , high: -10
    , critical: -15
}

Queue.jobState = {
    active: "active",
    inactive: "inactive",
    failed: "failed",
    complete: "complete",
    delayed: "delayed"
}

Queue.startInterface = function () {
    kue.app.listen(Config.get('kue_port'));
    log.info('kue interface is started')
}

Queue.prototype.connect = function () {
    this._queue = kue.createQueue({
        redis: Config.redis
    });
    Promise.promisifyAll(this._queue);
    this._queue.on('error', function (err) {
        log.error('kue error ', err);
    });
    log.debug("create queue");
}

//handler: (data: any)=>Promise<any>)
Queue.prototype.registerTask = function (taskName, handler) {
    this._queue.process(taskName, 1, function (job, done) {
        return handler(job)
            .then(function () {
                done()
            })
            .catch(function (err) {
                done(err)
            })
    });
}

Queue.prototype.addTask = function (taskName, data, priority, delay_ms) {
    var _this = this;
    priority = priority || Queue.priority.normal;
    return new Promise(function (resolve, reject) {
        var job = _this._queue.create(taskName, data)
            .delay(delay_ms)
            .priority(priority)
            .ttl(5 * 60 * 1000)
            .removeOnComplete(true)
            .save(function (err) {
                    if (err) {
                        reject(err)
                    }
                    resolve(job.id)
                }
            );
    })
}

Queue.prototype.getState = function (id, removeAfterComplete) {
    return new Promise(function (resolve, reject) {
        kue.Job.get(id, function (err, job) {
            if (!job) {
                return reject(new Error("No job was found with id " + id));
            }
            if (err) {
                return reject(new Error("Job has error: " + err + ". Job: " + id + ", " + job.state()));
            }
            //if (job.state() == 'failed'){
            //    reject(new Error("Job failed"))
            //}
            //if (job && job.state() == 'complete' ) {
            //    if (removeAfterComplete)
            //        job.remove();
            //}
            return resolve(job);
        });
    })
}

Queue.prototype.clean = function () {
    var rangeByState = Promise.promisify(kue.Job.rangeByState, {context: kue.Job});
    var inactiveCount = Promise.promisify(this._queue.inactiveCount, {context: this._queue});
    var activeCount = Promise.promisify(this._queue.activeCount, {context: this._queue});
    var completeCount = Promise.promisify(this._queue.completeCount, {context: this._queue});
    var failedCount = Promise.promisify(this._queue.failedCount, {context: this._queue});
    var delayedCount = Promise.promisify(this._queue.delayedCount, {context: this._queue});
    return Promise.all([
            inactiveCount(),
            activeCount(),
            completeCount(),
            failedCount(),
            delayedCount()
        ])
        .spread(function (inactive, active, complete, failed, delayed) {
            log.info("Kue counters: inactive=" + inactive, ", active=" + active,
                ", failed=" + failed,
                ", delayed=" + delayed,
                ", complete=" + complete)
            return Promise.all([
                rangeByState('inactive', 0, inactive, 'asc'),
                rangeByState('active', 0, active, 'asc'),
                rangeByState('failed', 0, failed, 'asc'),
                rangeByState('delayed', 0, delayed, 'asc'),
                rangeByState('complete', 0, complete, 'asc')
            ])
        })
        .spread(function (inactive, active, failed, delayed, complete) {
            log.info("Kue jobs: inactive=" + inactive.length, ", active=" + active.length,
                ", failed=" + failed.length,
                ", delayed=" + delayed.length,
                ", complete=" + complete.length)
            var jobs = inactive.concat(active, failed, delayed, complete)
            jobs.forEach(function (job) {
                job.remove();
            });
        })
};
module.exports = Queue